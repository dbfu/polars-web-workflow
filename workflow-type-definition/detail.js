import React from "react";
import { connect } from "dva";
import { Input, message, Spin, Tabs } from "antd";
import Datastructure from "./data-structure/data-structure";
import Condeitionparameters from "./condition-parameter/condition-parameters";
import ContentHeader from "widget/content-header";
import Content from "widget/content";
import ContentScroller from "widget/content-scroller";
import ApprovalConditions from "./approval-conditions";
import ApprovalRules from "./approval-rules";
import service from "./service";
import UrlManage from "./url-manage";

const TabPane = Tabs.TabPane;
const { Search } = Input;




class WorkflowtypeDefonitionDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchKey: '',
      tab: 'BANK',
      tabs: [
      ],
      typeCode: '',
      typeName: '',
      show: true,
    };
  }

  componentWillMount() {
    this.gitList()

  }


  // 更改标签页
  onChangeTabs = key => {
    this.setState({
      tab: key,
      searchKey: ''

    });
  };

  // // 返回

  // onBack = () => {
  //   const { dispatch } = this.props;
  //   dispatch(
  //     routerRedux.replace({
  //       pathname: `/workflow/type/definition`,
  //     })
  //   );

  // };

  gitList = () => {
    const { params } = this.props.match
    service.getListcode(params.id).then(res => {
      this.setState({
        typeCode: res.data.typeCode,
        typeName: res.data.typeName,
        show: false,
      })
    })
      .catch(err => {
        message.error(err.response.data.message);
      });


  }







  render() {
    const { typeCode, typeName, show } = this.state;
    const { params } = this.props.match;
    // console.log(this.bank) ref={(ref) => { (this.staff = ref) }}
    return (
      <div>
        <ContentHeader title="工作流类型定义" showBack onBack={this.onBack} />
        <ContentScroller>
          <Content>
            {<span> {show ? <Spin /> : <h3 className="header-title">{typeCode + '-' + typeName}</h3>}</span>}
            <Tabs onChange={this.onChangeTabs}>
              <TabPane tab="数据结构" key="Datastructure">
                <Datastructure params={params} />
              </TabPane>
              <TabPane tab="条件参数" key="Condeitionparameters">
                <Condeitionparameters params={params} />
              </TabPane>
              <TabPane tab="审批条件" key="ApprovalConditions">
                <ApprovalConditions params={params} />
              </TabPane>
              <TabPane tab="审批规则" key="ApprovalRules">
                <ApprovalRules params={params} />
              </TabPane>
              <TabPane tab="URL管理" key="UrlManage">
                <UrlManage params={params} />
              </TabPane>
            </Tabs>
          </Content>
        </ContentScroller>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    user: state.user.currentUser,
    company: state.user.company,
  };
}

export default connect(
  mapStateToProps,
  null,
  null,
  { withRef: true }
)(WorkflowtypeDefonitionDetail);
