import React from 'react';
import { Button, Form, Switch, Input, Divider, message, Select, Cascader, Spin, Tooltip, Icon } from 'antd';
import service from '../service';
import { connect } from 'dva';
import InputLanguage from "@/components/Widget/Template/input-language";


const formItemLayout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 10
  },
};

class NewWorkflowtypeDefonition extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      record: {},
      paramType: [],
      structRange: [],
      fieldValueList: [],
      LovSourceList: [], // 根据参数来源类型获取lov或值列表值
      LovSourceItemList: [],
      saveLoading: false,
      SourceinterfaceData: [],
      apiFieldValueList: [],
      apiSpinLoading: false,
    };
  }

  componentDidMount() {
    const { params } = this.props;
    if (params.id) {
      this.handleGetDetailedDate(params.id)
    }
    // 获取参数级别数据
    this.handleGetStructRange();
    // 取值列表中参数类型
    this.handleGetParamsTypeValue();
    // 获取接口级联数据
    this.getSourceInterface();
  }

  /**
   * @param id: 条件参数行id
   * 行数据数据不全，编辑时根据行id查询所有数据
   **/
  handleGetDetailedDate = id => {
    service.getParamTypeById(id)
      .then(res => {
        const record = { ...res.data };
        if (record.variableSourceInfo) {
          record.variableSourceInfo = JSON.parse(res.data.variableSourceInfo)
        }
        if (record.paramRangeInfo) {
          record.paramRangeInfo = JSON.parse(res.data.paramRangeInfo)
        }
        // 由于侧拉框部分下拉框和级联框的值来自勾选的值，调用接口查询，因此赋予初值时需要先手动给值调用接口
        const LovSourceItemList = [];
        if (record.paramRangeType && record.paramRangeType === 'lov') {
          record.paramRangeInfo.parameters.map(item => {
            LovSourceItemList.push({ keyCode: Object.keys(item)[0], name: item.tempName })
          })
        }
        this.setState({ record, LovSourceItemList }, () => {
          this.getParamsFieldValueList(record.structRank, record.variableSourceType);
          if (record.paramRangeType) {
            this.getLOVOrSysSourceList(record.paramRangeType);
          }
          if (record.variableSourceType === 'api') {
            this.getSourceInterface(record.variableSourceInfo.apiId);
          }
        })
      })
      .catch(err => {
        console.log(err);
        message.error(err.response.data.message);
      })
  }

  // 获取参数级别数据
  handleGetStructRange = () => {
    const { typeId } = this.props;
    service.getList(typeId)
      .then(res => {
        if (res.data) {
          this.setState({ structRange: res.data })
        }
      })
      .catch(err => {
        message.error(err.response.data.message)
      })
  }

  // 取值列表中参数类型
  handleGetParamsTypeValue = () => {
    this.getSystemValueList("WORKFLOW_PARAMETERS_TYPE")
      .then(res => {
        this.setState({ paramType: res.data.values })
      })
      .catch(error => {
        message.error(error.response.data.message)
      })
  }

  /** 提交 */
  handleSubmit = e => {
    e.preventDefault();
    const { form, onClose } = this.props;
    form.validateFieldsAndScroll((err, values) => {

      if (err) return;
      this.setState({ saveLoading: true })
      const params = this.formatFormValue(values);
      const i18n = {};
      i18n.paramName = values.paramName.i18n;
      params.paramName = values.paramName.value;
      params.i18n = i18n;
      service.saveNewRangeValue({ ...params })
        .then(() => {
          message.success("操作成功")
          this.setState({ saveLoading: false })
          if (onClose) {
            onClose(true);
          }
        })
        .catch(error => {
          message.error(error.response.data.message);
          this.setState({ saveLoading: false });
        })
    });
  };

  /**
   * 处理表单数据
   * @param values： 表单对象
   */
  formatFormValue = values => {
    const { typeId } = this.props;
    const { record } = this.state;
    const params = { ...record, ...values, typeId };

    if (values.variableSourceInfoApi) { // 参数来源中来源类型为接口取值
      const parameters = [];
      Object.keys(values).forEach(key => {
        const newKey = key.split('-')[1];
        if (key.includes('sourceParams-')) {
          const code = newKey.split('_')[0]
          const name = newKey.split('_')[1]
          const str = name.replace(/\//g, ".");
          parameters.push({
            fieldName: code,
            name: str,
            structCode: !!values[key] && Array.isArray(values[key]) ? values[key][0] : '',
            fieldCode: !!values[key] && Array.isArray(values[key]) ? values[key][1] : '',
          });

        }
      })

      params.variableSourceInfo = {
        apiId: values.variableSourceInfoApi[1], // 取第二个存储 apiId 值
        showCascadeApi: values.variableSourceInfoApi, // 做为级联展示的 包含（appId,apiId）数组
        parameters,
      }
      params.variableSourceInfo = JSON.stringify(params.variableSourceInfo);

      delete params.variableSourceInfoApi;
    } else if (values.variableSourceInfoData) {
      params.variableSourceInfo = {
        structCode: values.variableSourceInfoData[0],
        fieldCode: values.variableSourceInfoData[1],
      }
      params.variableSourceInfo = JSON.stringify(params.variableSourceInfo);
      delete params.variableSourceInfoData;
    }
    // 参数范围 sysCode值列表, lov LOV
    if (values.paramRangeType) {
      if (values.paramRangeType === 'sysCode') {
        const parameters = [];

        params.paramRangeInfo = { sysCode: values.paramRangeInfo };
      } else {
        const parameters = [];
        Object.keys(values).map(key => {
          if (key.includes("lovParams-")) {
            const tempKey = key.split("-")[1];
            const code = tempKey.split('_')[0]
            const name = tempKey.split('_')[1].replace(/\//g, ".");

            const temp = tempKey.includes('lovCode') ? {
              lovCode: values[key] || '',
              tempName: '参数-lovCode'
            } : {
                [code]: values[key] || '',
                tempName: name,
              }
            parameters.push(temp)
          }
        });
        params.paramRangeInfo = {
          lovCode: values.paramRangeInfo,
          parameters,
        };
      }
    }
    params.paramRangeInfo = params.paramRangeInfo && JSON.stringify(params.paramRangeInfo);
    return params;
  }

  handleCancel = () => {
    this.props.onClose && this.props.onClose();
  };

  /**
   * @param structCode: 参数级别值
   * @param structType: 来源类型值
   * 根据参数级别,来源类型获取字段数据源
   */
  getParamsFieldValueList = (structCode, structType) => {
    const { typeId } = this.props;

    let handleMethod = null;

    if (structCode && structType === "dataField") {
      // 匹配字段取值（指定级别）
      handleMethod = service.getDataStructById;
    } else if (structType === "api" && structCode) {
      // 接口取指定包括上级数据结构字段
      handleMethod = service.getParamsFieldValueList
    } else if (!structType || !structCode) {
      handleMethod = void 0;
    }
    if (handleMethod) {
      handleMethod(structCode, typeId)
        .then(res => {
          if (!res) return;
          let resultArr = [];

          if (structType === "dataField") {
            resultArr = res.data ? [{ ...res.data }] : [];
          } else {
            resultArr = Array.isArray(res.data) ? [...res.data] : [];
          }

          const tempArr = !!resultArr.length ? resultArr.map(item => {
            const newItem = { ...item };

            newItem.value = item.structCode;
            newItem.label = item.structName;
            newItem.disabled = !newItem.children
              || (
                Array.isArray(newItem.children)
                && newItem.children.length === 0
              ); // 不存在子级的禁用

            Array.isArray(newItem.children) && newItem.children.forEach(v => {
              v.value = v.fieldCode
              v.label = v.fieldName
            })
            return newItem;
          }) : [];

          this.setState({
            fieldValueList: tempArr,
          })
        })
        .catch(() => { })
    }
  }

  // 渲染参数input框
  renderParamsInput = () => {
    const { LovSourceItemList, record } = this.state;
    const { form: { getFieldDecorator }, currentUser, systemFlag } = this.props;
    const disabled = currentUser.tenantId === '0' ? false : (systemFlag === 'true')

    if (LovSourceItemList.length) {
      return (
        <>
          {
            (LovSourceItemList.map((item, index) => {
              const labelIndex = (`${this.$t(item.name)}` || item.keyCode);
              const rule = [
                { required: item.requiredFlag, message: this.$t("common.please.enter") }
              ];
              let str = '';
              if (item.name) { str = item.name.replace(/\./g, "/") };
              // if (item.name) { const str = item.name.replace('.', '/') }
              return (
                item.fieldName === 'lovCode'
                  ? (this.renderMultLovInput(index))
                  : (<Form.Item label={labelIndex} key={labelIndex} {...formItemLayout}>
                    {
                      getFieldDecorator(`lovParams-${item.keyCode}_${str}`, {
                        rules: rule,
                        initialValue: record.id && record.paramRangeInfo && Array.isArray(record.paramRangeInfo.parameters)
                          ? record.paramRangeInfo.parameters[index][item.keyCode] : undefined,
                      })(
                        <Input placeholder={this.$t("common.please.enter")} disabled={disabled} />
                      )
                    }
                  </Form.Item>)
              )
            }))
          }
        </>
      )
    }
  }

  // 渲染级联lov取值input
  renderMultLovInput = (index) => {
    const { LovSourceItemList, record } = this.state;
    const { form: { getFieldDecorator } } = this.props;
    return (
      <Form.Item
        label={(
          <span>
            参数-lovCode
            <Tooltip title="级联lovCode,用于配置审批条件lov取值中级联的数据源">
              <Icon type="exclamation-circle" style={{ marginLeft: '4px' }} />
            </Tooltip>
          </span>
        )}
        {...formItemLayout}
      >
        {
          getFieldDecorator(`lovParams-lovCode_${'参数-lovCode'}`, {
            rules: [{
              // pattern: /^([A-Za-z0-9_]+,)*[A-Za-z0-9_]+$/ig,
              // message: '请输入英文或数字,并以逗号相隔',
              // validateTrigger: 'onBlur',
            }],
            initialValue: record.id && record.paramRangeInfo && Array.isArray(record.paramRangeInfo.parameters)
              ? record.paramRangeInfo.parameters[index].lovCode : '',
          })(
            <Input placeholder={this.$t("common.please.enter")} />
          )
        }
      </Form.Item>
    )
  }

  // 渲染来源类型为接口取值时的动态参数框
  renderApiParamsInput = () => {
    const { form: { getFieldDecorator } } = this.props;
    const { record, SourceinterfaceData, fieldValueList } = this.state;

    if (!SourceinterfaceData.length) return;
    return (
      <>
        {
          (SourceinterfaceData.map((item) => {
            const rule = [
              { required: item.requiredFlag, message: this.$t("common.please.select") }
            ];
            // /\*/g,''
            const str = item.name ? item.name.replace(/\./g, "/") : 'null';
            return (
              <Form.Item label={this.$t(item.name) || item.keyCode} key={item.id} {...formItemLayout}>
                {
                  getFieldDecorator(`sourceParams-${item.keyCode}_${str}`, {
                    rules: rule,
                    initialValue: record.id && record.variableSourceInfo && Array.isArray(record.variableSourceInfo.parameters)
                      ? this.dealInitVAboutVarSourInApi(item.keyCode, record.variableSourceInfo.parameters) : undefined,
                  })(
                    <Cascader
                      mode="multiple"
                      placeholder={this.$t("common.please.select")}
                      allowClear
                      disabled={!!record.id}
                      options={fieldValueList}
                      getPopupContainer={node => node.parentNode}
                      onFocus={this.messageTipAboutInfoApi}
                    />
                  )
                }
              </Form.Item>
            )
          }))
        }
      </>
    )
  }


  getLOVOrSysSourceList = value => {

    if (value) {
      let handleMethod = null;
      if (value === 'sysCode') { // 值列表
        handleMethod = service.getSysSourceList;
      } else { // lov数据
        handleMethod = service.getLOVSourceList;
      }

      if (!handleMethod) return;
      handleMethod()
        .then(res => {
          if (res.data) {
            this.setState({ LovSourceList: res.data })
          }
        })
        .catch(() => { })
    }
  }

  /**
   * 获取lov对应接口的参数数据
   * @param id: 取值lov时的lovId
   */
  getLOVSourceItem = id => {
    if (id) {
      service
        .getappproveRender(id)
        .then(res => {
          this.setState({
            LovSourceItemList: res.data ? [...res.data, { fieldName: 'lovCode' }] : [],
          })
        })
        .catch(err => { message.error(err.response.data.message) })
    }
  }

  // 渲染来源接口里的值
  getSourceInterface = (apiId) => {
    this.setState({ apiSpinLoading: true });
    // 获取对应用下的接口字段
    if (apiId) {
      service.getappproveRender(apiId)
        .then(res => {
          if (res.data) {
            this.setState({ SourceinterfaceData: res.data })
          }
        }).catch(() => { })
    }

    service
      .getSourceInterface()
      .then(res => {
        if (res.data) {
          const tempArr = Array.isArray(res.data) ? res.data.map(item => {

            let params = {
              value: item.appId,
              label: item.appName,
              children: [],
            }
            Array.isArray(item.listInterface) && item.listInterface.forEach(v => {
              let vo = {
                value: v.id,
                label: v.interfaceName,
              }
              params.children.push(vo)
            })
            return params;
          }) : [];
          this.setState({
            apiFieldValueList: tempArr,
            apiSpinLoading: false,
            // SourceinterfaceData: res.data && res.data.listInterface ? res.data.listInterface : [],
          })
        }

      })
      .catch(() => { })
  }

  /**
   * 处理来源类型为api的初始值
   */
  dealInitVAboutVarSourInApi = (fieldName, value) => {
    if (Array.isArray(value)) {
      const index = value.findIndex(item => item.fieldName === fieldName);
      if (index >= 0) {
        const initValue = [value[index].structCode || '', value[index].fieldCode || ''];
        return initValue;
      }
      return [];
    }
    return [];
  }

  messageTipAboutInfoApi = () => {
    const { form: { getFieldValue } } = this.props;
    if (!getFieldValue('structRank')) {
      message.error("请先选择参数级别")
    }
  }

  onChangeApiInfo = (value) => {
    // const { SourceinterfaceData } = this.state;
    // interfaceId
    const interfaceId = value[1];
    // interfaceId获取字段渲染文本框
    service.getappproveRender(interfaceId)
      .then(res => {
        const interfaceData = res.data;
        this.setState({ SourceinterfaceData: interfaceData })
      })
      .catch(() => { })
  }

  render() {
    const { form: { getFieldDecorator, getFieldValue, resetFields }, currentUser, systemFlag } = this.props;
    const { record, paramType, structRange, fieldValueList, LovSourceList, saveLoading, apiFieldValueList, apiSpinLoading } = this.state;
    const disabled = currentUser.tenantId === '0' ? false : (systemFlag === 'true')
    return (
      <>
        <Form>
          <span>基本信息</span>
          <Divider />
          <Form.Item {...formItemLayout} label="参数代码">
            {getFieldDecorator('paramCode', {
              initialValue: record.id ? record.paramCode : undefined,
              rules: [
                { required: true, message: '请输入' },
                {
                  pattern: /^[0-9a-zA-Z_]{1,}$/,
                  message: '只能输入英文数字下划线!'
                },
              ],
            })(<Input placeholder="请输入" disabled={disabled || !!record.id} />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="参数名称">
            {getFieldDecorator('paramName', {
              initialValue: record.id ? { value: record.paramName, i18n: record.i18n ? record.i18n.paramName : [] } : undefined,
              rules: [{ required: true, message: '请输入' }],
            })(<InputLanguage placeholder="请输入" disabled={disabled} />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="参数级别">
            {getFieldDecorator('structRank', {
              initialValue: record.id ? record.structRank : undefined,
              rules: [{ required: true, message: this.$t("common.please.select") }],
            })(
              <Select
                placeholder={this.$t("common.please.select")}
                allowClear
                disabled={disabled || !!record.id}
                onChange={value => { this.getParamsFieldValueList(value, getFieldValue('variableSourceType')) }}
                getPopupContainer={node => node.parentNode}
              >
                {structRange.map(item => (
                  <Select.Option key={item.structCode}>{item.structName}</Select.Option>
                ))}
              </Select>
            )
            }
          </Form.Item>
          <Form.Item {...formItemLayout} label="参数类型">
            {getFieldDecorator('paramType', {
              initialValue: record.id ? record.paramType : undefined,
              rules: [{ required: true, message: this.$t("common.please.select") }],
            })(
              <Select
                placeholder={this.$t("common.please.select")}
                allowClear
                disabled={disabled || !!record.id}
                getPopupContainer={node => node.parentNode}
              >
                {
                  (paramType || []).map(item => (
                    <Select.Option key={item.value}>{item.name}</Select.Option>
                  ))
                }
              </Select>
            )}
          </Form.Item>
          {
            (
              getFieldValue("paramType")
              && getFieldValue("paramType").toLowerCase() === 'text'
            ) ? (
                <Form.Item {...formItemLayout} label="手动选择:">
                  {getFieldDecorator('manualFlag', {
                    valuePropName: 'checked',
                    initialValue: record.id ? record.manualFlag : true
                  })(
                    <Switch
                      style={{ marginRight: '8px' }}
                      disabled={(record.id && record.paramRangeType)}
                    /> // 编辑状态下，如果参数来源类型有值则禁用，如果无值，表示上一次该状态为禁用，可以设为启用
                  )}
                  {getFieldValue('manualFlag') ? "启用" : "禁用"}
                </Form.Item>
              ) : (<></>)
          }
          <Form.Item {...formItemLayout} label="状态:">
            {getFieldDecorator('enabled', {
              valuePropName: 'checked',
              initialValue: record.id ? record.enabled : true
            })(
              <Switch style={{ marginRight: '8px' }} disabled={disabled} />
            )}
            {getFieldValue('enabled') ? "启用" : "禁用"}
          </Form.Item>

          <span>参数来源</span>
          <Divider />
          <Form.Item {...formItemLayout} label="来源类型">
            {getFieldDecorator('variableSourceType', {
              initialValue: record.id ? record.variableSourceType : undefined,
              rules: [{ required: true, message: this.$t("common.please.select") }],
            })(
              <Select
                placeholder={this.$t("common.please.select")}
                allowClear
                disabled={disabled || !!record.id}
                getPopupContainer={node => node.parentNode}
                onChange={value => this.getParamsFieldValueList(getFieldValue('structRank'), value)}
              >
                <Select.Option value="dataField">数据结构字段</Select.Option>
                <Select.Option value="api">接口取值</Select.Option>
              </Select>
            )}
          </Form.Item>
          { // 级联选择器
            getFieldValue("variableSourceType") ?
              (
                getFieldValue("variableSourceType") === "dataField" ? (
                  <Form.Item {...formItemLayout} label="匹配字段">
                    {getFieldDecorator('variableSourceInfoData', {
                      initialValue: record.id && record.variableSourceInfo ?
                        [record.variableSourceInfo.structCode, record.variableSourceInfo.fieldCode]
                        : undefined,
                      rules: [{ required: true, message: this.$t("common.please.select") }],
                    })(
                      <Cascader
                        mode="multiple"
                        placeholder={this.$t("common.please.select")}
                        allowClear
                        disabled={disabled || !!record.id}
                        options={fieldValueList}
                        getPopupContainer={node => node.parentNode}
                        onFocus={this.messageTipAboutInfoApi}
                      />
                    )}
                  </Form.Item>
                ) : (
                    <>
                      <Form.Item {...formItemLayout} label="来源接口">
                        {getFieldDecorator('variableSourceInfoApi', {
                          initialValue: record.id && record.variableSourceInfo
                            && record.variableSourceInfo.showCascadeApi
                            ? record.variableSourceInfo.showCascadeApi
                            : undefined,
                          rules: [{ required: true, message: this.$t("common.please.select") }],
                        })(
                          <Cascader
                            mode="multiple"
                            placeholder={this.$t("common.please.select")}
                            allowClear
                            disabled={disabled || !!record.id}
                            options={apiFieldValueList}
                            getPopupContainer={node => node.parentNode}
                            onChange={value => { this.onChangeApiInfo(value) }}
                          />
                        )
                        }
                      </Form.Item>
                      {
                        getFieldValue("variableSourceType") === 'api' ? (
                          apiSpinLoading ? <Spin delay={500} /> : this.renderApiParamsInput()
                        ) : null
                      }
                    </>
                  )
              ) : null
          }
          {
            !!getFieldValue("manualFlag")
              && getFieldValue("paramType").toLowerCase() === 'text'
              ? (
                <>
                  <span>参数范围</span>
                  <Divider />
                  <Form.Item {...formItemLayout} label="参数来源类型">
                    {getFieldDecorator('paramRangeType', {
                      initialValue: record.id ? record.paramRangeType : undefined,
                      rules: [{ required: true, message: this.$t("common.please.select") }],
                    })( // 编辑状态下，如果本身无值，且手动选择为true则允许修改，不禁用, disabled 表示是否是系统预设
                      <Select
                        placeholder={this.$t("common.please.select")}
                        allowClear
                        disabled={!(record.id && getFieldValue('manualFlag') && !record.paramRangeType)}
                        onChange={value => {
                          resetFields(['paramRangeInfo'])
                          this.getLOVOrSysSourceList(value)
                        }}
                        getPopupContainer={node => node.parentNode}
                      >
                        <Select.Option value="sysCode">值列表</Select.Option>
                        <Select.Option value="lov">Lov取值</Select.Option>
                      </Select>
                    )}
                  </Form.Item>
                  {
                    getFieldValue("paramRangeType") ? (
                      getFieldValue("paramRangeType") === 'lov' ? (
                        <>
                          <Form.Item {...formItemLayout} label="LOV">
                            {getFieldDecorator('paramRangeInfo', {
                              initialValue: record.id && record.paramRangeInfo
                                ? record.paramRangeInfo.lovCode
                                : undefined,
                              rules: [{ required: true, message: this.$t("common.please.select") }],
                            })(
                              <Select
                                showSearch
                                placeholder={this.$t("common.please.select")}
                                allowClear
                                disabled={!(record.id && getFieldValue('manualFlag') && !record.paramRangeType)}
                                onChange={(value, option) => { if (option) { this.getLOVSourceItem(option.key) } }}
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                                getPopupContainer={node => node.parentNode}
                              >
                                {
                                  Array.isArray(LovSourceList) && LovSourceList.map(item => (
                                    <Select.Option value={item.lovCode} key={item.apiId}>
                                      {item.lovName}
                                    </Select.Option>
                                  ))
                                }
                              </Select>
                            )}
                          </Form.Item>
                          {
                            getFieldValue("paramRangeType") === 'lov'
                            && getFieldValue("paramRangeInfo")
                            && this.renderParamsInput()
                          }
                        </>
                      ) : (
                          <>
                            <Form.Item {...formItemLayout} label="值列表">
                              {getFieldDecorator('paramRangeInfo', {
                                initialValue: record.id && record.paramRangeInfo ? record.paramRangeInfo.sysCode : undefined,
                                rules: [{ required: true, message: this.$t("common.please.select") }],
                              })(
                                <Select
                                  placeholder={this.$t("common.please.select")}
                                  allowClear
                                  disabled={!(record.id && getFieldValue('manualFlag') && !record.paramRangeType) || disabled}
                                  getPopupContainer={node => node.parentNode}
                                >
                                  {
                                    Array.isArray(LovSourceList) && LovSourceList.map(item => (
                                      <Select.Option value={item.code} key={item.code}>{item.name}</Select.Option>
                                    ))
                                  }
                                </Select>
                              )}
                            </Form.Item>
                          </>
                        )
                    ) : null
                  }
                </>
              ) : null
          }
        </Form>
        <div className="slide-footer">
          {!disabled &&
            <Button type="primary" onClick={this.handleSubmit} loading={saveLoading}>
              {this.$t('common.save')}
            </Button>}
          <Button onClick={this.handleCancel}>
            {this.$t('common.cancel')}
          </Button>
        </div>
      </>
    );
  }
}
// export default Form.create()(NewWorkflowtypeDefonition);

const ConditionparametersForm = Form.create()(NewWorkflowtypeDefonition)
export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(ConditionparametersForm);
