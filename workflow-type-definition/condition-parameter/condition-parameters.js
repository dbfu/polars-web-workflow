
import React from 'react';
import CustomTable from 'widget/custom-table';
import { Badge, Button, message } from 'antd';
import { connect } from 'dva';
import config from 'config';
import SlideFrame from 'widget/slide-frame';
import SearchArea from 'widget/search-area';
import NewRole from './new';
import service from '../service';

class WorkflowTypeDefinition extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editParams: {},
      visibel: false,
      formItems: [
        {
          label: '参数代码',// 代码
          id: 'paramCode',
          placeholder: '请输入',
          type: 'input',
        },
        {
          label: '参数名称',// 名称
          id: 'paramName',
          placeholder: '请输入',
          type: 'input',
        },
        {
          label: '参数类型',
          id: 'paramType',
          placeholder: '请选择',
          type: 'value_list',
          options: [],
          valueListCode: 'WORKFLOW_PARAMETERS_TYPE',
        },
        {
          label: '参数级别',
          id: 'structRank',
          type: 'value_list',
          placeholder: '请选择',
          options: [],
        },
      ],
      columns: [
        {
          title: '参数代码',// 代码
          align: 'center',
          dataIndex: 'paramCode',

        },
        {
          title: '参数名称',// 名称
          align: 'center',
          dataIndex: 'paramName',
          tooltips: true,
        },
        {
          title: '参数级别', // 参数级别
          align: 'center',
          dataIndex: 'structRankName',
          tooltips: true,
        },
        {
          title: '参数类型',// 名称
          align: 'center',
          dataIndex: 'paramTypeName',
          tooltips: true,
        },
        {
          title: '参数说明',// 名称
          align: 'center',
          dataIndex: 'structRemark',
          tooltips: true,
        },
        {
          title: '状态',
          dataIndex: 'enabled',
          align: 'center',
          render: desc => <Badge status={desc ? 'success' : 'error'} text={desc ? '启用' : '禁用'} />,
        },

        {
          title: this.$t({ id: 'common.operation' }),// 操作
          dataIndex: 'option',
          align: 'center',
          render: (value, record) => {
            const { params, currentUser } = this.props
            return (
              <span>
                {currentUser.tenantId === '0' && <span><a onClick={() => this.handleEdit(record)}>编辑</a> </span>}
                {(params.systemFlag === "false" && (currentUser.tenantId === record.tenantId)) ? <a onClick={() => this.handleEdit(record)}>编辑</a> : (currentUser.tenantId === '0' ? null : <a onClick={() => this.handleEdit(record)}>详情</a>)}
              </span>
            )
          },
        },
      ],
    };
  }

  componentDidMount() {
    const { formItems } = this.state;
    const { params } = this.props;

    service.getapprovamatching(params.id)
      .then(res => {
        const list = [];
        if (!res.data) {
          return;
        }
        res.data.map(item => {
          list.push({ value: item.structCode, label: item.structName });
          return undefined;
        });
        formItems[3].options = list;
        this.setState({ formItems });
      })
      .catch(err => {
        message.error(err.response.message);        
      });
  }

  // 新建
  handleOpenSlideFrameToAdd = () => {
    this.setState({
      visibel: true,
      editParams: {},
    })
  };

  // 编辑
  handleEdit = record => {
    this.setState({
      visibel: true,
      editParams: { ...record },
    })
  };

  search = values => {
    this.table.search(values);
  };

  handleCloseSlideFrame = flag => {
    this.setState({
      visibel: false,
      editParams: {},
    }, () => {
      if (flag) {
        this.table.search({});
      }
    })
  }

  render() {
    const { columns, visibel, formItems, editParams } = this.state;
    const { params, currentUser } = this.props; // params: 路由上的参数

    const disabled = currentUser.tenantId === '0' ? false : (params.systemFlag === 'true')

    return (
      <div style={{ backgroundColor: '#fff', padding: 10, overflow: 'auto' }}>

        <SearchArea
          searchForm={formItems}
          submitHandle={this.search}
        />
        {!disabled &&
          (
            <Button style={{ margin: '10px 0' }} onClick={this.handleOpenSlideFrameToAdd} type="primary">
              新建
            </Button>
          )

        }
        <CustomTable
          ref={ref => { this.table = ref }}
          columns={columns}
          url={`${config.wflUrl}/api/wfl/type/param`}
          params={{ typeId: params.id }}
        />
        <SlideFrame
          title={editParams.id ? '编辑条件参数' : '新建条件参数'}
          show={visibel}
          onClose={() => { this.handleCloseSlideFrame(false) }}
        >
          <NewRole params={editParams} typeId={params.id} systemFlag={params.systemFlag} onClose={(flag) => { this.handleCloseSlideFrame(flag) }} />
        </SlideFrame>


      </div>
    );
  }
}
export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(WorkflowTypeDefinition);
