import React from 'react';
import Table from 'widget/table';
import { Divider, Spin, Button, Form, Input, message, Checkbox, Select, Popconfirm } from 'antd';
import moment from 'moment';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import 'styles/workflow/workflow-steucture.less'
import service from '../../workflow-type-definition/service';
// import SelectMenus from './menus';
import ContentHeader from "widget/content-header"
import Content from 'widget/content'
import ContentScroller from 'widget/content-scroller'
import SearchArea from 'widget/search-area';
const { Option } = Select;
const FormItem = Form.Item;



class WorkflowtypeDefonition extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newShow: false,
      allocShow: false,
      roleId: '',
      record: {},
      versionNumber: '',
      recordKey: 0,
      tableData: [],
      getWorkflowfieldtype: [],
      loading: true,
      searchParams: {}, // 模糊查询数据搜索参数
      datalist: {},
      parentStructName: '',
      pagination: {
        total: 0,
        page: 0,
        pageSize: 10,
        current: 1,
        size: "small",
      },
      show: true,

      searchItems: [
        {
          label: '字段代码',// 代码
          id: 'fieldCode',
          placeholder: '请输入',
          type: 'input',
        },
        {
          label: '字段名称',// 名称
          id: 'fieldName',
          placeholder: '请输入',
          type: 'input',
        },
        {
          label: '字段类型',// 名称
          id: 'fieldType',
          placeholder: '请选择',
          type: 'value_list',
          valueListCode: 'WORKFLOW_FIELD_TYPE',
          options: [],
        },


      ],
      columns: [
        {
          title: '字段代码',// 代码
          align: 'center',
          // field_code/code/type
          dataIndex: 'fieldCode',
          editable: true,
          render: (value, record, index) => {
            const {
              form: { getFieldDecorator, getFieldValue },
            } = this.props;
            if (record.recordStatus === 'create' || record.recordStatus === 'edit') {
              return (
                <FormItem>
                  {getFieldDecorator(`${record.id}-fieldCode`, {
                    initialValue: record.fieldCode,
                    rules: [{ required: true, message: '字段代码为空' }],
                  })(<Input onChange={values => { this.onChange(values, index, 'fieldCode') }} placeholder="请输入" disabled={(record.id && record.recordStatus === 'edit')} />)}
                </FormItem>
              );
            } else {
              return value;
            }
          },
        },
        {
          title: '字段名称',// 代码
          align: 'center',
          dataIndex: 'fieldName',
          editable: true,
          render: (value, record, index) => {
            const {
              form: { getFieldDecorator, getFieldValue },
            } = this.props;
            if (record.recordStatus === 'create' || record.recordStatus === 'edit') {
              return (
                <FormItem>
                  {getFieldDecorator(`${record.id}-fieldName`, {
                    initialValue: record.fieldName,
                    rules: [{ required: true, message: '字段名称为空' }],
                  })(<Input onChange={(values) => this.onChange(values, index, 'fieldName')} placeholder="请输入" />)}
                </FormItem>
              );
            } else {
              return value;
            }
          },
        },
        {
          title: '字段类型',// 代码
          align: 'center',
          dataIndex: 'fieldTypeName',
          editable: true,
          render: (value, record, index) => {
            const { getWorkflowfieldtype, } = this.state;
            const {
              form: { getFieldDecorator, getFieldValue },
            } = this.props;
            if (record.recordStatus === 'create' || record.recordStatus === 'edit') {
              return (
                <FormItem>
                  {getFieldDecorator(`${record.id}-fieldType`, {
                    initialValue: record.fieldType,
                    rules: [{ required: true, message: '字段类型为空' }],
                  })(
                    <Select onChange={(value) => { this.onChange(value, index, 'fieldType') }} placeholder="请选择" style={{ width: '100%' }}>
                      {getWorkflowfieldtype.map(item => {
                        return (
                          <Select.Option key={item.value} >
                            {item.name}
                          </Select.Option>
                        );
                      })}
                    </Select>

                  )}
                </FormItem>
              );
            } else {
              return value;
            }
          },
        },
        {
          title: '必输',// 代码
          align: 'center',
          // required
          dataIndex: 'required',
          editable: true,
          render: (value, record, index) => {

            const {
              form: { getFieldDecorator, getFieldValue },
            } = this.props;
            if (record.recordStatus === 'create' || record.recordStatus === 'edit') {
              return (
                <FormItem>
                  {getFieldDecorator(`${record.id}-required`, {
                    initialValue: record.required,
                  })(<Checkbox onChange={(values) => { this.onChange(values, index, 'required') }} defaultChecked={record.required} />)}
                </FormItem>
              );
            } else {
              return <Checkbox checked={record.required} disabled />
            }
          },
        },
        {
          title: this.$t({ id: 'common.operation' }),// 操作
          dataIndex: 'option',
          align: 'center',
          render: (value, record, index) => {
            const { currentUser, form: { getFieldDecorator, getFieldValue } } = this.props

            return (
              <div>
                {
                  //currentUser.tenantId === record.tenantId &&
                  <span>
                    {
                      (record.recordStatus === 'create' || record.recordStatus === 'edit') ?
                        (<span>
                          <a onClick={(e) => this.recordSave(e, record, index)}>保存</a>
                          <Divider type="vertical" />
                          <a onClick={(e) => this.cancel(e, record, index)}>取消</a>
                        </span>)
                        :
                        (<span>
                          <a onClick={() => this.edit(record, index)}>编辑</a>
                          <Divider type="vertical" />
                          <Popconfirm
                            onConfirm={() => this.delete(record, index)}
                            title={this.$t('common.confirm.delete')}
                          >
                            <a>删除</a>
                          </Popconfirm>
                        </span>)
                    }
                  </span>
                }
              </div>
            );
          },
        },
      ],
    };
  }

  // alloc = record => {
  //   this.setState({ allocShow: true, roleId: record.id });
  // };

  componentWillMount = () => {
    const { columns } = this.state
    const { currentUser, match } = this.props
    const disabled = currentUser.tenantId === '0' ? false : (match.params.systemFlag === 'true')
    if (disabled) {
      columns.splice(4, 1)
      this.setState({ columns })
    }




  }

  componentDidMount = () => {
    const { record } = this.state
    const { superiorid, id } = this.props.match.params
    this.getList()
    this.getworkflowfieldtype(record)
    if (superiorid) {
      service.getListcode(superiorid).then(res => {
        this.setState({
          datalist: res.data,
          show: false
        })

      }).catch(err => {
        message.error(err.response.data.message);
      });

      service.getdataname(id).then(res => {
        console.log(res)
        this.setState({
          parentStructName: res.data,

        })
      }).catch(err => {
        message.error(err.response.data.message);
      });
    }
  }


  onChange = (values, index, item) => {
    if (values && item) {
      const { tableData } = this.state;
      tableData[index][item] = item === 'fieldType' ? values : (item === 'required' ? !values.target.value : values.target.value)
      this.setState({ tableData }, () => {
        console.log(tableData)
      })
    }
  }


  // 获取值列表
  getworkflowfieldtype = () => {
    this.getSystemValueList('WORKFLOW_FIELD_TYPE')
      .then(res => {
        this.setState({
          getWorkflowfieldtype: res.data.values,
        });
      })
      .catch(err => {
        message.error(err.response.data.message);
      });
  }


  // 删除
  delete = (record) => {
    const { superiorid } = this.props.match.params
    if (record) {
      service.deletedatastructure(record.id, superiorid).then(res => {
        message.success('操作成功！');
        // this.table.search();
        this.getList()
      })
        .catch(err => {
          message.error(err.response.data.message);
        })
    }
  }


  // 获取数据
  getList = () => {
    const { searchParams, pagination } = this.state;
    const id = this.props.match.params.id
    console.log(this.props.match.params)
    service.getfieldList(id, searchParams, pagination.page, pagination.pageSize).then(res => {
      const { data } = res
      this.setState({
        tableData: data,
        loading: false,
        show: false,
        pagination: {
          ...pagination,
          total: Number(res.headers['x-total-count'])
            ? Number(res.headers['x-total-count'])
            : 0,
          onChange: this.onChangePager,
          current: pagination.page + 1,
          pageSize: pagination.pageSize,
          onShowSizeChange: this.onShowSizeChange,
          showSizeChanger: true,
          showQuickJumper: true,
          showTotal: (total, range) =>
            this.$t('common.show.total', { range0: `${range[0]}`, range1: `${range[1]}`, total }),
        },

      })
    })
      .catch(err => {
        message.error(err.res.data.message);
      });


  }


  // 新建
  handadd = () => {
    const { tableData, recordKey } = this.state;
    let { pagination } = this.state;
    const total = pagination.total + 1;
    const pageSize = pagination.pageSize + 1;
    const value = {
      id: `create||${recordKey}`,
      fieldCode: '',
      fieldName: '',
      fieldType: '',
      required: true,
      recordStatus: 'create',
    };
    pagination = {
      ...pagination,
      pageSize,
      total,
    }

    tableData.unshift(value);
    const temp = recordKey + 1;
    this.setState({
      tableData,
      recordKey: temp,
      pagination,
    });

  };

  // 编辑
  edit = (record, index) => {
    const { tableData, versionNumber } = this.state
    tableData[index].recordStatus = 'edit';
    this.setState({ tableData, versionNumber: record.versionNumber });
  };

  recordSave = (e, record, index) => {
    const { versionNumber, tableData } = this.state
    const { form } = this.props;
    form.validateFields((error, values) => {
      if (!error) {

        if (tableData && tableData[index].recordStatus === "edit") {
          let paramsValues = {
            ...values,
          }
          const singleParam = {};
          const id = this.props.match.params.id

          for (let i in paramsValues) {
            const v = i.substr(i.indexOf('-') + 1)
            // let params = v + ':' + paramsValues[i]
            singleParam[v] = paramsValues[i]
          }
          singleParam.structId = id;
          singleParam.id = record.id
          singleParam.versionNumber = versionNumber
          service.editdataStructrue(singleParam).then(res => {
            message.success('编辑成功')
            tableData[index].recordStatus = ''
            this.setState({ tableData, loading: false }, () => { this.getList() })
          })
            .catch((err) => {
              message.error(err.response.data.message);
            })
        } else if (tableData && tableData[index].recordStatus === "create") {
          const typeId = this.props.match.params.id

          // let result = Object.keys(values).reduce((prev, cur) => {
          //   console.log(cur.split('-'))
          //   const [rowId, column] = cur.split("-");
          //   const flag = cur.indexOf('||') != -1;
          //   if (prev[rowId]) {
          //     prev[rowId][column] = values[cur];
          //   } else {
          //     prev[rowId] = flag ? {} : { id: rowId, versionNumber };
          //     prev[rowId][column] = values[cur];
          //   }
          //   // hand@300170
          //   return prev;
          // }, {})
          // result = Object.values(result).map(item => ({ ...item, structId: id }));
          // console.log(result)
          // const valuedata = result.reverse()
          // console.log(valuedata[index], index,valuedata)
          tableData[index].structId = typeId
          const { recordStatus, id, ...params } = tableData[index]
          service.newdataStructrue(params).then(res => {
            console.log(index, tableData);
            tableData[index] = res.data
            message.success('新建成功')
            if (tableData[index].fieldType === 'TEXT') { tableData[index].fieldTypeName = '文本' } else { tableData[index].fieldTypeName = '数字' }
            this.setState({
              tableData,
              loading: false,
              pagination: {
                total: 0,
                page: 0,
                pageSize: 10,
                current: 1,
                size: "small",
              },
            })
          })
            .catch((err) => {
              message.error(err.response.data.message);
            })
        }
      }
    });


  };

  // 全部保存
  recordallSave = () => {
    const { versionNumber, tableData } = this.state
    const { form } = this.props;
    const structId = this.props.match.params.id
    this.setState({}, () => {
      form.validateFields((error, values) => {
        console.log(values)
        if (error) return
        let result = Object.keys(values).reduce((prev, cur) => {
          const [rowId, column] = cur.split("-");
          const flag = cur.indexOf('||') != -1;
          if (prev[rowId]) {
            prev[rowId][column] = values[cur];
          } else {
            prev[rowId] = flag ? {} : { id: rowId, versionNumber };
            prev[rowId][column] = values[cur];
          }
          // hand@300170
          return prev;
        }, {})
        result = Object.values(result).map(item => ({ ...item, structId: structId }));
        service.recordallSave(result).then(res => {
          message.success('保存成功')
          tableData.map(item => {
            item.recordStatus = ''
          })
          this.setState({
            tableData,
            loading: false,
            pagination: {
              total: 0,
              page: 0,
              pageSize: 10,
              current: 1,
              size: "small",
            },
          }, () => { this.getList() })
        }).catch(error => {
          message.error(error.response.data.message);
        })

      })
    })


  }

  //取消行编辑
  cancel = (e, record, index) => {
    const { tableData, recordKey } = this.state;
    e.preventDefault();
    e.stopPropagation()
    if (record.recordStatus === 'create') {
      tableData.splice(index, 1);
      this.setState({ tableData });
    } else if (record.recordStatus === 'edit') {

      tableData[index] = record;
      tableData[index].recordStatus = '';
      this.setState({ tableData });
    }
  }

  // 详情
  Save = record => {
    e.preventDefault();
    e.stopPropagation()


  };



  close = flag => {
    this.setState({ newShow: false, record: {} }, () => {
      flag && this.table.reload();
    });
  };

  search = values => {
    // this.table.search(values);
    const { searchParams, pagination } = this.state

    this.setState({
      searchParams: values,
      pagination: {
        ...pagination,
        page: 0,
      },
    }, () => {
      this.getList();
    });
  };


  /**
  * 分页点击
  */
  onChangePager = pagination => {
    const temp = {};
    temp.page = pagination.current - 1;
    temp.current = pagination.current;
    temp.pageSize = pagination.pageSize;
    this.setState(
      {
        pagination: temp,
      },
      () => {
        this.getList();
      }
    );
  };

  /**
  * 改变每页显示的条数
  */
  onShowSizeChange = (current, pageSize) => {
    const temp = {};
    temp.page = current - 1;
    temp.pageSize = pageSize;
    this.setState(
      {
        pagination: temp,
      },
      () => {
        this.getList();
      }
    );
  };

  empty = () => {
    this.search({})
  }


  onBack = () => {
    this.$pushPage({
      functionCode: "workflow_type_definition",
      pageCode: 'workflow_type_definition_detail',
      params: [],
    });

  }



  render() {
    const { show, columns, newShow, allocShow, roleId, searchItems, parentStructName, tableData, loading, datalist, pagination } = this.state;
    const { currentUser, match } = this.props
    const disabled = currentUser.tenantId === '0' ? false : (match.params.systemFlag === 'true')
    return (
      <div className='work-type-define'  >
        <ContentHeader title="工作流类型定义详情" showBack onBack={this.onBack} />
        <ContentScroller>
          <Content>
            {show ? <Spin /> : <h3 className="header-title">{datalist.typeCode + '-' + datalist.typeName + '-' + (parentStructName.structName || '')}</h3>}

            {/* <SearchForm formItems={formItems} search={this.search} /> */}
            <SearchArea
              searchForm={searchItems}
              submitHandle={this.search}
            />
            {
              !disabled &&
              (
                <span>
                  <Button style={{ margin: '10px 0' }} onClick={this.handadd} type="primary">
                    新建
                  </Button>
                  <Button disabled={tableData[0] ? !(tableData[0].recordStatus === 'create' || tableData[0].recordStatus === 'edit') : false} style={{ marginLeft: 20 }} onClick={this.recordallSave} type="primary">
                    保存
                  </Button>
                </span>
              )
            }
            <Table
              ref={ref => (this.table = ref)}
              columns={columns}
              onRow={(record, index) => {
                return {

                };
              }}
              rowKey={record => {
                const { recordKey } = record;
                return `${record.id}|${recordKey}`;
              }}
              dataSource={tableData}
              pagination={pagination}
              loading={loading}
              onChange={this.onChangePager}
              size="small"

            />
          </Content>
        </ContentScroller>
      </div >
    );
  }
}
export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(Form.create()(WorkflowtypeDefonition));
