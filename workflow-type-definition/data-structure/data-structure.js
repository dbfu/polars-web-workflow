import React from 'react';
import CustomTable from 'widget/custom-table';
import { Divider, Badge, Button, message, Alert } from 'antd';
import moment from 'moment';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import config from 'config';

import service from '../service';
import EditRole from './edit';
// import SelectMenus from './menus';

import SearchArea from 'widget/search-area';


class WorkflowtypeDefonition extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newShow: false,
      allocShow: false,
      roleId: '',
      record: {},
      formItems: [
        {
          label: '表代码',// 代码
          id: 'structCode',
          placeholder: '请输入',
          type: 'input',
        },
        {
          label: '表名称',// 名称
          id: 'structName',
          placeholder: '请输入',
          type: 'input',
        },

      ],
      columns: [
        {
          title: '数据结构表代码',// 代码
          align: 'center',
          dataIndex: 'structCode',

        },
        {
          title: '数据结构表名称',// 名称
          align: 'center',
          dataIndex: 'structName',
          tooltips: true,
        },
        {
          title: '上级来源数据',// 名称
          align: 'center',
          dataIndex: 'parentStructName',
          tooltips: true,

        },
        {
          title: '状态',
          dataIndex: 'enabled',
          align: 'center',
          render: desc => <Badge status={desc ? 'success' : 'error'} text={desc ? '启用' : '禁用'} />,
        },

        {

          title: this.$t({ id: 'common.operation' }),// 操作
          dataIndex: 'option',
          align: 'center',
          render: (value, record, index) => {
            const { params, currentUser } = this.props
            return (
              <span>
                {currentUser.tenantId === '0' && <span><a onClick={(e) => this.edit(e, record)}>编辑</a> <Divider type="vertical" /></span>}
                {(params.systemFlag === "false" && (currentUser.tenantId === record.tenantId)) ?
                  <span>
                    <a onClick={(e) => this.edit(e, record)}>{this.$t("common.edit")}</a>
                    <Divider type="vertical" />
                    <a onClick={() => this.newfield(record, index)}>详情</a>
                  </span> :
                  (<a onClick={() => this.newfield(record, index)}>详情</a>)
                }
              </span>
            );
          },
        },
      ],
    };
  }


  // 新建
  add = () => {
    this.setState({ newShow: true });
  };

  // 编辑
  edit = (e, record) => {
    e.preventDefault()
    e.stopPropagation()
    this.setState({ record, newShow: true });
  };


  close = flag => {
    this.setState({ newShow: false, record: {} }, () => {
      if (flag) {
        this.table.search();

      }
    });
  };

  search = values => {
    this.table.search(values);
  };



  //详情
  newfield = (record, index) => {
    console.log(record, index)
    const { id } = this.props.params
    const { params, currentUser } = this.props

    this.$pushPage({
      functionCode: "workflow_type_definition",
      pageCode: 'workflow_type_definition_new',
      params: [record.id, id, params.systemFlag],
    });

  }

  render() {
    const { columns, newShow, allocShow, roleId, formItems, record } = this.state;
    const { params, currentUser } = this.props
    const disabled = currentUser.tenantId === '0' ? false : (params.systemFlag === 'true')

    return (
      <div style={{ backgroundColor: '#fff', padding: 10, overflow: 'auto' }}>
        {/* <SearchForm formItems={formItems} search={this.search} /> */}
        <SearchArea
          searchForm={formItems}
          submitHandle={this.search}
        />
        {!disabled &&
          (
            <Button style={{ margin: '10px 0' }} onClick={this.add} type="primary">
              新建
            </Button>
          )
        }
        <CustomTable
          ref={ref => (this.table = ref)}
          columns={columns}
          url={`${config.wflUrl}/api/wfl/type/struct`}
          params={{ typeId: params.id }}
        // onClick={(e) => this.newfield(e, record)}

        />
        <EditRole params={{ record, params }} visible={newShow} onClose={this.close} />
      </div >
    );
  }
}
export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(WorkflowtypeDefonition);
