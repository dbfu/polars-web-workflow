
import React from 'react';
import { Modal, Form, Switch, Input, message, Select } from 'antd';
import service from '../service'
import InputLanguage from "@/components/Widget/Template/input-language";


class NewworkflowtypeDefonition extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      // record: {}
      Sourcedata: [],
    };
  }




  componentWillReceiveProps(nextProps) {
    if (nextProps.visible) { this.getHigherdata() }
    this.setState({ visible: nextProps.visible });
  }


  getHigherdata = () => {
    const { params } = this.props;
    const { params: { id } } = params

    service.getHigherdata(id).then(res => {
      this.setState({
        Sourcedata: res.data,
      })
    })
      .catch(err => {
        message.error(err.response.data.message);
      })

  }

  handleSubmit = e => {
    const { form: { validateFieldsAndScroll }, onClose, params } = this.props
    e.preventDefault();
    validateFieldsAndScroll((error, values) => {
      if (error) return
      if (params.record.id) {
        const finalParams = {
          id: params.record.id,
          typeId: params.params.id,
          ...params.record,
          ...values,
        };
        const i18n = {};
        i18n.structName = values.structName.i18n;
        finalParams.structName = values.structName.value;
        finalParams.i18n = i18n;
        service.editdataStrusave(finalParams).then(() => {
          message.success('操作成功！');
          this.setState({})
          if (onClose) { onClose(true); }
        })
          .catch(err => {
            message.error(err.response.data.message);
          })
      } else {
        const finalParams = {
          typeId: params.params.id,
          ...values,
        }
        const i18n = {};
        i18n.structName = values.structName.i18n;
        finalParams.structName = values.structName.value;
        finalParams.i18n = i18n;
        service.getdataStructurenewsave(finalParams).then(() => {
          message.success('操作成功！');
          this.setState({})

          if (onClose) { onClose(true); }
        })
          .catch(err => {
            message.error(err.response.data.message);
          })
      }
    });
  };

  handleCancel = () => {
    const { onClose } = this.props;
    if (onClose) {
      onClose()
    }
  };


  render() {
    const { params } = this.props;
    const { form: { getFieldDecorator }, form: { getFieldValue } } = this.props
    const { visible, Sourcedata } = this.state;
    const formItemLayout = {
      labelCol: {
        span: 8,
      },
      wrapperCol: {
        span: 10,
      },
    };

    return (
      <Modal
        title={!params.record.id ? "新建数据结构" : "编辑数据结构"}
        visible={visible}
        onOk={this.handleSubmit}
        onCancel={this.handleCancel}
        destroyOnClose
      >
        <Form>
          <Form.Item {...formItemLayout} label="数据结构代码">
            {getFieldDecorator('structCode', {
              initialValue: params.record.id ? params.record.structCode : '',
              rules: [
                { required: true, message: '请输入' },
                {
                  pattern: /^[0-9a-zA-Z_]{1,}$/,
                  message: '只能输入英文数字下划线!',
                },
              ],
            })(<Input disabled={params.record.id} placeholder="请输入" />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="数据结构名称">
            {getFieldDecorator('structName', {
              initialValue: params.record ? { value: params.record.structName, i18n: params.record.i18n ? params.record.i18n.structName : [] } : '',
              rules: [{ required: true, message: '请输入' }],
            })(<InputLanguage placeholder="请输入" />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="上级数据结构">
            {getFieldDecorator('parentStructId', {
              initialValue: params.record ? params.record.parentStructId : undefined,
            })(
              <Select placeholder="请选择" disabled={params.record.id}>
                {Sourcedata.map(item => {
                  return (
                    <Select.Option key={item.id}>
                      {`${item.structName}-${item.structCode}`}
                    </Select.Option>
                  );
                })}
              </Select>
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label="状态:">
            {getFieldDecorator('enabled', {
              valuePropName: 'checked',
              initialValue: params.record.id ? params.record.enabled : true,
            })(
              <Switch />
            )}
            {getFieldValue('enabled') ? "启用" : "禁用"}
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}
export default Form.create()(NewworkflowtypeDefonition);
