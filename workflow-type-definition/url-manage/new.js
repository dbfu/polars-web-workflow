import React from "react";
import { Button, Divider, Form, Input, message, Select } from "antd";
import service from "../service";
import { connect } from "dva";
import { messages } from "utils/utils";
import Lov from "widget/Template/lov";

const {Option} = Select;


const formItemLayout = {
  labelCol: {
    span: 8
  },
  wrapperCol: {
    span: 10
  },
};

class NewWorkflowUrl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      record: {},
      paramType: [],
      showLov: true,
      urlTypeList: [{label: "PC", value: "PC"}, { label: "APP", value: "APP"}],
      urlKeyList: [{ label: "WORKFLOW", value: "WORKFLOW"},
        {label: "MESSAGE_DETAIL", value: "MESSAGE_DETAIL"}, {label: "MESSAGE_CENTRAL", value: "MESSAGE_CENTRAL"}],
    };
  }

  componentDidMount() {
      const {params} = this.props;
      if (params.id) {
        service.queryUrl(params.id)
          .then(res => {
              const showLov = res.data.urlType === "PC";
              const functionCode = res.data.urlKey === "WORKFLOW" ? "WORKFLOW_BACKLOG" : "PERIPHERAL_MESSAGE_NOTICE";
              this.setState({record : res.data, functionCode, showLov});
            }
          )
          .catch(error => {
            message.error(error.response.data.message);
            this.setState({ saveLoading: false });
          });
      }
  };

  changeUrlType = (value) => {
    if (value === "PC") {
      this.setState({showLov : true}, () =>
        this.props.form.setFieldsValue({url: undefined, pageContent: undefined}));
    } else {
      this.setState({showLov : false}, () =>
        this.props.form.setFieldsValue({url: undefined, pageContent: undefined}));
    }
   };

  changeUrlKey = (value) => {
    if (value === "WORKFLOW") {
      this.setState({functionCode : "WORKFLOW_BACKLOG"}, () =>
        this.props.form.setFieldsValue({url: undefined, pageContent: undefined}));
    } else {
      this.setState({functionCode : "PERIPHERAL_MESSAGE_NOTICE"}, () =>
        this.props.form.setFieldsValue({url: undefined, pageContent: undefined}));
    }
  };


  /** 提交 */
  handleSubmit = e => {
    e.preventDefault();
    const { form, onClose } = this.props;
    form.validateFieldsAndScroll((err, values) => {

      if (err) return;
      this.setState({ saveLoading: true });
      debugger
      const {record, showLov} = this.state;
      values.id = record.id;
      values.url = showLov ? values.pageContent.functionPageCode : values.url;
      values.typeId = this.props.typeId;
      values.versionNumber = record.versionNumber;

      let wflUrl;
      if (!record.id) {
        wflUrl = service.createWflUrl(values);
      } else {
        wflUrl = service.updateWflUrl(values);
      }
      wflUrl.then(() => {
          message.success("操作成功");
          this.setState({ saveLoading: false });
          if (onClose) {
            onClose(true);
          }
        })
        .catch(error => {
          message.error(error.response.data.message);
          this.setState({ saveLoading: false });
        })
    });
  };

  // 取消
  handleCancel = () => {
    this.props.onClose && this.props.onClose();
  };


  render() {
    const { form, currentUser, systemFlag } = this.props;
    const { getFieldDecorator } = form;
    const { urlTypeList, urlKeyList, record, showLov, saveLoading, functionCode } = this.state;

    const disabled = currentUser.tenantId === '0' ? false : (systemFlag === 'true');
    return (
      <>
        <Form>
          <span>基本信息</span>
          <Divider />
          <Form.Item {...formItemLayout} label="URL类型">
            {getFieldDecorator('urlType', {
              initialValue: record.id ? record.urlType : "PC",
              rules: [
                { required: true, message: '请输入' },
              ],
            })(<Select
              size="default"
              placeholder={this.$t('common.please.select')}
              onChange={this.changeUrlType}
              allowClear
              disabled={disabled}
            >
              {urlTypeList.map(item => <Option key={item.value}>{item.label}</Option>)}
            </Select>)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="URL类别">
            {getFieldDecorator('urlKey', {
              initialValue: record.id ? record.urlKey : undefined,
              rules: [
                { required: true, message: '请输入' },
              ],
            })(<Select
              size="default"
              placeholder={this.$t('common.please.select')}
              allowClear
              onChange={this.changeUrlKey}
              disabled={disabled}
            >
              {urlKeyList.map(item => <Option key={item.value}>{item.label}</Option>)}
            </Select>)}
          </Form.Item>
          {
            showLov && <Form.Item {...formItemLayout} label={messages('页面' /*页面*/)}>
            {getFieldDecorator('pageContent', {
              rules: [
                {
                  required: true,
                  message: messages('common.please.select' ), //请输入
                },
              ],
              initialValue: record.id ? {id: record.id, pageName: record.url, functionPageCode: record.url} : undefined,
            })(
              <Lov
                placeholder={messages('common.please.select')}
                code="page_list_lov"
                labelKey="pageName"
                valueKey="functionPageCode"
                extraParams={{functionCode: functionCode}}
                single={true}
                disabled={disabled}
              />
            )}
            </Form.Item>
          }
          {
            !showLov && <Form.Item {...formItemLayout} label={messages('页面' /*页面*/)}>
              {getFieldDecorator('url', {
                rules: [
                  {
                    required: true,
                    message: messages('common.please.select' ), //请输入
                  },
                ],
                initialValue: record.id ? record.url : undefined,
              })(
                <Input placeholder="请输入" disabled={disabled}/>
              )}
            </Form.Item>
          }


        </Form>
        <div className="slide-footer">
          <Button type="primary" onClick={this.handleSubmit} loading={saveLoading}>
            {this.$t('common.save')}
          </Button>
          <Button onClick={this.handleCancel}>
            {this.$t('common.cancel')}
          </Button>
        </div>
      </>
    );
  }
}


const NewWorkflowUrlForm = Form.create()(NewWorkflowUrl)
export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(NewWorkflowUrlForm);
