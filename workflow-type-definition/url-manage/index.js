import React from 'react';
import CustomTable from 'widget/custom-table';
import { Button } from 'antd';
import { connect } from 'dva';
import config from 'config';
import SlideFrame from 'widget/slide-frame';
import SearchArea from 'widget/search-area';
import NewUrl from './new';

class WorkflowUrlManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editParams: {},
      visible: false,
      formItems: [
        {
          label: 'URL类型',
          id: 'urlType',
          placeholder: '请输入',
          type: 'select',
          colSpan: 6,
          options: [{label: "PC", value: "PC"}, { label: "APP", value: "APP"}],
          labelKey: 'label',
          valueKey: 'value',
        },
        {
          label: 'URL类别',
          id: 'urlKey',
          placeholder: '请输入',
          type: 'select',
          colSpan: 6,
          options: [{ label: "WORKFLOW", value: "WORKFLOW"},
            {label: "MESSAGE_DETAIL", value: "MESSAGE_DETAIL"}, {label: "MESSAGE_CENTRAL", value: "MESSAGE_CENTRAL"}],
          labelKey: 'label',
          valueKey: 'value',
        },

      ],
      columns: [
        {
          title: 'URL类型',// 名称
          align: 'center',
          dataIndex: 'urlType',
          tooltips: true,
        },
        {
          title: 'URL类别',
          align: 'center',
          dataIndex: 'urlKey',
          tooltips: true,
        },
        {
          title: 'URL',
          align: 'center',
          dataIndex: 'url',
          tooltips: true,
        },
        {
          title: this.$t({ id: 'common.operation' }),// 操作
          dataIndex: 'option',
          align: 'center',
          render: (value, record) => {
            const { params, currentUser } = this.props;
            return (
              <span>
                {currentUser.tenantId === '0' && <span><a onClick={() => this.handleEdit(record)}>编辑</a> </span>}
                {(params.systemFlag === "false" && (currentUser.tenantId !== 0)) ?
                  <a onClick={() => this.handleEdit(record)}>编辑</a> : (currentUser.tenantId === '0' ? null :
                    <a onClick={() => this.handleEdit(record)}>详情</a>)}
              </span>
            )
          },
        },
      ],
    };
  }

  // 新建
  handleOpenSlideFrameToAdd = () => {
    this.setState({
      visible: true,
      editParams: {},
    })
  };

  // 编辑
  handleEdit = record => {
    this.setState({
      visible: true,
      editParams: { ...record },
    })
  };

  search = values => {
    this.table.search(values);
  };

  handleCloseSlideFrame = flag => {
    this.setState({
      visible: false,
      editParams: {},
    }, () => {
      if (flag) {
        this.table.search({});
      }
    })
  };

  render() {
    const { columns, visible, formItems, editParams } = this.state;
    const { params, currentUser } = this.props; // params: 路由上的参数
    const disabled = currentUser.tenantId === '0' ? false : (params.systemFlag === 'true');

    return (
      <div style={{ backgroundColor: '#fff', padding: 10, overflow: 'auto' }}>
        <SearchArea
          searchForm={formItems}
          submitHandle={this.search}
        />
        {!disabled &&
          (
            <Button style={{ margin: '10px 0' }} onClick={this.handleOpenSlideFrameToAdd} type="primary">
              新建
            </Button>
          )
        }
        <CustomTable
          ref={ref => { this.table = ref }}
          columns={columns}
          url={`${config.wflUrl}/api/wfl/type/url`}
          params={{ typeId: params.id }}
        />
        <SlideFrame
          title={editParams.id ? '编辑URL' : '新建URL'}
          show={visible}
          onClose={() => { this.handleCloseSlideFrame(false) }}
        >
          <NewUrl params={editParams} typeId={params.id} systemFlag={params.systemFlag}
                  onClose={(flag) => { this.handleCloseSlideFrame(flag) }} />
        </SlideFrame>
      </div>
    );
  }
}
export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(WorkflowUrlManage);
