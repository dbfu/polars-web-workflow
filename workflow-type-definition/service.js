import httpFetch from 'share/httpFetch'

import config from "config"

export default {

  // 查询数据结构详情列表
  getfieldList(id, params, page, size, ) {
    return httpFetch.get(`${config.wflUrl}/api/wfl/type/struct/field?page=${page}&size=${size}`, { ...params, structId: id });
  },

  //单独查询代码和code
  getListcode(id) {
    return httpFetch.get(`${config.wflUrl}/api/wfl/type/${id}`);
  },
  // 新建工作流
  getNewsave(list) {
    return httpFetch.post(`${config.wflUrl}/api/wfl/type`, list);
  },

  /** 编辑*/
  editSave(params) {
    return httpFetch.put(`${config.wflUrl}/api/wfl/type`, params);
  },

  // 新建数据结构
  getdataStructurenewsave(list) {
    return httpFetch.post(`${config.wflUrl}/api/wfl/type/struct`, list);
  },


  /** 编辑数据结构*/
  editdataStrusave(params) {
    return httpFetch.put(`${config.wflUrl}/api/wfl/type/struct`, params);
  },

  /** 获取上级数据来源的接口*/
  getHigherdata(id) {
    return httpFetch.get(`${config.wflUrl}/api/wfl/type/struct/list/${id}`);
  },

  /** 获取数据结构字段具体匹配的数据源 */
  getDataStructById(structCode, typeId) {
    return httpFetch.get(`${config.wflUrl}/api/wfl/type/struct/fieId/or/assign?structCode=${structCode}&typeId=${typeId}`)
  },




  /** 数据结构详情的新建*/
  newdataStructrue(params) {
    return httpFetch.post(`${config.wflUrl}/api/wfl/type/struct/field`, params);
  },

  /** 数据结构详情的编辑*/
  editdataStructrue(params) {
    return httpFetch.put(`${config.wflUrl}/api/wfl/type/struct/field`, params);
  },
  // 查询数据结构详情列表
  getList(id) {
    return httpFetch.get(`${config.wflUrl}/api/wfl/type/struct?typeId=${id}`);
  },


  /** 数据结构详情的全部保存*/
  recordallSave(params) {
    return httpFetch.post(`${config.wflUrl}/api/wfl/type/struct/field/list`, params);
  },


  getParamTypeById(id) {
    return httpFetch.get(`${config.wflUrl}/api/wfl/type/param/${id}`)
  },

  /**
   * 根据参数级别,来源类型获取字段数据源
   * @param {*} structCode 参数级别code
   */
  getParamsFieldValueList(structCode, typeId) {
    const url = `${config.wflUrl}/api/wfl/type/struct/fieId/or/parent?structCode=${structCode}&typeId=${typeId}`;
    return httpFetch.get(url);
  },


  getApiParamsFieldValueLit() {
    const url = `${config.baseUrl}/api/lov/page?page=0&size=9999&roleType=TENANT`;
    return httpFetch.get(url);
  },

  // 查询来源接口字段
  getSourceInterface() {
    const url = `${config.baseUrl}/api/interface/query/keyword`
    return httpFetch.get(url)
  },


  // 获取系统值列表
  getSysSourceList() {
    const url = `${config.baseUrl}/api/custom/enumerations?page=0&size=9999&enabled=true&roleType=TENANT`
    return httpFetch.get(url)
  },
  // 获取系统lov
  getLOVSourceList() {
    const url = `${config.baseUrl}/api/lov/page?page=0&size=9999&roleType=TENANT`
    return httpFetch.get(url)
  },
  // 获取lov详情
  getLOVSourceItem(id) {
    const url = `${config.baseUrl}/api/lov/${id}`
    return httpFetch.get(url)
  },

  /**
   * 新建，更新参数
   * @param {*} params
   */
  saveNewRangeValue(params) {
    const url = `${config.wflUrl}/api/wfl/type/param/save`;
    return httpFetch.post(url, params)
  },


  // 新建审批条件的
  newapprovalSave(params) {
    const url = `${config.wflUrl}/api/wfl/condition/save`;
    return httpFetch.post(url, params)
  },

  // 删除审批条件的

  approvalDelete(id) {
    return httpFetch.delete(`${config.wflUrl}/api/wfl/condition/${id}`)
  },



  // 新建审批规则接口
  newapprovarulesSave(params) {
    const url = `${config.wflUrl}/api/wfl/aproval/rule/save`;
    return httpFetch.post(url, params)
  },


  getapprovarules() {
    const url = `${config.baseUrl}/api/application?page=0&size=9999`;
    return httpFetch.get(url)
  },

  getapprovarulesid(appId) {
    const url = `${config.baseUrl}/api/interface/query/?page=0&size=999&appId=${appId}`;
    return httpFetch.get(url)
  },



  //通过id查询渲染
  getappproveRender(interfaceId) {
    const url = `${config.baseUrl}/api/interfaceRequest/query?page=0&size=999&interfaceId=${interfaceId}`;
    return httpFetch.get(url)
  },

  // 查询匹配字段
  getapprovamatching(typeId) {
    const url = `${config.wflUrl}/api/wfl/type/struct/data/structField?typeId=${typeId}`;
    return httpFetch.get(url)
  },

  // 查询数据结构的表名称
  getdataname(id) {
    const url = `${config.wflUrl}/api/wfl/type/struct/${id}`;
    return httpFetch.get(url)
  },





  // 删除数据结构表
  deletedatastructure(id, typeId) {
    return httpFetch.delete(`${config.wflUrl}/api/wfl/type/struct/field/${id}?typeId=${typeId}`)
  },

  // 创建工作流 url
  createWflUrl(dto) {
    const url = `${config.wflUrl}/api/wfl/type/url`;
    return httpFetch.post(url, dto);
  },

  // 更新工作流 url
  updateWflUrl(dto) {
    const url = `${config.wflUrl}/api/wfl/type/url`;
    return httpFetch.put(url, dto);
  },

  //查询工作流 url
  queryUrl(id){
    return httpFetch.get(`${config.wflUrl}/api/wfl/type/url/${id}`);
  },


};
