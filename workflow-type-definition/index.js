import React from 'react';
import CustomTable from 'components/Template/custom-table';
import { Divider, Badge, Button, message, Alert } from 'antd';
import moment from 'moment';
import { connect } from 'dva';
import { routerRedux } from 'dva/router';
import config from 'config';

import service from './service';
import NewRole from './new';
// import SelectMenus from './menus';
import ContentHeader from "widget/content-header"
import Content from 'widget/content'
import ContentScroller from 'widget/content-scroller'
import SearchArea from 'widget/search-area';


class WorkflowtypeDefonition extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newShow: false,
      allocShow: false,
      roleId: '',
      record: {},
      formItems: [
        {
          label: '类型代码',// 代码
          id: 'typeCode',
          placeholder: '请输入',
          type: 'input',
        },
        {
          label: '类型名称',// 名称
          id: 'typeName',
          placeholder: '请输入',
          type: 'input',
        },

      ],
      columns: [
        {
          title: '工作流类型代码',// 代码
          align: 'center',
          dataIndex: 'typeCode',

        },
        {
          title: '工作流类型名称',// 名称
          align: 'center',
          dataIndex: 'typeName',
          tooltips: true,
        },
        {
          title: '状态',
          dataIndex: 'enabled',
          align: 'center',
          render: desc => <Badge status={desc ? 'success' : 'error'} text={desc ? '启用' : '禁用'} />,
        },
        {
          title: '创建方式',// 创建日期
          align: 'center',
          dataIndex: 'systemFlag',
          render: record => {
            return (
              <span>
                {record ? '系统预设' : '用户自定义'}
              </span>
            )
          },
        },
        {
          title: this.$t({ id: 'common.operation' }),// 操作
          dataIndex: 'option',
          align: 'center',
          render: (value, record) => {
            const { currentUser } = this.props
            return (

              // <span>
              //   {!record.systemFlag && (this.props.currentUser.tenantId == record.tenantId) ?
              //     <a onClick={() => this.edit(record)}>{this.$t("common.edit")}</a>
              //     : null
              //   }
              //   {currentUser.tenantId === '0' && <span><a onClick={() => this.edit(record)}>编辑</a> <Divider type="vertical" /></span>}
              //   {(record.systemFlag === "false") ? <a onClick={() => this.edit(record)}>编辑</a> : (currentUser.tenantId === '0' ? null : <a onClick={() => this.new(record)}>详情</a>)}
              // </span>
              <span>
                {/* 当前租户非admin时,且systeFlag 为 true(系统预设),则没有编辑 */}
                {currentUser.tenantId === '0' &&
                  <span>
                    <a onClick={() => this.edit(record)}>{this.$t("common.edit")}</a>
                    <Divider type="vertical" />
                  </span>
                }
                {!record.systemFlag ?
                  <a onClick={() => this.edit(record)}>{this.$t("common.edit")}</a>
                  : null
                }
                <span>
                  {!record.systemFlag && <Divider type="vertical" />}
                  <a onClick={() => this.new(record)}>详情</a>
                </span>
              </span>
            );
          },
        },
      ],
    };
  }

  // 新建
  add = () => {
    this.setState({ newShow: true });
  };

  // 详情
  new = record => {
    this.$pushPage({
      functionCode: "workflow_type_definition",
      pageCode: 'workflow_type_definition_detail',
      params: [record.id, record.systemFlag]
    });

  };

  // 编辑
  edit = record => {
    this.setState({ record, newShow: true });
  };


  close = flag => {
    this.setState({ newShow: false, record: {} }, () => {
      flag && this.table.reload();
    });
  };

  search = values => {
    this.table.search(values);
  };



  render() {
    const { columns, newShow, allocShow, roleId, formItems, record } = this.state;
    return (
      <div style={{ backgroundColor: '#fff', padding: 10, overflow: 'auto' }}>
        <ContentHeader title="工作流类型定义" />
        <ContentScroller>
          <Content>
            <SearchArea
              searchForm={formItems}
              submitHandle={this.search}
              // clearHandle={this.empty}
            />
            <Button style={{ margin: '10px 0' }} onClick={this.add} type="primary">
              新建
            </Button>
            <CustomTable
              ref={ref => (this.table = ref)}
              columns={columns}
              url={`${config.wflUrl}/api/wfl/type`}
            />
            <NewRole params={record} visible={newShow} onClose={this.close} />
          </Content>
        </ContentScroller>
      </div >
    );
  }
}
export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(WorkflowtypeDefonition);
