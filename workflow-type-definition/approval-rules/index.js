
import React, { Component } from 'react';
import CustomTable from 'widget/custom-table';
import { Badge, Button, Divider } from 'antd';
import config from 'config';
import SearchArea from 'widget/search-area';
import SlideFrame from 'widget/slide-frame';
import NewRole from './new';
import { connect } from 'dva';



class ApprovalRules extends Component {
  constructor(props) {
    super(props);
    this.state = {
      record: {},
      visibel: false,
      searchFormItems: [
        {
          label: '审批规则代码',
          id: 'approvalRuleCode',
          placeholder: this.$t("common.please.enter"),
          type: 'input',
        },
        {
          label: '审批规则名称',// 名称
          id: 'approvalRuleName',
          placeholder: this.$t("common.please.enter"),
          type: 'input',
        },
        {
          label: '状态',// 名称
          id: 'enabled',
          placeholder: this.$t("common.please.select"),
          type: 'select',
          options: [
            { label: '启用', value: true },
            { label: '禁用', value: false }
          ]
        },
      ],
      columns: [
        {
          title: '审批规则代码',// 代码
          align: 'center',
          dataIndex: 'approvalRuleCode',

        },
        {
          title: '审批规则名称',// 名称
          align: 'center',
          dataIndex: 'approvalRuleName',
          tooltips: true,
        },
        {
          title: '描述',// 名称
          align: 'center',
          dataIndex: 'description',
          tooltips: true,
        },
        {
          title: '审批规则说明',// 名称
          align: 'center',
          dataIndex: 'approvalDescription',
          tooltips: true,
        },
        {
          title: '状态',
          dataIndex: 'enabled',
          align: 'center',
          render: desc => <Badge status={desc ? 'success' : 'error'} text={desc ? '启用' : '禁用'} />,
        },
        {
          title: '创建方式',// 名称
          align: 'center',
          dataIndex: 'createType',
          render: (value) => {
            return (
              <span>
                {value === 'userDefinition' ? '用户自定义' : '系统预设'}
              </span>
            );
          },
        },
        {
          title: this.$t({ id: 'common.operation' }),// 操作
          dataIndex: 'option',
          align: 'center',
          render: (value, record) => {
            const { params, currentUser } = this.props
            return (
              <span>
                {currentUser.tenantId === '0' && <span><a onClick={(e) => this.edit(e, record)}>编辑</a></span>}
                {(params.systemFlag === "false") ? <a onClick={(e) => this.edit(e, record)}>编辑</a> : (currentUser.tenantId === '0' ? null : <a onClick={(e) => this.new(e, record)}>详情</a>)}
              </span>
            );
          },
        },
      ],
    }
  }

  /*编辑*/
  edit = (e, record) => {
    this.setState({
      visibel: true,
      record,
    })

  }

  // 详情
  new = (e, record) => {
    console.log(record)
    this.setState({
      visibel: true,
      record,
    })

  }



  // 搜素
  handleSearch = (values) => {
    this.table.search(values);
  }





  handleCloseSlideFrame = flag => {
    this.setState({
      visibel: false,
    }, () => {
      if (flag) {
        this.table.search({});
      }
    })
  }


  // 新建
  handleNewApprovalValue = () => {
    this.setState({
      visibel: true,
      record: {},
    })
  };


  render() {
    const { searchFormItems, columns, record, visibel } = this.state;
    const { params, currentUser, systemFlag } = this.props;
    const disabled = currentUser.tenantId === '0' ? false : (params.systemFlag === 'true')
    return (
      <div>
        <SearchArea
          searchForm={searchFormItems}
          submitHandle={this.handleSearch}
        />

        {!disabled &&
          <Button style={{ margin: '10px 0' }} onClick={this.handleNewApprovalValue} type="primary">
            新建
        </Button>}




        <CustomTable
          ref={ref => { this.table = ref; return this.table }}
          columns={columns}
          url={`${config.wflUrl}/api/wfl/aproval/rule`}
          onRowClick={this.detail}
          params={{ typeId: params.id }}
        />
        <SlideFrame
          title={record.id ? '编辑审批规则' : '新建审批规则'}
          show={visibel}
          onClose={() => { this.handleCloseSlideFrame(false) }}
        >
          <NewRole params={record} typeId={params.id} systemFlag={params.systemFlag} onClose={(flag) => { this.handleCloseSlideFrame(flag) }} />
        </SlideFrame>
      </div>
    )
  }
}
export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(ApprovalRules);
