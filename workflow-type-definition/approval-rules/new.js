import React from 'react';
import { Divider, Form, Switch, Input, Icon, message, Button, Select, Row, Col, DatePicker, InputNumber, Cascader } from 'antd';
import ListSelector from 'widget/list-selector';
import config from 'config';
import moment from "moment";
import { connect } from 'dva';
import InputLanguage from "@/components/Widget/Template/input-language";

import service from '../service';
const { TextArea } = Input;
const { Option } = Select;

class Conditionparameters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      record: {},
      approval: false,
      approvalInterfacelist: [],
      approvalRulesItem: [],
      approvalRulesRender: [],
      pprovaMatching: [],
      // lov接口数据
      LovSourceList: [],
      // lovFiedName集合
      LovItemList: [],
      fieldValueList: [],
      // 值列表集合数据
      sysCodeSourceList: [],
    };
  }


  componentDidMount = () => {
    const { params } = this.props
    this.getapprovalInterface()
    if (params.id) {
      const approvalRules = JSON.parse(params.ruleInterfaceInfo)

      this.getapply(params.apply)
      this.getParamsFieldValueList('dataField')
      // 值列表和lov
      this.getDefaultLOVOrSysSourceList()

      // 赋予lov参数初始值
      const LovItemList = [];
      approvalRules.map((item, index) => {
        if (item.sourceInfoType && item.sourceInfoType === 'LOV') {
          const tempArr = [];

          if (Array.isArray(item.parameters)) {
            item.parameters.forEach((item, index) => {
              const temp = {
                keyCode: Object.keys(item)[0],
                value: Object.values(item)[0],
                name: item.tempName
              }
              tempArr.push(temp)
            })
            LovItemList[index] = tempArr
            this.setState({ LovItemList })
          }
        }
      })

      this.setState({
        approvalRulesRender: approvalRules,
        LovItemList,
      })

    }
  }


  componentWillReceiveProps(nextProps) {
    this.setState({ visible: nextProps.visible });
  }


  // 获取审批接口
  getapprovalInterface = () => {
    service.getapprovarules().then(res => {
      this.setState({
        approvalInterfacelist: res.data,
      })
    })
      .catch(err => {
        message.error(err.response.data.message);
      })
  };

  /**
 * 获取对应lov的参数数据
 * @param id: 取值lov时的lovId
 */
  getapply = id => {
    this.setState({ approvalRulesRender: [] })

    if (id) {
      service
        .getapprovarulesid(id)
        .then(res => {
          this.setState({ approvalRulesItem: res.data || [] })
        })
        .catch(err => {
          console.log(err);
          message.error(err.response.data.message)

        })
    }

  }


  getapprovalRulePort = id => {
    // approvalRulesRender
    if (id) {
      service
        .getappproveRender(id)
        .then(res => {
          this.setState({ approvalRulesRender: res.data || [] })
        })
        .catch(err => {
          console.log(err);
          message.error(err.response.data.message)
        })
    }

  }

  // 查询匹配字段
  getapprovamatching = () => {
    const { typeId } = this.props
    service.getapprovamatching(typeId).then(res => {
      this.setState({
        pprovaMatching: res.data || [],

      })
    })
      .catch(err => {
        message.error(err.response.data.message)
      })

  }

  // 获取默认系统的lov和值列表
  getDefaultLOVOrSysSourceList = () => {
    service.getLOVSourceList()
      .then(res => {
        if (res.data) {
          this.setState({ LovSourceList: res.data })
        }
      })
      .catch(() => { })

    service.getSysSourceList()
      .then(res => {
        this.setState({ sysCodeSourceList: res.data })
      })
      .catch(() => { })

  }

  // 查询系统的lov和值列表
  getLOVOrSysSourceList = value => {
    if (value) {
      let handleMethod = null;
      if (value === 'listvalues') { // 值列表
        handleMethod = service.getSysSourceList;
      } else { // lov数据
        handleMethod = service.getLOVSourceList;
      }

      if (!handleMethod) return;
      handleMethod()
        .then(res => {
          if (res.data) {
            if (value === 'listvalues') {
              this.setState({ sysCodeSourceList: res.data })
            } else {
              this.setState({ LovSourceList: res.data })
            }

          }
        })
        .catch(() => { })
    }
  }


  // 查询lov的参数
  getLOVlist = (id, index) => {
    if (id) {
      const { LovItemList } = this.state;
      service
        .getappproveRender(id)
        .then(res => {
          const values = res.data;
          const tempArr = [];
          if (values) {
            values.forEach(item => {
              const temp = {
                keyCode: item.keyCode,
                value: '',
                name: item.name
              }
              tempArr.push(temp)

            })
          }
          LovItemList[index] = tempArr;
          this.setState({
            LovItemList,
          })
        })
        .catch(err => {
          console.log(err);
          message.error(err.response.data.message)
        })
    }

  }


  // 渲染LOv的参数
  renderParamsInput = (indexLov) => {
    const { LovItemList, record } = this.state;
    const { form: { getFieldDecorator }, params, currentUser, systemFlag } = this.props;
    const formItemLayout = {
      labelCol: {
        span: 8
      },
      wrapperCol: {
        span: 10
      },
    };
    const disabled = currentUser.tenantId === '0' ? false : (systemFlag === 'true')

    if (LovItemList[indexLov] && LovItemList[indexLov].length > 0) {
      return (
        <>
          {
            (LovItemList[indexLov].map((item, index) => {
              console.log(item)
              const labelIndex = (`${this.$t(item.name)}` || item.keyCode);
              const rule = [
                { required: false, message: this.$t("common.please.enter") }
              ];
              return (
                <Form.Item label={labelIndex} key={labelIndex} {...formItemLayout}>
                  {
                    getFieldDecorator(`lovParams-${indexLov}:${item.keyCode}`, {
                      rules: rule,
                      initialValue: params.id && item.value ? item.value : ''
                    })(
                      <Input placeholder={this.$t("common.please.enter")} disabled={disabled} />
                    )
                  }
                </Form.Item>
              )
            }))
          }
        </>
      )
    }
  }

  getLovData = async (apiId) => {
    new Promise((resolve, reject) => {
      service.getappproveRender(apiId)
        .then(result => {
          const data = result.data && result.data ? result.data : []
          resolve(data)
        })
        .catch(err => {
          console.log(err);
          message.error("加载lov详情失败!!! 请稍后再试")
        });
    })

  };


  getParamsFieldValueList = (value) => {
    const { form: { getFieldValue } } = this.props;
    const { typeId } = this.props
    if (value) {
      service
        .getapprovamatching(typeId)
        .then(res => { // 獲取匹配字段數據源
          if (res.data) {
            const tempArr = Array.isArray(res.data) ? res.data.map(item => {
              const newItem = item;
              newItem.value = item.structCode,
                newItem.label = item.structName,
                Array.isArray(newItem.children) && newItem.children.forEach(v => {
                  v.value = v.fieldCode
                  v.label = v.fieldName
                })
                if(!Array.isArray(newItem.children)){
                  newItem.disabled = true
                }
              return newItem;
            }) : [];
            this.setState({
              fieldValueList: tempArr,
            })
          }
        })
        .catch((err) => {
          console.log(err);

          message.error(err.response.data.message)
        })
    }

  }

  // newapprovarulesSave   新建的接口方法

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {

      if (err) return;
      const { typeId, params } = this.props
      const { approvalRulesRender, LovItemList } = this.state
      const { systemFlag } = this.props.systemFlag;

      const newTemp = Object.keys(values).reduce((prev, cur) => {

        if (cur.includes('-')) {
          const index = cur.split("-")[1].includes(':') ? Number(cur.split("-")[1].split(':')[0]) : Number(cur.split("-")[1])
          const indexSuffix = cur.split("-")[1].includes(':') && cur.split("-")[1].split(':')[1];
          // num --- str=>NaN
          const newKey = cur.split('-')[0];
          if (prev[index]) {
            if (cur.includes('lovParams') && Array.isArray(prev[index].parameters)) {
              prev[index].parameters.push({
                [indexSuffix]: values[cur],
                tempName: LovItemList[index][1].name,

              })

            } else if (cur.includes('lovParams')) {
              prev[index].parameters = [];
              prev[index].parameters.push({
                [indexSuffix]: values[cur],
                tempName: LovItemList[index][0].name,
              })

            } else {
              if (newKey.toLowerCase() !== 'lovid') {
                prev[index][newKey] = values[cur];
              }
              else {
                prev[index].LovId = values[cur].split('-')[0];
                prev[index].lovCode = values[cur].split('-')[1];
                // prev[index].apiId = values[cur].split('-')[2];
              }
            };
          } else {
            const keyCode = params.id ? 'fieldName' : 'keyCode';
            prev[index] = {
              fieldName: approvalRulesRender[index][keyCode],
              // approaches-${index}
              paramType: approvalRulesRender[index].reqType,
              name: approvalRulesRender[index].name
            };
            prev[index][newKey] = values[cur];
          }
        }

        return prev;

      }, [])

      newTemp.map(item => {
        if (item.matchingfield) {
          item.matchingfield = {
            structCode: item.matchingfield[0],
            fieldCode: item.matchingfield[1],
          };
        }
      });


      const finalParams = {
        ruleInterfaceId: values.ruleInterfaceId,
        approvalRuleCode: values.approvalRuleCode,
        approvalRuleName: values.approvalRuleName,
        description: values.description,
        enabled: values.enabled,
        createType: systemFlag ? "systemPreset" : "userDefinition",
        ruleInterfaceInfo: JSON.stringify(newTemp),
        typeId: typeId,
        apply: values.apply,
      }



      const finalParamsedit = {
        ruleInterfaceId: values.ruleInterfaceId,
        approvalRuleCode: values.approvalRuleCode,
        approvalRuleName: values.approvalRuleName,
        description: values.description,
        enabled: values.enabled,
        createType: systemFlag ? "systemPreset" : "userDefinition",
        ruleInterfaceInfo: JSON.stringify(newTemp),
        typeId: typeId,
        apply: values.apply,
        id: params.id,
        versionNumber: params.versionNumber

      }
      if (!params.id) {
        const i18n = {};
        i18n.approvalRuleName = values.approvalRuleName.i18n;
        finalParams.approvalRuleName = values.approvalRuleName.value;
        finalParams.i18n = i18n;
        service.newapprovarulesSave(finalParams).then(res => {
          message.success("操作成功")
          const { onClose } = this.props;
          if (onClose) { onClose(true); }

        })
          .catch(error => {
            message.error(error.response.data.message);
          })
      } else {
        const i18n = {};
        i18n.approvalRuleName = values.approvalRuleName.i18n;
        finalParamsedit.approvalRuleName = values.approvalRuleName.value;
        finalParamsedit.i18n = i18n;
        service.newapprovarulesSave(finalParamsedit).then(res => {
          message.success("操作成功")
          const { onClose } = this.props;
          if (onClose) { onClose(true); }

        })
          .catch(error => {
            message.error(error.response.data.message);
          })

      }
    });
  };



  handleCancel = () => {
    const { onClose } = this.props;
    if (onClose) { onClose(true); }
  };


  render() {
    const { getFieldDecorator, getFieldValue, setFieldsValue } = this.props.form;
    const { visible, approvalRulesItem, approvalInterfacelist, approvalRulesRender,
      pprovaMatching, number, LovSourceList, LovItemList, fieldValueList, sysCodeSourceList } = this.state;
    const { params, currentUser, systemFlag } = this.props
    const formItemLayout = {
      labelCol: {
        span: 8
      },
      wrapperCol: {
        span: 10
      },
    };
    const disabled = currentUser.tenantId === '0' ? false : (systemFlag === 'true')
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <p>基本信息</p>
          <Divider />
          <Form.Item {...formItemLayout} label="审批规则代码">
            {getFieldDecorator('approvalRuleCode', {
              initialValue: params.id ? params.approvalRuleCode : '',
              rules: [
                { required: true, message: '请输入' },
                {
                  pattern: /^[0-9a-zA-Z_]{1,}$/,
                  message: '只能输入英文数字下划线!',
                },
              ],
            })(<Input disabled={disabled || params.id} placeholder="请输入" />)}
          </Form.Item>

          <Form.Item {...formItemLayout} label="审批规则名称">
            {getFieldDecorator('approvalRuleName', {
              initialValue: params.id ? { value: params.approvalRuleName, i18n: params.i18n ? params.i18n.approvalRuleName : [] } : '',
              rules: [{ required: true, message: '请输入' }],
            })(<InputLanguage placeholder="请输入" disabled={disabled} />)}
          </Form.Item>

          <Form.Item {...formItemLayout} label="应用">
            {getFieldDecorator('apply', {
              initialValue: params.id ? params.apply : undefined,
              rules: [{ required: true, message: '请选择' }],
            })(
              <Select placeholder="请选择"
                disabled={disabled}
                getPopupContainer={node => node.parentNode}
                onChange={(value, option) => {
                  setFieldsValue({ ruleInterfaceId: '' })
                  if (option) { this.getapply(option.key) }
                }}
                allowClear
              >
                {
                  approvalInterfacelist.map(item => {
                    return (
                      <Select.Option key={item.id}>
                        {item.appName}
                      </Select.Option>
                    )
                  })
                }
              </Select>
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label="规则接口">
            {getFieldDecorator('ruleInterfaceId', {
              initialValue: params.id ? params.ruleInterfaceId : undefined,
              rules: [{ required: true, message: '请选择' }],
            })(
              <Select placeholder="请选择"
                showSearch
                getPopupContainer={node => node.parentNode}
                allowClear
                onChange={(value, option) => { if (option) { this.getapprovalRulePort(option.key) } }}
                disabled={!(getFieldValue('apply'))}
                showSearch
                filterOption={(input, option) =>
                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                disabled={disabled || !(getFieldValue('apply'))}
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                getPopupContainer={node => node.parentNode}
              >
                {
                  approvalRulesItem.map(item => {
                    return (
                      <Select.Option key={item.id}>
                        {item.interfaceName}
                      </Select.Option>
                    )
                  })
                }
              </Select>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label="描述">
            {getFieldDecorator('description', {
              initialValue: params.id ? params.description : '',
            })(<TextArea placeholder="请输入" disabled={disabled} />)}
          </Form.Item>

          <Form.Item {...formItemLayout} label="状态:">
            {getFieldDecorator('enabled', {
              valuePropName: 'checked',
              initialValue: params.id ? params.enabled : true
            })(
              <Switch
                disabled={disabled}
              />
            )}
            {this.props.form.getFieldValue('enabled') ? "启用" : "禁用"}
          </Form.Item>
          <p>接口参数设置</p>
          <Divider />
          {
            approvalRulesRender.map((item, index) => {
              const dataIndex = `list${index}`
              return (
                <span key={dataIndex}>
                  < Form.Item {...formItemLayout} label={`${this.$t(item.name)}: 取值方式`}>
                    {getFieldDecorator(`approaches-${index}`, {
                      initialValue: params.id ? item.approaches : undefined,
                      rules: [{ required: true, message: '请选择' }],
                    })(
                      <Select placeholder="请选择"
                        getPopupContainer={node => node.parentNode}
                        disabled={disabled}
                        allowClear
                        onChange={value => { this.getParamsFieldValueList(value, 'structCode') }}

                      >
                        <Option value='dataField'>数据结构表字段</Option>
                        <Option value='manual'>手动选择</Option>
                        <Option value='empty'>为空</Option>
                      </Select>
                    )}
                  </Form.Item>

                  {getFieldValue(`approaches-${index}`) === "dataField" ?
                    <Form.Item {...formItemLayout} label="匹配字段">
                      {getFieldDecorator(`matchingfield-${index}`, {
                        initialValue: params.id && item.matchingfield ? [item.matchingfield.structCode, item.matchingfield.fieldCode] : undefined,
                        rules: [{ required: true, message: '请选择' }],
                      })(
                        <Cascader
                          disabled={disabled}
                          mode="multiple"
                          placeholder={this.$t("common.please.select")}
                          allowClear
                          options={fieldValueList}
                          getPopupContainer={node => node.parentNode}
                        // onFocus={this.messageTipAboutInfoApi}
                        />
                      )}
                    </Form.Item>

                    : null}
                  {getFieldValue(`approaches-${index}`) === "manual" ?
                    <Form.Item {...formItemLayout} label="数据范围类型">
                      {getFieldDecorator(`sourceInfoType-${index}`, {
                        initialValue: params.id ? item.sourceInfoType : undefined,
                        rules: [{ required: true, message: '请选择' }],
                      })(
                        <Select placeholder="请选择"
                          disabled={disabled}
                          getPopupContainer={node => node.parentNode}
                          allowClear
                          onChange={value => this.getLOVOrSysSourceList(value)}

                        >
                          <Option value='LOV'>LOV</Option>
                          <Option value='listvalues'>值列表</Option>
                        </Select>
                      )}
                    </Form.Item>
                    : null
                  }



                  {
                    getFieldValue(`approaches-${index}`) === 'manual' &&
                      getFieldValue(`sourceInfoType-${index}`) === "LOV" ?
                      <Form.Item {...formItemLayout} label="LOV">
                        {getFieldDecorator(`LovId-${index}`, {
                          initialValue: params.id && item.lovCode ? `${item.LovId}-${item.lovCode}` : undefined,
                          rules: [{ required: true, message: '请选择' }],
                        })(
                          <Select
                            disabled={disabled}
                            placeholder={this.$t("common.please.select")}
                            allowClear
                            getPopupContainer={node => node.parentNode}
                            onChange={(value, option) => { this.getLOVlist(option.key, index) }}
                            showSearch
                            filterOption={(input, option) =>
                              option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                          >
                            {LovSourceList.map(item => (
                              <Select.Option key={item.apiId} value={`${item.id}-${item.lovCode}`}>{item.lovName}</Select.Option>
                            ))}
                          </Select>
                        )}
                      </Form.Item>
                      : null
                  }

                  {getFieldValue(`approaches-${index}`) === 'manual' &&
                    getFieldValue(`sourceInfoType-${index}`) === 'LOV'
                    && getFieldValue(`LovId-${index}`)
                    && this.renderParamsInput(index)
                  }
                  {/* ruleInterfaceId */}
                  {getFieldValue(`approaches-${index}`) === 'manual' &&
                    getFieldValue(`sourceInfoType-${index}`) === "listvalues" ?
                    <Form.Item {...formItemLayout} label="值列表">
                      {getFieldDecorator(`valueCode-${index}`, {
                        initialValue: params.id ? item.valueCode : undefined,
                        rules: [{ required: true, message: '请输入' }],
                      })(
                        <Select
                          showSearch
                          disabled={disabled}
                          placeholder={this.$t("common.please.select")}
                          allowClear
                          optionFilterProp="children"
                          filterOption={(input, option) =>
                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                          }
                          getPopupContainer={node => node.parentNode}
                        >
                          {
                            Array.isArray(sysCodeSourceList) && sysCodeSourceList.map(item => (
                              <Select.Option value={item.code}>{item.name}</Select.Option>
                            ))
                          }
                        </Select>
                      )}
                    </Form.Item>
                    : null
                  }

                </span>
              )
            })
          }
        </Form>
        <div className="slide-footer">
          {!disabled &&
            <Button type="primary" onClick={this.handleSubmit}>
              {this.$t('common.save')}
            </Button>}
          <Button onClick={this.handleCancel}>
            {this.$t('common.cancel')}
          </Button>
        </div>
      </div >
    );
  }
}

const ConditionparametersForm = Form.create()(Conditionparameters)
export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(ConditionparametersForm);
// export default Form.create()(Conditionparameters);
