
import React, { Component } from 'react';
import CustomTable from 'widget/custom-table';
import { Badge, Button, Divider, message, Popconfirm } from 'antd';
import config from 'config';
import SearchArea from 'widget/search-area';
import SlideFrame from 'widget/slide-frame';
import NewRole from './new';
import service from '../service'
import { connect } from 'dva';



class ApprovalConditions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      record: {},
      visibel: false,
      searchFormItems: [
        {
          label: '审批条件代码',
          id: 'conditionSetCode',
          placeholder: this.$t("common.please.enter"),
          type: 'input',
        },
        {
          label: '审批条件名称',// 名称
          id: 'conditionSetName',
          placeholder: this.$t("common.please.enter"),
          type: 'input',
        },
        {
          label: '状态',// 名称
          id: 'enabled',
          placeholder: this.$t("common.please.select"),
          type: 'select',
          options: [
            { label: '启用', value: true },
            { label: '禁用', value: false }
          ]
        },
      ],
      columns: [
        {
          title: '审批条件代码',// 代码
          align: 'center',
          dataIndex: 'conditionSetCode',

        },
        {
          title: '审批条件名称',// 名称
          align: 'center',
          dataIndex: 'conditionSetName',
          tooltips: true,
        },
        {
          title: '说明',// 名称
          align: 'center',
          dataIndex: 'description',
          tooltips: true,
        },
        {
          title: '状态',
          dataIndex: 'enabled',
          align: 'center',
          render: desc => <Badge status={desc ? 'success' : 'error'} text={desc ? '启用' : '禁用'} />,
        },
        {
          title: this.$t({ id: 'common.operation' }),// 操作
          dataIndex: 'option',
          align: 'center',
          render: (value, record) => {
            const { params, currentUser } = this.props
            return (
              <span>
                {(currentUser.tenantId === record.tenantId) && <span><a onClick={(e) => this.edit(e, record)}>编辑</a></span>}
                {((currentUser.tenantId !== record.tenantId) ? <a onClick={(e) => this.edit(e, record)}>详情</a> : null)}
                {currentUser.tenantId === record.tenantId && <Divider type="vertical" />}
                {
                  currentUser.tenantId === record.tenantId &&
                  <span>
                    <Popconfirm
                      onConfirm={(e) => this.delete(e, record)}
                      title={this.$t('common.confirm.delete')}
                    >
                      <a>删除</a>
                    </Popconfirm>
                  </span>}
              </span>
            );
          },
        },
        // {
        //   title: '应用审批流',
        //   dataIndex: 'status',
        //   align: 'center',
        //   render: (value, record) => {
        //     return (
        //       <span>
        //         {/* {(this.props.params.systemFlag === "false" && (this.props.currentUser.tenantId === record.tenantId)) ? <a onClick={() => this.applicationapproval(record)}>应用审批流</a> : null} */}
        //       </span>
        //     )
        //   },
        // },
      ],
    }
  }

  /*编辑*/
  edit = (e, record) => {
    this.setState({
      visibel: true,
      record,
    })

  }



  // 删除
  delete = (e, record) => {
    if (record) {
      service.approvalDelete(record.id).then(res => {
        message.success('操作成功！');
        this.table.search();
        this.setState({})
      })
        .catch(err => {
          console.log(err)
          message.error(err.response.data.message);
        })
    }
  }


  // 应用审批流
  applicationapproval = (e, record) => {
    console.log(record)

  }


  // 搜素
  handleSearch = (values) => {
    this.table.search(values);
  }






  handleCloseSlideFrame = flag => {
    this.setState({
      visibel: false,
    }, () => {
      if (flag) {
        this.table.search({});
      }
    })
  }


  // 新建
  handleNewApprovalValue = () => {
    this.setState({
      visibel: true,
      record: {},
    })
  };


  render() {
    const { searchFormItems, columns, record, visibel } = this.state;
    const { params, currentUser } = this.props;
    const disabled = currentUser.tenantId === '0' ? false : (params.systemFlag === 'true')
    return (
      <div>
        <SearchArea
          searchForm={searchFormItems}
          submitHandle={this.handleSearch}
        />


        {<Button style={{ margin: '10px 0' }} onClick={this.handleNewApprovalValue} type="primary">
          新建
        </Button>}

        <CustomTable
          ref={ref => { this.table = ref; return this.table }}
          columns={columns}
          url={`${config.wflUrl}/api/wfl/condition`}
          onRowClick={this.detail}
          params={{ typeId: params.id }}
        />
        <SlideFrame
          title={record.id ? '编辑审批条件' : '新建审批条件'}
          show={visibel}
          onClose={() => { this.handleCloseSlideFrame(false) }}
        >
          <NewRole params={record} typeId={params.id} systemFlag={params.systemFlag} onClose={(flag) => { this.handleCloseSlideFrame(flag) }} />
        </SlideFrame>
      </div>
    )
  }
}
export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(ApprovalConditions);

