import React from 'react';
import { Divider, Form, Switch, Input, Icon, message, Button, Select, Row, Col, DatePicker, InputNumber, Badge } from 'antd';
import ListSelector from 'widget/list-selector';
import config from 'config';
import moment, { version } from "moment";
import LOV from 'widget/Template/multistage-list-selector'
import { connect } from 'dva';
import InputLanguage from "@/components/Widget/Template/input-language";
import service from '../service';
import { call } from 'redux-saga/effects';
const { TextArea } = Input;
const { Option } = Select;

class Conditionparameters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      record: {},
      approval: false,
      showLov: -1,
      number: '',
      approvalList: [],
      selectedData: [],
      selectorItem: {
        title: '选择参数',
        url: `${config.wflUrl}/api/wfl/type/param`,
        searchForm: [

          { type: 'input', id: 'paramCode', label: '参数代码' },
          { type: 'input', id: 'paramName', label: '参数名称' },
          { type: 'input', id: 'structRemark', label: '说明' },
        ],
        columns: [
          { title: '参数代码', dataIndex: 'paramCode' },
          { title: '参数名称', dataIndex: 'paramName' },
          { title: '说明', dataIndex: 'structRemark' },
        ],
        key: 'id',
      },
      lovValueArr: [],
      sysValueArr: [],
      sysValueSelected: [],
      // 获取的值列表的值
      sysValueByCode: [],
      // 已选择值列表的值
      sysCodeSelectData: [],
      selectorItemCode: {
        title: '选择值列表的值',
        url: '',
        searchForm: [

          { type: 'input', id: 'name', label: '值名称' },
          {
            label: '状态',// 名称
            id: 'enabled',
            placeholder: this.$t("common.please.select"),
            type: 'select',
            options: [
              { label: '启用', value: true },
              { label: '禁用', value: false }
            ]
          },
        ],
        columns: [
          { title: 'id', dataIndex: 'id' },
          { title: '名称', dataIndex: 'name' },
          {
            title: '状态',
            dataIndex: 'enabled',
            align: 'center',
            render: desc => <Badge status={desc ? 'success' : 'error'} text={desc ? '启用' : '禁用'} />,
          },
        ],
        key: 'id',
      },
      sysValueByCodeVisible: false,
      // 当前index
      currentIndex: '',
    };
  }


  componentDidMount = () => {
    const { params, typeId } = this.props
    if (params.id) {
      const { approvalList, selectedData, lovValueArr, sysValueArr, sysValueSelected } = this.state
      const paramsCode = JSON.parse(params.conditionInfo)


      Array.isArray(paramsCode) && paramsCode.map((item, index) => {
        item.lovParams = item.lovParams && (JSON.parse(item.lovParams))
        if (item.paramRangeType === 'lov') {
          lovValueArr[index] = item.lovParams
        } else {
          sysValueArr[index] = item.lovParams
          // 已选中的值
          const selectedData = item.lovParams && item.lovParams.selectedRows && item.lovParams.selectedRows
          const ids = [];
          if (Array.isArray(selectedData)) {
            selectedData.forEach(e => {

              if (e.id) {
                const temp = {
                  id: e.id
                }
                ids.push(temp)
              }

            })
          }
          sysValueSelected[index] = ids
        }
        this.setState({
          lovValueArr,
          sysValueArr,
          sysValueSelected,
        })
      })
      this.setState({
        approvalList: paramsCode || [],
        selectedData: paramsCode || [],
      })

    }
    // this.getDefaultSysCode();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ visible: nextProps.visible });
  }


  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      const { typeId, params } = this.props
      const { approvalList, lovValueArr, sysValueArr } = this.state;
      const newTemp = Object.keys(values).reduce((prev, cur) => {
        if (cur.includes('-')) {
          const index = Number(cur.split("-")[1]);
          const newKey = cur.split('-')[0];
          const indexStr = String(index)
          if (prev[index]) {
            prev[index][newKey] = values[cur];
            prev[index] = { ...approvalList[index], ...prev[index] }
            delete prev[index].enabled;
            delete prev[index].deleted;
          } else {
            prev[index] = {};
            prev[index][newKey] = values[cur];

            if (approvalList[index].paramRangeType === 'lov') {
              prev[index].lovParams = JSON.stringify(lovValueArr[index]);
            } else {

              let sysValueArrInfo = sysValueArr[index];
              if (sysValueArrInfo) {
                sysValueArrInfo.sysCode = JSON.parse(approvalList[index].paramRangeInfo).sysCode;
                prev[index].lovParams = JSON.stringify(sysValueArrInfo);
              }
            }

            prev[index] = { ...approvalList[index], ...prev[index] }
            delete prev[index].enabled;
            delete prev[index].deleted;

          }
        }
        return prev;

      }, [])
      const finalParams = {
        conditionSetCode: values.conditionSetCode,
        conditionSetName: values.conditionSetName,
        description: values.description,
        enabled: values.enabled,
        workflowConditionSetInfoItems: newTemp,
        typeId: typeId
      }
      const finalParamsedit = {
        conditionSetCode: values.conditionSetCode,
        conditionSetName: values.conditionSetName,
        description: values.description,
        enabled: values.enabled,
        workflowConditionSetInfoItems: newTemp,
        typeId: typeId,
        id: params.id,
        versionNumber: params.versionNumber,
      }

      if (!params.id) {
        const i18n = {};
        i18n.conditionSetName = values.conditionSetName.i18n;
        finalParams.conditionSetName = values.conditionSetName.value;
        finalParams.i18n = i18n;
        service.newapprovalSave(finalParams).then(res => {
          message.success('操作成功！');
          // this.setState({ saveLoading: false })
          const { onClose } = this.props;
          if (onClose) { onClose(true); }
        })
          .catch(err => {
            message.error(err.response.data.message);
          })
      } else {
        const i18n = {};
        i18n.conditionSetName = values.conditionSetName.i18n;
        finalParamsedit.conditionSetName = values.conditionSetName.value;
        finalParamsedit.i18n = i18n;
        service.newapprovalSave(finalParamsedit).then(res => {
          message.success('操作成功！');
          const { onClose } = this.props;
          if (onClose) { onClose(true); }
        })
          .catch(err => {
            message.error(err.response.data.message);
          })

      }

    });
  };

  handleCancel = () => {
    const { onClose } = this.props;
    if (onClose) { onClose(true); }
  };

  // lov弹出框保存
  approvalOk = (value) => {
    const { approvalList } = this.state;
    const valueList = Array.isArray(value.result) ? value.result : [];

    valueList.forEach(item => {
      approvalList.push(item)
    });

    this.setState({
      approvalList,
      approval: false,
    })
  }


  // lov弹出框触发
  showapprovalModel = () => {
    const { approval } = this.state;
    this.setState({ approval: true })
  }


  // 当为手动选择开启的时候
  scopeChange = (value) => {
    this.setState(

    )
  }

  // 删除
  delete = (index) => {
    const { approvalList } = this.state
    approvalList.splice(index, 1);
    this.setState({ approvalList: approvalList.length === 0 ? [] : approvalList })
  }


  // 处理级联Lov参数
  toCodeArr = (paramRangeInfo) => {
    //let info = paramRangeInfo ? JSON.parse(paramRangeInfo) : '';

    const lovCodes = [];
    if (paramRangeInfo) {
      const temp = JSON.parse(paramRangeInfo);
      // => {"lovCode":"MDATA_COMPANY_LOV_QUERY","parameters":[{"code":""},{"name":""},{"lovCode":""}]}

      const obj = {};
      let lovCodeStr = '';
      temp.parameters.forEach(item => {
        const key = Object.keys(item)[0]
        if (key !== 'lovCode') {
          obj[key] = item[key];
        } else {
          lovCodeStr = item.lovCode
        }
      })

      const final = `${temp.lovCode}(${JSON.stringify(obj)})|${lovCodeStr}`;
      const tempArr = final.split('|');
      return tempArr;
    }
    return [];

  }

  // 弹框加载值列表的值
  onOpenSysCodeValue = (item, currentIndex) => {
    const { selectorItemCode, sysValueByCodeVisible } = this.state;
    const sysCode = item.paramRangeInfo ? JSON.parse(item.paramRangeInfo).sysCode : item.lovParams.sysCode;

    if (sysCode) {
      selectorItemCode.url = `${config.baseUrl}/api/custom/enumerations/template/by/type?type=${sysCode}`
      this.setState({ selectorItemCode, sysValueByCodeVisible: true, currentIndex })
    }

  }

  // 处理值列表的值
  sysCodeOk = (value) => {
    const { currentIndex, sysValueArr } = this.state;
    let template = { selectedRows: [], sysCode: '' };
    value.result.map(item => {
      let temp = {
        id: item.id,
        code: item.value,
      }
      template.selectedRows.push(temp)
    })
    sysValueArr[currentIndex] = template;
    this.setState({
      sysValueArr,
      sysValueByCodeVisible: false,
    })
  }

  // 处理初始化是否手动选择数据

  toInitialSelectManual = (params, item) => {
    if (item.manualFlag) {
      return "manual"
    } else {
      if (params.id && item.selectManual) {
        return item.selectManual;
      }
      if (item.selectManual) {
        return item.selectManual;
      }
      return "scope"
    }
  }

  // LOV的弹窗//一选择多少条
  showLov = (item, index) => {
    this.setState({
      showLov: index,
    })

  }

  // 保存选择的lov值列表数值
  handleGetLov = (value, index) => {
    const { lovValueArr } = this.state;
    // 处理成 lovValueArr[index] 有 selectedRows[]属性
    let tempArr = {
      selectedRows: [],
      superiorRows: []
    }
    let temps = []
    // obj.constructor === Object
    if (value.constructor === Object) {
      value.selectedRows.forEach(item => {
        let temp = {
          id: item.id,
          name: item.name
        }
        tempArr.selectedRows.push(temp)
      })

      value.superiorRows.forEach(item => {
        let temp1 = {
          code: item.code,
          dataIndex: item.dataIndex,
          id: item.id,
          lovCode: item.lovCode,
          title: item.title,
        }
        tempArr.superiorRows.push(temp1)
      })
    } else {
      value.forEach(item => {
        let temp = {
          id: item.id,
          name: item.name
        }
        tempArr.selectedRows.push(temp)
        delete tempArr.superiorRows
      })

    }

    lovValueArr[index] = tempArr;
    this.setState({ lovValueArr, showLov: -1 })
  }









  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { approval, selectorItem, approvalList, showLov, lovValueArr, sysValueByCode,
      sysValueByCodeVisible, selectorItemCode, currentIndex, sysValueArr, sysValueSelected } = this.state;
    const { params, systemFlag, currentUser } = this.props
    const disabled = params.id ? ((currentUser.tenantId === params.tenantId) ? false : true) : false
    const formItemLayout = {
      labelCol: {
        span: 8
      },
      wrapperCol: {
        span: 10
      },
    };

    const Layout = {
      labelCol: {
        span: 10
      },
      wrapperCol: {
        span: 13
      },
    };
    return (
      <div>
        <Form>
          <p>基本信息</p>
          <Divider />
          <Form.Item {...formItemLayout} label="审批条件代码">
            {getFieldDecorator('conditionSetCode', {
              initialValue: params.id ? params.conditionSetCode : '',
              rules: [
                { required: true, message: '请输入' },
                {
                  pattern: /^[0-9a-zA-Z_]{1,}$/,
                  message: '只能输入英文数字下划线!'
                },
              ],
            })(<Input disabled={disabled || params.id} placeholder="请输入" />)}
          </Form.Item>

          <Form.Item {...formItemLayout} label="审批条件名称">
            {getFieldDecorator('conditionSetName', {
              initialValue: params.id ? { value: params.conditionSetName, i18n: params.i18n ? params.i18n.conditionSetName : [] } : '',
              rules: [{ required: true, message: '请输入' }],
            })(<InputLanguage placeholder="请输入" disabled={disabled} />)}
          </Form.Item>

          <Form.Item {...formItemLayout} label="说明">
            {getFieldDecorator('description', {
              initialValue: params.id ? params.description : '',
            })(<TextArea placeholder="请输入" disabled={disabled} />)}
          </Form.Item>

          <Form.Item {...formItemLayout} label="状态:">
            {getFieldDecorator('enabled', {
              valuePropName: 'checked',
              initialValue: params.id ? params.enabled : true
            })(
              <Switch
                disabled={disabled}
              />
            )}
            {this.props.form.getFieldValue('enabled') ? "启用" : "禁用"}
          </Form.Item>
          <p>条件明细</p>
          <Divider />
          <Button disabled={disabled} type="link" icon="plus" onClick={this.showapprovalModel}>新增条件</Button>

          <Form.Item {...formItemLayout} label="">
            {getFieldDecorator('enabled', {
              valuePropName: 'checked',
              initialValue: params.id ? params.enabled : true
            })(
              <ListSelector
                disabled={disabled}
                visible={approval}
                selectorItem={selectorItem}
                onOk={this.approvalOk}
                onCancel={() => this.setState({ approval: false })}
                extraParams={{ typeId: this.props.typeId, enabled: true }}
                showSelectTotal
                clearFlag
              />
            )}
          </Form.Item>

          {Array.isArray(approvalList) && approvalList.map((item, index) => {
            const dataIndex = `list${index}`
            if (item.paramType === "TEXT") {
              return (
                <div key={dataIndex}>
                  <Row>
                    <Col span={6}>
                      <Form.Item {...Layout} key={index} label={item.paramName}>
                        {getFieldDecorator(`contains-${index}`, {
                          initialValue: params.id ? item.contains : '',
                          rules: [{ required: true, message: '请输入' }],
                        })(
                          <Select placeholder="请选择" onChange={this.approvalChange} disabled={disabled} defaultValue={'include'}>
                            <Option key='include'>包含</Option>
                            <Option key='exclude'>排除</Option>
                          </Select>
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={5}>
                      {console.log(item)}
                      <Form.Item wrapperCol={{ span: 24 }} style={{ padding: '0 5px' }} label=''>
                        {getFieldDecorator(`selectManual-${index}`, {
                          initialValue: params.id ? item.selectManual : this.toInitialSelectManual(params, item),
                          rules: [{ required: true, message: '请输入' }],
                        })(
                          <Select placeholder="请选择" onChange={this.scopeChange} disabled={disabled || item.manualFlag === false}>
                            <Option key='scope'>范围限定</Option>
                            <Option key='manual'>手动选择</Option>
                          </Select>
                        )}
                      </Form.Item>
                    </Col>

                    {(item.selectManual === 'manual' || item.manualFlag) ?


                      (item.paramRangeType === 'lov'
                        ?
                        (<span>
                          <Col span={6}>
                            <LOV
                              key={index}
                              visible={showLov === index}
                              codeArr={this.toCodeArr(item.paramRangeInfo)}
                              onOk={value => { this.handleGetLov(value, index) }}
                              onCancel={() => this.setState({ showLov: -1 })}
                              width={700}
                              valueKey='id'
                              value={!!lovValueArr[index] ? (!!lovValueArr[index].superiorRows ? lovValueArr[index] : lovValueArr[index].selectedRows) : []}
                              disabled={disabled}
                            />
                          </Col>
                          <Col span={6}>
                            {getFieldValue(`selectManual-${index}`) === 'manual' &&
                              <Button disabled={disabled} type="link" icon="plus" onClick={() => { this.showLov(item, index) }}>
                                已选择{lovValueArr[index] && lovValueArr[index].selectedRows ? `${lovValueArr[index].selectedRows.length}` : 0}条
                            </Button>

                            }
                          </Col>
                          {getFieldValue(`selectManual-${index}`) === 'scope' &&
                            <span>
                              <Col span={6}>
                                <Form.Item wrapperCol={{ span: 24 }} style={{ padding: '0 5px' }} label=''>
                                  {getFieldDecorator(`upper-${index}`, {
                                    initialValue: params.id ? item.upper : '',
                                    rules: [
                                      { required: true, message: '请输入' },
                                      {
                                        validator: (rule, value, callback) => {
                                          const { form } = this.props
                                          const start1 = form.getFieldValue(`limit-${index}`);
                                          if (Number(value) > Number(start1)) {
                                            message.error('请输入正确的范围值!!')
                                            return
                                          } else {
                                            callback()

                                          }

                                        }
                                      }
                                    ],
                                  })(
                                    <Input placeholder="请输入" disabled={disabled}></Input>
                                  )}
                                </Form.Item>
                              </Col>
                              <Col span={6}>
                                <Form.Item wrapperCol={{ span: 24 }} style={{ padding: '0 5px' }} label=''>
                                  {getFieldDecorator(`limit-${index}`, {
                                    initialValue: params.id ? item.limit : '',
                                    rules: [{ required: true, message: '请输入' }],
                                  })(
                                    <Input placeholder="请输入" disabled={disabled}></Input>
                                  )}
                                </Form.Item>
                              </Col>
                            </span>}

                        </span>)

                        : (
                          <span>
                            <Col span={6}>
                              {/* 值列表的值弹框 */}
                              <ListSelector
                                visible={sysValueByCodeVisible}
                                selectorItem={selectorItemCode}
                                selectedData={sysValueSelected[index]}
                                onOk={this.sysCodeOk}
                                valueKey="id"
                                onCancel={() => this.setState({ sysValueByCodeVisible: false })}
                                showSelectTotal
                                clearFlag
                                disabled={disabled}
                              />
                            </Col>
                            <Col span={6}>
                              {getFieldValue(`selectManual-${index}`) === 'manual' ? < Button disabled={disabled} type="link" icon="plus" onClick={() => { this.onOpenSysCodeValue(item, index) }}>
                                已选择 {sysValueArr[index] && sysValueArr[index].selectedRows ? `${sysValueArr[index].selectedRows.length}` : 0}条
                              </Button> : ''}
                            </Col>
                            {getFieldValue(`selectManual-${index}`) === 'scope' &&
                              <span>
                                <Col span={6}>
                                  <Form.Item wrapperCol={{ span: 24 }} style={{ padding: '0 5px' }} label=''>
                                    {getFieldDecorator(`upper-${index}`, {
                                      initialValue: params.id ? item.upper : '',
                                      rules: [
                                        { required: true, message: '请输入' },
                                        {
                                          validator: (rule, value, callback) => {
                                            const { form } = this.props
                                            const start1 = form.getFieldValue(`limit-${index}`);
                                            if (Number(value) > Number(start1)) {
                                              message.error('请输入正确的范围值!!')
                                              return
                                            } else {
                                              callback()

                                            }

                                          }
                                        }
                                      ],
                                    })(
                                      <Input placeholder="请输入" disabled={disabled}></Input>
                                    )}
                                  </Form.Item>
                                </Col>
                                <Col span={6}>
                                  <Form.Item wrapperCol={{ span: 24 }} style={{ padding: '0 5px' }} label=''>
                                    {getFieldDecorator(`limit-${index}`, {
                                      initialValue: params.id ? item.limit : '',
                                      rules: [{ required: true, message: '请输入' }],
                                    })(
                                      <Input placeholder="请输入" disabled={disabled}></Input>
                                    )}
                                  </Form.Item>
                                </Col>
                              </span>}

                          </span>
                        ))
                      :
                      (<span>
                        <Col span={6}>
                          <Form.Item wrapperCol={{ span: 24 }} style={{ padding: '0 5px' }} label=''>
                            {getFieldDecorator(`upper-${index}`, {
                              initialValue: params.id ? item.upper : '',
                              rules: [
                                { required: true, message: '请输入' },
                                {
                                  validator: (rule, value, callback) => {
                                    const { form } = this.props
                                    const start1 = form.getFieldValue(`limit-${index}`);
                                    if (Number(value) > Number(start1)) {
                                      message.error('请输入正确的范围值!!')
                                      return
                                    } else {
                                      callback()

                                    }

                                  }
                                }
                              ],
                            })(
                              <Input placeholder="请输入" disabled={disabled}></Input>
                            )}
                          </Form.Item>
                        </Col>
                        <Col span={6}>
                          <Form.Item wrapperCol={{ span: 24 }} style={{ padding: '0 5px' }} label=''>
                            {getFieldDecorator(`limit-${index}`, {
                              initialValue: params.id ? item.limit : '',
                              rules: [{ required: true, message: '请输入' }],
                            })(
                              <Input placeholder="请输入" disabled={disabled}></Input>
                            )}
                          </Form.Item>
                        </Col>
                      </span>
                      )}
                    <span>{!disabled && <Icon type='delete' style={{ color: 'red', float: 'right', lineHeight: '30px' }} onClick={() => { this.delete(index) }} />}</span>
                  </Row>

                </div>
              )
            } else if (item.paramType === "DATE") {
              return (
                <div key={dataIndex}>
                  <Row>
                    <Col span={6}>
                      <Form.Item {...Layout} label={item.paramName}>
                        {getFieldDecorator(`startDateActive-${index}`, {
                          rules: [
                            {
                              required: true,
                              message: this.$t("common.please.select"),// 请选择
                            },
                          ],
                          initialValue: params.id ? moment(item.startDateActive) : moment()
                        })(

                          <DatePicker disabled={disabled} placeholder={this.$t('common.please.select')} style={{ width: "100%" }} getCalendarContainer={node => node.parentNode} />
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={6} >
                      <Form.Item wrapperCol={{ span: 24 }} style={{ padding: '0 5px' }} label={''}>
                        {getFieldDecorator(`endDateActive-${index}`, {
                          initialValue: params.id ? moment(item.endDateActive) : moment()
                        })(
                          <DatePicker
                            disabled={disabled}
                            getCalendarContainer={node => node.parentNode}
                            placeholder={this.$t('common.please.select')}
                            style={{ width: "100%" }}
                            onChange={date => {
                              const { form } = this.props
                              const startDate = form.getFieldValue(`startDateActive-${index}`);
                              if (date) {
                                if (new Date(startDate) > new Date(date)) {
                                  message.error('有效日期从大于有效日期至');
                                  return
                                } else {
                                  callback()
                                }
                              }
                            }}
                          />
                        )}
                      </Form.Item>
                    </Col>
                    <span>{!disabled && <Icon type='delete' style={{ color: 'red', float: 'right', lineHeight: '30px' }} onClick={() => { this.delete(index) }} />}</span>
                  </Row>
                </div>
              )
            } else if (item.paramType === "DECIMAL") {
              return (
                <div key={dataIndex}>
                  <Row>
                    <Col span={6}>
                      <Form.Item {...Layout} label={item.paramName}>
                        {getFieldDecorator(`numberData1-${index}`, {
                          initialValue: params.id ? item.numberData1 : '',
                          rules: [
                            {
                              validator: (rule, value, callback) => {
                                if (value) {
                                  const { form } = this.props
                                  const start = form.getFieldValue(`numberData1-${index}`);
                                  const symbol1 = form.getFieldValue(`symbol1-${index}`);
                                  if ((value && symbol1 == '')) {
                                    callback('请输入第一个条件符！！')
                                    return
                                  } else {
                                    callback()
                                  }
                                } else {
                                  callback()
                                }

                              }
                            },
                            {
                              validator: (rule, value, callback) => {
                                const { form } = this.props
                                const start1 = form.getFieldValue(`numberData2-${index}`);
                                if (!value && !start1) {
                                  message.error('请输入正确的范围值!!')
                                  return
                                } else {
                                  callback()

                                }

                              }
                            }
                          ]
                        })(
                          <InputNumber placeholder="请输入" disabled={disabled}></InputNumber>
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={5}>
                      <Form.Item wrapperCol={{ span: 24 }} style={{ padding: '0 5px' }} label={''}>
                        {getFieldDecorator(`symbol1-${index}`, {
                          initialValue: params.id ? item.symbol1 : '',
                        })(
                          <Select placeholder="请选择" onChange={this.Number} disabled={disabled}>
                            <Option value=''>无</Option>
                            <Option value='9003'>&lt;</Option>
                            <Option value='9004'>&le;</Option>

                          </Select>
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={2}>
                      <span>金额</span>
                    </Col>
                    <Col span={5}>
                      <Form.Item wrapperCol={{ span: 24 }} style={{ padding: '0 5px' }} label={''}>
                        {getFieldDecorator(`symbol2-${index}`, {
                          initialValue: params.id ? item.symbol2 : '',
                        })(
                          <Select placeholder="请选择" onChange={this.scopeChange} disabled={disabled}>
                            <Option value=''>无</Option>
                            <Option value='9003'>&lt;</Option>
                            <Option value='9004'>&le;</Option>
                            <Option value='9005'>=</Option>
                          </Select>
                        )}
                      </Form.Item>
                    </Col>
                    <Col span={5}>
                      <Form.Item wrapperCol={{ span: 24 }} style={{ padding: '0 5px' }} label={''}>
                        {getFieldDecorator(`numberData2-${index}`, {
                          initialValue: params.id ? item.numberData2 : '',
                          rules: [
                            {
                              validator: (rule, value, callback) => {
                                if (value) {
                                  const { form } = this.props
                                  const start = form.getFieldValue(`numberData1-${index}`);
                                  if (Number(start) > Number(value)) {
                                    callback('请输入合法的金额条件值，表达式不合法')
                                    return
                                  } else {
                                    callback()
                                  }
                                } else {
                                  callback()
                                }
                              },
                            },
                            {
                              validator: (rule, value, callback) => {
                                if (value) {
                                  const { form } = this.props
                                  const start = form.getFieldValue(`numberData1-${index}`);
                                  const symbol1 = form.getFieldValue(`symbol2-${index}`);
                                  if ((value && symbol1 == '')) {
                                    callback('请输入第二个条件符！！')
                                    return
                                  } else {
                                    callback()
                                  }
                                } else {
                                  callback()
                                }

                              }
                            }
                          ],
                        })(
                          <InputNumber placeholder="请输入" disabled={disabled}></InputNumber>
                        )}
                      </Form.Item>
                    </Col>
                    {!disabled && <Icon type='delete' style={{ color: 'red', float: 'right', lineHeight: '30px' }} onClick={() => { this.delete(index) }} />}
                  </Row>

                </div>
              )
            }
          })
          }
        </Form>
        <div className="slide-footer">
          {!disabled &&
            <Button type="primary" onClick={this.handleSubmit}>
              {this.$t('common.save')}
            </Button>}
          <Button onClick={this.handleCancel}>
            {this.$t('common.cancel')}
          </Button>
        </div>

      </div >
    );
  }
}
const ConditionparametersForm = Form.create()(Conditionparameters)
export default connect(({ user }) => ({
  currentUser: user.currentUser,
}))(ConditionparametersForm);
// export default Form.create()(Conditionparameters);
