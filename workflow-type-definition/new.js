import React from 'react';
import { Modal, Form, Switch, Input, Icon, message } from 'antd';
import service from './service';
import InputLanguage from "@/components/Widget/Template/input-language";

class NewworkflowtypeDefonition extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      record: {},
      // saveLoading: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ visible: nextProps.visible });
  }



  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {

      if (err) return;
      let finalParams = {
        ...values
      }
      const i18n = {};
      i18n.typeName = values.typeName.i18n;
      finalParams.typeName = values.typeName.value;
      finalParams.i18n = i18n;
      const { params } = this.props;
      let handleMethod = null;
      if (params.id) {
        handleMethod = service.editSave;
        finalParams = { ...params, ...finalParams };
      } else {
        handleMethod = service.getNewsave;
      }
      if (!handleMethod) return;
      handleMethod(finalParams).then(res => {
        message.success('操作成功！');
        // this.setState({ saveLoading: false })
        const { onClose } = this.props;
        if (onClose) { onClose(true); }
        if (!params.id) {
          this.dataStructure(res.data)
        }
      })
        .catch(err => {
          message.error(err.response.data.message);
          // this.setState({ saveLoading: false });
        })

    });
  };

  //保存之后跳转到详情页面
  dataStructure = record => {
    this.$pushPage({
      functionCode: "workflow_type_definition",
      pageCode: 'workflow_type_definition_detail',
      params: [record.id, record.systemFlag]
    });
  };

  handleCancel = () => {
    const { onClose } = this.props;
    if (onClose) { onClose(true); }
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { visible } = this.state;
    const { params } = this.props
    const formItemLayout = {
      labelCol: {
        span: 8
      },
      wrapperCol: {
        span: 10
      },
    };

    return (
      <Modal
        title={!params.id ? "新建工作流类型" : "编辑工作流类型"}
        visible={visible}
        onOk={this.handleSubmit}
        onCancel={this.handleCancel}
        destroyOnClose
      >
        <Form>
          <Form.Item {...formItemLayout} label="工作流类型代码">
            {getFieldDecorator('typeCode', {
              initialValue: params.id ? params.typeCode : '',
              rules: [
                { required: true, message: '请输入' },
                { max: 50, message: '名称不超过50个字符' },
                {
                  pattern: /^[0-9a-zA-Z_]{1,}$/,
                  message: '只能输入英文数字下划线!',
                },
              ],
            })(<Input disabled={params.id} placeholder="请输入" />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="工作流类型名称">
            {getFieldDecorator('typeName', {
              initialValue: params.id ? { value: params.typeName, i18n: params.i18n ? params.i18n.typeName : [] } : '',
              rules: [
                { required: true, message: '请输入' },


              ],
            })(<InputLanguage placeholder="请输入" />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="状态:">
            {getFieldDecorator('enabled', {
              valuePropName: 'checked',
              initialValue: params.id ? params.enabled : true
            })(
              <Switch
              />
            )}
            {this.props.form.getFieldValue('enabled') ? "启用" : "禁用"}
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}
export default Form.create()(NewworkflowtypeDefonition);
