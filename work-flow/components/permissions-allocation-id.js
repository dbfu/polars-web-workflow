
import React from 'react';
import { Select, Radio } from 'antd';

import { SelectDepOrPerson } from 'widget/index';
import SelectEmployeeGroup from 'widget/Template/select-employee-group';
import PropTypes from 'prop-types';
import ListSelector from 'widget/list-selector';
import { connect } from 'dva';
import {
  WFL_IDENTITY_ALL,
  WFL_IDENTITY_COMPANY,
  WFL_IDENTITY_USER_GROUP,
  WFL_IDENTITY_CONTACT,
  WFL_IDENTITY_DEPARTMENT,
} from './range-type';

const RadioGroup = Radio.Group;

/**
 * 权限分配组件(全部人员，按人员添加，按部门添加，按人员组添加,按公司添加)--oid
 */
class PermissionsAllocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

      type: WFL_IDENTITY_ALL,
      showSelectDepartment: false,
      showSelectEmployeeGroup: false,
      showSelectEmployee: false,
      selectedList: [],
      selectEmployeeText: '',
      showSelectCompany: false,
    };
  }

  componentWillMount() {
    const { value: model } = this.props;

    if (model) {
      const result = model.values ? model.values.map(item => ({
        ...item,
        contactId: item.id,
        id: item.id,
        label: item.name,
        name: item.name,
      })) : [];
      this.setState({ type: model.type, selectedList: result }, () => {
        this.setSelectEmployeeText();
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    const model = nextProps.value;

    if (model) {
      const result = model.values ? model.values.map(item => ({
        ...item,
        contactId: item.id,
        id: item.id,
        label: item.name,
        name: item.name,
      })) : [];
      this.setState({ type: model.type, selectedList: result }, () => {
        this.setSelectEmployeeText();
      });
    }
  }

  onApplyEmployee = e => {
    this.setState({ type: e.target.value, selectedList: [] }, () => {
      this.setSelectEmployeeText();
      this.onChange();
    });
  };

  setSelectEmployeeText = () => {
    let text = '';
    const { type, selectedList } = this.state;
    if (type === WFL_IDENTITY_DEPARTMENT) {
      text = this.$t('common.selected.number.department', {
        number: selectedList.length,
      });
    } else if (type === WFL_IDENTITY_USER_GROUP) {
      text = this.$t('common.selected.number.user.group', {
        number: selectedList.length,
      });
    } else if (type === WFL_IDENTITY_ALL) {
      text = this.$t('common.all.type');
    } else if (type === WFL_IDENTITY_CONTACT) {
      text = this.$t('common.selected.number.user', {
        number: selectedList.length,
      });
    } else if (type ===  WFL_IDENTITY_COMPANY) {
      text = this.$t('common.selected.number.user.company', {
        number: selectedList.length,
      });
    }
    this.setState({ selectEmployeeText: text });
  };

  showSelectEmployeeGroup = () => {
    const { type } = this.state;
    // this.refs.selectEmployeeGroup.blur();
    this.select.blur();
    if (type === WFL_IDENTITY_DEPARTMENT) {
      this.setState({ showSelectDepartment: true });
    } else if (type === WFL_IDENTITY_USER_GROUP) {
      this.setState({ showSelectEmployeeGroup: true });
    } else if (type === WFL_IDENTITY_CONTACT) {
      this.setState({ showSelectEmployee: true });
    } else if (type ===  WFL_IDENTITY_COMPANY) {
      this.setState({ showSelectCompany: true });
    }
  };

  handleListCancel = () => {
    this.setState({
      showSelectEmployee: false,
      showSelectEmployeeGroup: false,
      showSelectDepartment: false,
      showSelectCompany: false,
    });
  };

  handleListOk = values => {
    let value;
    const { companySelType } = this.props;
    if((companySelType && values.type === companySelType) || values.type === 'company_oid') {
      value = values.result.map(item => {
        return {
          label: item.name,
          key: item.id,
          value: item.id,
          id: item.id,
          name: item.name,
        }
      })
      this.setState({ selectedList: value }, () => {
        this.setSelectEmployeeText();
        this.onChange(value);
        this.handleListCancel();
      });
    } else if (values.type !== 'bgtUser') {
      value = values.checkedKeys.map(item => {
        return {
          label: item.label,
          key: item.value,
          value: item.value,
          contactId: item.value,
          name: item.label,
        };
      });
      this.setState({ selectedList: value }, () => {
        this.setSelectEmployeeText();
        this.onChange(value);
        this.handleListCancel();
      });
    } else {
      value = values.result.map(item => {
        return {
          label: item.fullName,
          key: item.id,
          value: item.id,
          id: item.id,
          name: item.fullName,
        };
      });
      this.setState({ selectedList: value }, () => {
        this.setSelectEmployeeText();
        this.onChange(value);
        this.handleListCancel();
      });
    }
  };

  onChange = values => {
    const { onChange }= this.props;
    const { type } = this.state;
    if (onChange) {
      onChange({ type, values: values || [] });
    }
  };

  handSelectDept = values => {
    const value = values.map(item => {
      return {
        name: item.name,
        key: item.id,
        id: item.id,
        label: item.name,
      };
    });
    this.setState({ selectedList: value }, () => {
      this.setSelectEmployeeText();
      this.onChange(value);
      this.handleListCancel();
    });
  };

  /**
   * 按人员弹窗保存事件
   */
  handleListSeflectorOk = values => {
    const { data, recordKey } = this.state;
    data[recordKey].solutionParameterList = values.result;
    this.setState({
      data,
    });
  };

  render() {
    const {
      selectEmployeeText,
      type,
      showSelectEmployee,
      showSelectEmployeeGroup,
      showSelectDepartment,
      selectedList,
      showSelectCompany,
    } = this.state;
    const { disabled, hiddenComponents, mode, companySelType } = this.props;
    const textStyle = {
      position: 'absolute',
      top: 3,
      left: 10,
      right: 10,
      width: 180,
      height: 26,
      lineHeight: '26px',
      background: type ===  WFL_IDENTITY_ALL || disabled ? '#f5f5f5' : '#fff',
      color: type === WFL_IDENTITY_ALL || disabled ? 'rgba(0, 0, 0, 0.25)' : 'rgba(0, 0, 0, 0.65)',
      cursor: 'pointer',
    };
    return (
      <div>
        <RadioGroup onChange={this.onApplyEmployee} value={type}>
          {hiddenComponents.indexOf(WFL_IDENTITY_ALL) >= 0 || (
            <Radio disabled={disabled} value={WFL_IDENTITY_ALL}>
              {this.$t('common.all.user')}
            </Radio>
          )}
          {hiddenComponents.indexOf(WFL_IDENTITY_CONTACT) >= 0 || (
            <Radio disabled={disabled} value={WFL_IDENTITY_CONTACT}>
              {this.$t('common.add.by.users')}
            </Radio>
          )}
          {hiddenComponents.indexOf(WFL_IDENTITY_DEPARTMENT) >= 0 || (
            <Radio disabled={disabled} value={WFL_IDENTITY_DEPARTMENT}>
              {this.$t('common.add.by.department')}
            </Radio>
          )}
          {hiddenComponents.indexOf(WFL_IDENTITY_USER_GROUP) >= 0 || (
            <Radio disabled={disabled} value={WFL_IDENTITY_USER_GROUP}>
              {this.$t('common.add.by.user.group')}
            </Radio>
          )}
          {hiddenComponents.indexOf( WFL_IDENTITY_COMPANY) >= 0 || (
            <Radio disabled={disabled} value={WFL_IDENTITY_COMPANY}>
              {this.$t('common.add.by.companys') /* {按公司添加} */}
            </Radio>
          )}
        </RadioGroup>
        {type &&
          type !== WFL_IDENTITY_ALL && (
            <div
              style={{
                position: 'relative',
                width: '100%',
                height: 32,
                lineHeight: '32px',
                marginTop: 10,
              }}
            >
              <Select
                disabled={type === WFL_IDENTITY_ALL || disabled}
                value={[]}
                // ref="selectEmployeeGroup"
                ref={ref => {this.select = ref; return this.select}}
                onFocus={this.showSelectEmployeeGroup}
                dropdownStyle={{ display: 'none' }}
                labelInValue
                style={{ minWidth: 200, maxWidth: 300 }}
              />
              <div
                style={textStyle}
                onClick={() => {
                  if(!disabled)this.showSelectEmployeeGroup();
                }}
              >
                {selectEmployeeText}
              </div>
            </div>
          )}

        {showSelectEmployeeGroup && (
          <SelectEmployeeGroup
            visible={showSelectEmployeeGroup}
            onCancel={this.handleListCancel}
            onOk={this.handleListOk}
            single
            selectedData={selectedList}
            mode={mode}
          />
        )}
        <SelectDepOrPerson
          visible={showSelectDepartment}
          onCancel={this.handleListCancel}
          onOk={this.handleListOk}
          single
          onlyDep
          title="选择部门"
          onConfirm={this.handSelectDept}
          renderButton={false}
          noFooter
          selectedData={selectedList}
          mode={mode}
        />
        <ListSelector
          visible={showSelectEmployee}
          onCancel={this.handleListCancel}
          type="bgtUser"
          // selectorItem={JSON.stringify(itemSelectorItem) === '{}' ? undefined : itemSelectorItem}
          extraParams={{ roleType: 'TENANT' }}
          selectedData={[...selectedList]}
          onOk={this.handleListOk}
          valueKey="id"
          showDetail={false}
        />
        <ListSelector
          visible={showSelectCompany}
          onCancel={this.handleListCancel}
          type={companySelType || 'company_oid'}
          // extraParams={{ setOfBooksId }}
          selectedData={[...selectedList]}
          onOk={this.handleListOk}
          valueKey="id"
          showDetail={false}
        />
      </div>
    );
  }
}

PermissionsAllocation.propTypes = {
  onChange: PropTypes.func, // 进行选择后的回调
  disabled: PropTypes.bool, // 是否可用
  value: PropTypes.object, // 已选择的值 {key: "",value: "",label:""}
  hiddenComponents: PropTypes.array, // 不需要显示的组件 ["all","department","userGroup","contact","company"] 表示这三个组件不需要显示 默认全部都显示
  mode: PropTypes.string,
};

PermissionsAllocation.defaultProps = {
  hiddenComponents: [],
  disabled: false,
  mode: 'id',
  onChange: () => {},
  value: {key: "",value: "",label:""},
};

function mapStateToProps(state) {
  return {
    company: state.user.company,
  };
}
export default connect(mapStateToProps)(PermissionsAllocation);
