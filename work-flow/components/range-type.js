
export  const WFL_IDENTITY_ALL = 'WFL_IDENTITY_ALL';
export  const WFL_IDENTITY_COMPANY = 'WFL_IDENTITY_COMPANY';
export  const WFL_IDENTITY_USER_GROUP = 'WFL_IDENTITY_USER_GROUP';
export  const WFL_IDENTITY_CONTACT = 'WFL_IDENTITY_CONTACT';
export  const WFL_IDENTITY_DEPARTMENT = 'WFL_IDENTITY_DEPARTMENT';

export const symbolText = {
  '9001': '>',
  '9002': '>=',
  '9003': '<',
  '9004': '<=',
  '9005': '=',
}

/**
 * 将[{key: [{},{}],{},}]形式中key对应的值转为 [{},{},{}],并校验成员对象中字段为数组且length === 0
 * [
 *  {key: [{attr:1,value:[],rangeType: 'manual},{attr:2}]},{}
 * ]
 * => [{attr:1,value:[],rangeType: 'manual},{attr:2}]
 * 校验 rangeType为manual且value有成员
 */
export const checkRequiredValue = (list) => {
  let flag = false;
  if(list && Array.isArray(list)) {
    let temp = [];
    list.forEach(item => {
      if(Array.isArray(item.workflowIdentities)) {
        temp = [ ...temp, ...item.workflowIdentities ]
      }
    });

    if(temp.length > 0) {
      temp.some(target => {
        if(
          target.rangeType === 'manual'
          && Array.isArray(target.value)
          && (target.value.length === 0)
        ) {
          flag = true;
          return true;
        }
        return false;
      })
    }
  }
  return flag;
}
