/** 选择发送通知方式和动作 */
import React, { Component } from 'react';
import { Modal, Checkbox, Row, Col, Radio, Divider, message } from 'antd';

const CheckboxGroup = Checkbox.Group;

class AddNoticeAction extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checkedValues: props.checkedValues || [],
      actions: Object.keys(props.actions).map(key => ({ value: key, label: props.actions[key] })),
      type: (props.type && JSON.stringify(props.type) === '[]') || !(props.type instanceof Array)? 1001 : ( props.type.includes(1001) ? 1001 : props.type.includes(1002) ? 1002 : props.type.includes(1003) ? 1002 : '' || 1001),
      values: props.type || [],
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      checkedValues: nextProps.nodeTypeFlag ? ['1002'] : nextProps.checkedValues,
      type: (nextProps.type && JSON.stringify(nextProps.type) === '[]') || !(nextProps.type instanceof Array)? 1001 : ( nextProps.type.includes(1001) ? 1001 : nextProps.type.includes(1002) ? 1002 : nextProps.type.includes(1003) ? 1002 : '' || 1001),
      values: nextProps.type,
    });
  }

  onChange = values => {
    this.setState({ checkedValues: values });
  };

  valuesChange = values => {
    const val = [];
    if (values.length > 0) {
      // eslint-disable-next-line no-plusplus
      for (let i = 0; i < values.length; i++) {
        if (values[i] !== 1001) {
          val.push(values[i]);
        }
      }
    }
    this.setState({ values: val });
  }

  typeChange = e => {
    this.setState({ type: e.target.value });
  }

  onOk = () => {
    const { onOk } = this.props;
    const { checkedValues, type, values } = this.state;

    let mothods = [type];
    if (type === 1002) {

      if (!values || !values.length) {
        message.error("请至少选择一个要通知的客户端！");
        return;
      }
      mothods = values;
    }
    if (!checkedValues.length) {
      message.error("请至少选择一个要通知的动作！");
      return;
    }

    onOk(checkedValues, mothods);
  };

  render() {
    const { visible, onCancel, loading, nodeTypeFlag = false } = this.props;
    const { checkedValues, actions, type, values } = this.state;
    return (
      <Modal
        title="选择发送通知方式和动作"
        visible={visible}
        onOk={this.onOk}
        onCancel={onCancel}
        confirmLoading={loading}
      >
        <Radio.Group onChange={this.typeChange} value={type}>
          <Radio value={1002}>系统通知</Radio>
          <Radio value={1001}>邮件通知</Radio>
        </Radio.Group>
        {type === 1002 && (
          <CheckboxGroup
            value={values}
            style={{ width: '100%' }}
            onChange={this.valuesChange}
          >
            <Row style={{ marginTop: 10 }}>
              <Col span={6}>
                <Checkbox value={1002}>pc</Checkbox>
              </Col>
              <Col span={6}>
                <Checkbox value={1003}>app</Checkbox>
              </Col>
            </Row>
          </CheckboxGroup>
        )
        }
        <Divider />

        <CheckboxGroup
          value={checkedValues.map(o => o.toString())}
          style={{ width: '100%' }}
          onChange={this.onChange}
        >
          <Row>
            {actions.map(item => {
              if(item && String(item.value) !== '1001') {
                return (
                  <Col key={item.value} style={{ marginBottom: 10 }} span={8}>
                    <Checkbox
                      value={item.value}
                      disabled={nodeTypeFlag && String(item.value) !== '1002'}
                    >
                      {item.label}
                    </Checkbox>
                  </Col>
                )
              } else return null;
            })}
          </Row>
        </CheckboxGroup>
      </Modal>
    );
  }
}

export default AddNoticeAction;
