import React, { Component } from 'react';
import ListSelector from 'widget/list-selector'
import config from 'config';
import { Popover, Row, Col, Tag, Empty } from 'antd';
import { symbolText } from './range-type';

class RuleListSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectItem: {
        title: '审批条件',
        url: `${config.wflUrl}/api/wfl/condition?enabled=true`,
        searchForm: [
          { type: 'input', id: 'conditionSetCode', label: '条件代码' },
          { type: 'input', id: 'conditionSetName', label: '条件名称' },
        ],
        columns: [
          { title: '审批条件代码', dataIndex: 'conditionSetCode' },
          {
            title: '审批条件名称', dataIndex: 'conditionSetName',
            render: this.renderRules,
          },
          { title: '说明', dataIndex: 'description' },
        ],
        key: 'id',
      },
      ruleVisibleList: [],
    }
  }

  onOk = values => {
    const { onOk } = this.props;
    if (onOk) {
      onOk(values)
    }
  }

  onCancel = () => {
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel()
    }
  }

  renderRules = (value, record, index) => {
    const { ruleVisibleList } = this.state;
    return (
      <Popover
        title={`${record.conditionSetCode}-${record.conditionSetName}`}
        content={this.renderPop(record.conditionInfo)}
        getPopupContainer={node => node.parentNode}
        overlayClassName="pop-card"
        overlayStyle={{width: '400px'}}
        visible={ruleVisibleList[index]}
      >
        <div>{value}</div>
      </Popover>
    );
  };

  renderPop = (infoList) => {
    return (Array.isArray(JSON.parse(infoList))) ? (
      <div className="ant-popover-box" style={{overflow: 'scroll', maxHeight: '280px', minHeight: '100px'}}>
        {
          JSON.parse(infoList).map((info,index) => {
            const dataIndex = `ruleInfoPop${index}`
            return (
              <Row key={dataIndex} style={{width: '360px'}}>
                <Col
                  span={5}
                  className="ant-form-item-required"
                  style={{textAlign: 'right', padding: '0 5px'}}
                >
                  {`${info.paramName} :`}
                </Col>
                {
                  info.paramType !== 'TEXT' ? (
                    <Col span={18}>{`${info.numberData1} ${symbolText[info.symbol1]} 金额 ${symbolText[info.symbol2]} ${info.numberData2}`}</Col>
                  ) : (
                    <>
                      <Col span={4}>{info.contains === 'include' ? '包含' : '排除'}</Col>
                      {
                        info.limit
                          ? (<Col span={15}>{`${info.limit} ~ ${info.upper}`}</Col>)
                          : (
                            <Col span={15}>
                              {this.renderTag(info.lovParams)}
                            </Col>
                          )
                      }
                    </>
                    )
                }
              </Row>
            )
          })
        }
      </div>
    ) : (
      <Empty />
    )
  }

  renderTag = lists => {
    const list = typeof lists === 'string' ? JSON.parse(lists) : lists;
    if(list) {
      if(Array.isArray(list)) {
        return list.map((param,index) => {
          const dataIndex = `paramName${index}`
          return (<Tag key={dataIndex}>{param.name || param.code || '-'}</Tag>)
        })
      } else if(
        list.selectedRows
        && Array.isArray(list.selectedRows)
      ) {
        return list.selectedRows.map((param,index) => {
          const dataIndex = `paramName${index}`
          return (<Tag key={dataIndex}>{param.name || param.code || '-'}</Tag>)
        })
      } else return null
    } else return null
  }

  // 鼠标移入、移出 行
  onMouseEnterLeave = (index, visible) => {
    const { ruleVisibleList } = this.state;
    ruleVisibleList[index] = visible;
    setTimeout(() => this.setState({ ruleVisibleList }), 200); // 待优化
  };

  render() {
    const { visible, selectedData, extraParams } = this.props;
    const { selectItem } = this.state;

    return (
      <ListSelector
        visible={visible}
        // type='wfl_approval_condition'
        selectorItem={selectItem}
        labelKey="conditionSetName"
        valueKey="id"
        onCancel={this.onCancel}
        onOk={this.onOk}
        selectedData={selectedData}
        extraParams={extraParams}
        onRowMouseEnter={(record, index) => this.onMouseEnterLeave(index, true)}
        onRowMouseLeave={(record, index) => this.onMouseEnterLeave(index, false)}
      />
    )
  }
}

export default RuleListSelector
