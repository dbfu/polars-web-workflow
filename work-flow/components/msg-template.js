import React, { Component } from 'react';
import { Modal } from 'antd'

import BraftEditor from 'braft-editor';
import 'braft-editor/dist/index.css';
import 'styles/setting/announcement-information/announcement-information-detail.scss';


class MessageTemplate extends Component {
  render() {
    const { visible, params, onCancel, onOk } = this.props;
    return (
      <Modal
        title={params.pushMethodName}
        visible={visible}
        width={720}
        onOk={onOk}
        onCancel={onCancel}
      >
        <div className='announcement-information-detail'>
          <span style={{ fontSize: 16 }}>标题：{params.messageTitle}</span>
          <div style={{ marginTop: 12 }}>
            <span style={{ fontSize: 16 }}>正文：</span>
            <BraftEditor
              height={240}
              contentFormat="html"
              initialContent={params.messageContent}
              contentId={params.id}
              controls={[]}
              disabled
              value={params.messageContent}
              media={{
                image: true,
                video: false,
                audio: false,
                externalMedias: {
                  image: false,
                  video: false,
                  audio: false,
                },
                uploadFn: this.handleImageUpload,
              }}
              ref={instance => { this.editorInstance = instance }}
            />
          </div>
        </div>
      </Modal>
    )
  }
}

export default MessageTemplate
