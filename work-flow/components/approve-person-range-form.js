
import React, { Component } from 'react';
import { Button, message } from 'antd';
import PermissionAllocation from './permissions-allocation-id';
import {
  WFL_IDENTITY_ALL,
  WFL_IDENTITY_COMPANY,
  WFL_IDENTITY_USER_GROUP,
  WFL_IDENTITY_CONTACT,
  WFL_IDENTITY_DEPARTMENT,
} from './range-type';

export default class ApprovePersonRangeForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      permissions: {
        type: WFL_IDENTITY_ALL,
        values: [],
      },
      selectData: [],
      selectOids: [],
      approverType: 6301,
    }
  }

  componentDidMount = () => {
    const { range = [] } = this.props;
    if(!range || JSON.stringify(range) === '{}') return;
    const idList = range.selectOids;
    if (idList) {
      this.setState({
        selectOids: range.approverType === WFL_IDENTITY_DEPARTMENT
          ? idList.map(item => item.id)
          : idList,
      });
    }
    if (range.statusTemp === 'new' || range.statusTemp === 'edit') {
      const type = range.approverType;
      this.setState({
        approverType: range.approverType,
        permissions: {
          type,
          values: range.selectOids
            ? range.selectOids.map(item => {
                return {
                  label: type === WFL_IDENTITY_DEPARTMENT ? item.name : '',
                  value: type === WFL_IDENTITY_DEPARTMENT ? item.id : '',
                  name: type === WFL_IDENTITY_DEPARTMENT ? item.name : '',
                  key: type === WFL_IDENTITY_DEPARTMENT ? item.id : '',
                  id: type === WFL_IDENTITY_COMPANY ? item : item.id,
                  contactId: type === WFL_IDENTITY_USER_GROUP ? item : '',
                };
              })
            : [],
        },
      });
    }
  };

  // 保存
  handleSave = () => {
    const { approverType, selectOids,selectData } = this.state;
    const { onOk } = this.props;

    if(onOk) {
      onOk(approverType,selectOids,selectData)
    }
  }

  // 必选校验
  requiredChecked = () => {
    const { approverType, selectOids } = this.state;
    const len = selectOids.length;
    let isSelected = true;
    if(len === 0) {
      switch (approverType) {
        case WFL_IDENTITY_COMPANY:
          message.error('请选择至少一个关于公司的人员范围');
          isSelected = false;
          break;
        case WFL_IDENTITY_USER_GROUP:
          message.error('请选择至少一个关于人员组的人员范围');
          isSelected = false;
          break;
        case WFL_IDENTITY_CONTACT:
          message.error('请选择至少一个关于人员的人员范围');
          isSelected = false;
          break;
        case WFL_IDENTITY_DEPARTMENT:
          message.error('请选择至少一个关于部门的人员范围');
          isSelected = false;
          break;
        default: break;
      }
    }
    return isSelected;
  }

  // 权限更改
  onPermissionChange = values => {
    const nowDepartOrUserIdList = [];

    values.values.map(value => {
      nowDepartOrUserIdList.push(value.value);
      return undefined;
    });

    this.setState({
      approverType: values.type,
      selectOids: nowDepartOrUserIdList,
      selectData:values.values,
    });
  }

  render() {
    const { loading, permissions } = this.state;
    const { handleCancelRangeForm } = this.props;
    return (
      <div className='approve-person-range-form'>
        <PermissionAllocation
          onChange={this.onPermissionChange}
          value={permissions}
          mode='id'
          companySelType='vendor_approve_company'
        />
        <div className="buttons" style={{margin: '5px 0'}}>
          <Button
            type="primary"
            className="save-condition-btn"
            loading={loading}
            onClick={this.handleSave}
            style={{marginRight: 10}}
          >
            {this.$t('common.save')}
          </Button>
          <Button onClick={handleCancelRangeForm}>{this.$t('common.cancel')}</Button>
        </div>
      </div>
    )
  }
}
