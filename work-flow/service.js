
import httpFetch from 'share/httpFetch'

import config from "config"
export default {
  /**
   * 获取工作流类型
   */
  getWorkflowTypeLineData() {
    const url = `${config.wflUrl}/api/wfl/type?enabled=true&page=0&size=999`;
    return httpFetch.get(url);
  },

  /**\
   * 新建工作流行数据
   * @param params
   */
  saveWorkflowLineData(params) {
    return httpFetch.post(`${config.wflUrl}/api/wfl/process/setting`, params);
  },

  /**
   * 编辑工作流行数据
   * @param {*} params
   */
  editWorkflowLineData(params) {
    return httpFetch.put(`${config.wflUrl}/api/wfl/process/setting`, params);
  },

  /**
   * 复制
   * @param {*} idObj ： {sourceId,destinationId}
   */
  copyWorkflowNodeValue(idObj) {
    const url = `${config.wflUrl}/api/wfl/process/setting/copy`;
    return httpFetch.get(url, idObj);
  },

  /**
   * 回滚
   * @param {*} revertObj : {processSettingId,definitionId }
   */
  resetWorkflowLineVersion(revertObj) {
    const url = `${config.wflUrl}/api/wfl/process/setting/revert`;
    return httpFetch.get(url, revertObj)
  },

  /**
   * 根据工作流id获取对应行数据
   * @param {*} id
   */
  getProcessType(id) {
    const url = `${config.wflUrl}/api/wfl/process/setting/${id}`;
    return httpFetch.get(url);
  },

  // 获取页面列表
  getPages(pageName,functionCode) {
    return httpFetch.get(`${config.baseUrl}/api/page/list/query/by/function/code`, { pageName , functionCode })
  },

  // 保存工作流定义内容
  saveContent(id, content) {
    return httpFetch.post(`${config.wflUrl}/api/wfl/process/setting/save`,
      { processSettingId: id, content });
  },

  // 获取工作流定义内容
  getContent(id,parentDefinitionId) {
    const params = { processSettingId: id };
    if(parentDefinitionId && parentDefinitionId !== '0') {
      params.parentDefinitionId = parentDefinitionId
    }
    return httpFetch.get(`${config.wflUrl}/api/wfl/process/definition/current`,params);
  },

  /**
   * 添加催办
   * @param {*} params
   */
  hastenApproval(params) {
    const url = `${config.wflUrl}/api/workflow/hasten/definition/create`;
    return httpFetch.post(url,params);
  },
  /**
   * 获取催办信息
   * @param {*} processSettingId
   * @param {*} nodeCode
   */
  getHastenValue(processSettingId,nodeCode) {
    const url = `${config.wflUrl}/api/workflow/hasten/definition/query?processSettingId=${processSettingId}&nodeCode=${nodeCode}`;
    return httpFetch.get(url);
  },
  /**
   * 获取工作流定义列表页所有数据
   * @param {*} page
   * @param {*} size
   */
  getApprovalChainList(page = 0,size = 999) {
    const url = `${config.wflUrl}/api/wfl/process/setting?page=${page}&size=${size}`;
    return httpFetch.get(url)
  },
  /**
   * 删除催办信息
   * @param {*} id
   */
  deleteHasten(id) {
    const url = `${config.wflUrl}/api/workflow/hasten/definition/delete/${id}`;
    return httpFetch.delete(url)
  }
}
