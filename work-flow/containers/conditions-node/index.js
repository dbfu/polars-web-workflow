
/** 条件节点 */
import React, { Component } from 'react';
import { Form, Input, Button, Row, Icon, Tag } from 'antd';
import InputLanguage from 'components/Widget/Template/input-language'
import RuleListSelector from '../../components/rule-list-selector'
import service from '../../service';

const headerTitle = {
  marginBottom: '14px',
  borderBottom: '1px solid #e8e8e8',
  fontSize: '16px',
  padding: '10px 0',
  fontWeight: 700,
}
const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 12 },
}

class ConditionsNode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nodeRules: [],
      selectedView: false,
    };
  }

  componentDidMount() {
    const { params = {} } = this.props;
    if (JSON.stringify(params) !== '{}' && params.assignees && params.assignees[0]) {
      this.setState({
        nodeRules: params.assignees[0].approvalConditionList,
      })
    }
  }

  componentWillUnmount() {
    this.setState = () => {
      return false;
    }
  }


  // 展开审批条件模态框
  handleOpenSelector = e => {
    e.preventDefault();
    this.setState({ selectorView: true });
  }

  // 关闭审批条件模态框
  handleCancelSelector = () => {
    this.setState({ selectorView: false });
  }

  // 删除已选条件
  handleCloseTag = id => {
    const { nodeRules } = this.state;
    const index = nodeRules.findIndex(rule => id === rule.id);

    if (index >= 0) {
      nodeRules.splice(index, 1);
      this.setState({ nodeRules })
    }
  }

  // 回调获取审批条件模态框数据
  handleSelectConditions = values => {
    const nodeRules = (values.result || []).map(item => ({id: item.id, name: item.conditionSetName}))
    this.setState({ nodeRules }, this.handleCancelSelector);
  }

  // 关闭侧拉
  handleCancel = () => {
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel();
    }
  }

  // 点击保存- 获取数据- 抛出
  handleTempStorage = () => {
    const { form: { validateFieldsAndScroll }, onSave, params = {} } = this.props;
    const { nodeRules } = this.state;

    validateFieldsAndScroll((err, values) => {
      if (err) return;

      const approvalConditions = nodeRules.map(rule => rule.id);
      values.remark = values.remark.value;
      values.i18n = {  remark: values.remark.i18n };
      if (onSave) {
        const oldSetting = !!params.settings ? params.settings : {};
        const tempObj = {
          settings: { ...oldSetting, ...values },
          assignees: [{
            approvalConditionList: rulesList,
            approvalConditions,
          }],
        }
        console.log(tempObj, 'condition-node');
        onSave(tempObj); // 回调，将侧拉框数据抛出
      }
    })
  }

  render() {
    const { params = {}, form: { getFieldDecorator }, processTypeId = '' } = this.props;
    const editFlag = JSON.stringify(params) !== '{}' && params;
    const editParam = editFlag ? params.settings : {};
    const { nodeRules, selectorView } = this.state;

    return (
      <>
        <div style={headerTitle}>基本信息</div>
        <Form>
          <Form.Item {...formItemLayout} label={this.$t('条件代码')}>
            {getFieldDecorator('conditionCode', {
              initialValue: editFlag
                ? editParam.conditionCode
                : undefined,
              rules: [{ required: true, message: this.$t('common.please.enter')}],
            })(
              <Input placeholder={this.$t('common.please.enter')} />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label={this.$t('条件名称')}>
            {getFieldDecorator('remark', {
              initialValue: editFlag
                ? { value: editParam.remark, i18n: editParam.i18n ? editParam.i18n.remark : [] }
                : undefined,
              rules: [{ required: true, message: this.$t('common.please.enter')}],
            })(
              <InputLanguage placeholder={this.$t('common.please.enter')} />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label={this.$t('setting.key1378'/*审批意见*/)}>
            {getFieldDecorator('comments', {
              rules: [{
                max: 50,
                message: this.$t('common.max.characters.length', { max: 50 })
              }],
              initialValue: editFlag ? editParam.comments : undefined,
            })(
              <Input.TextArea rows={2} placeholder={this.$t('common.please.enter')} />
            )}
          </Form.Item>
        </Form>
        {/* 审批条件 */}
        <div style={{ ...headerTitle, marginTop: '16px' }}>审批条件</div>
        <div style={{ padding: '10px' }}>
          <Row>
            <a onClick={e => { this.handleOpenSelector(e) }}>
              <Icon type="plus" style={{ marginRight: '4px' }} />
              添加审批条件
            </a>
          </Row>
          {
            (Array.isArray(nodeRules) && !!nodeRules.length)
            && (
              <Row style={{ padding: '10px 16px' }}>
                <span style={{ marginRight: '10px' }}>{`${this.$t('setting.key1330')} :`}</span>
                {
                  nodeRules.map(rule => (
                    <Tag key={rule.id} closable onClose={() => { this.handleCloseTag(rule.id) }}>
                      {rule.name}
                    </Tag>
                  ))
                }
              </Row>
            )
          }
        </div>
        {/* 底部按钮 */}
        <div className="slide-footer">
          <Button className="btn" type="primary" onClick={this.handleTempStorage}>
            {this.$t('common.ok')}
          </Button>
          <Button className="btn" onClick={this.handleCancel}>
            {this.$t('common.cancel')}
          </Button>
        </div>
        <RuleListSelector
          visible={selectorView}
          type='wfl_approval_condition'
          labelKey="conditionSetName"
          valueKey="id"
          onCancel={this.handleCancelSelector}
          onOk={this.handleSelectConditions}
          selectedData={nodeRules}
          extraParams={{ typeId: processTypeId || '' }}
        />
      </>
    )
  }
}

export default Form.create()(ConditionsNode);
