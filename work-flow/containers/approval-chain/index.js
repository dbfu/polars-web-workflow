/** 审批链 */
import React, { Component } from 'react';
import { Form, Select, message, Radio, Row, Col, Tag, Button, Input, Icon } from 'antd';
import InputLanguage from 'components/Widget/Template/input-language';
import ListSelector from 'widget/list-selector';
import FlowDesign from "../../flow-design/flow-design";
import service from '../../service';

const headerTitle = {
  marginBottom: '14px',
  borderBottom: '1px solid #e8e8e8',
  fontSize: '16px',
  padding: '10px 0',
  fontWeight: 700,
}
const formItemStyle = {
  marginBottom: '10px'
}

class ApprovalChain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      approvalList: [], // 审批链数组
      notifyFlag: 1001, // 审批类型
      parameters: [], // 分组审批参数
      paramSelectorView: false,
      approvalNodeData: {}, // 下拉选择的审批链数据
    }
  }

  componentWillMount() {
    const { params } = this.props;
    this.setState({
      notifyFlag: (params.assignmentMethod && params.assignmentMethod.type)
        ? params.assignmentMethod.type
        : 1001,
      parameters: (params.assignmentMethod && params.assignmentMethod.type)
        ? params.assignmentMethod.parameters
        : [],
    })
  }

  componentDidMount() {
    service
      .getApprovalChainList()
      .then(res => {
        if (res.data) {
          this.setState({ approvalList: res.data })
        }
      })
      .catch(err => {
        message.error(err.response.data.message)
      })
  }

  /**
   * 改变审批类型，当为普通时，需清空分组审批下勾选的参数数据
   * @param e: antd radio 控件提供的回调对象
   */
  handleChangeNotifyFlag = e => {
    let { parameters } = this.state;
    if (String(e.target.value) === '1001') {
      parameters = []
    }
    this.setState({
      notifyFlag: e.target.value,
      parameters,
    })
  }

  // 分组审批参数-模态框可见
  handleGroupParamVisible = (switchFlag, e) => {
    if (e) e.preventDefault();
    if (switchFlag === 'close') {
      this.setState({ paramSelectorView: false });
    } else if (switchFlag === 'open') {
      this.setState({ paramSelectorView: true });
    }
  }

  /**
   * 回调获取分组参数数据
   * @param result: list
   */

  handleSelectParam = ({ result }) => {
    if (result) {
      this.setState({
        parameters: result.map(item => ({ id: item.id, name: item.paramName, code: item.paramCode })),
      }, () => {
        this.handleGroupParamVisible('close')
      })
    }
  }

  /**
   * tag 删除勾选的分组参数
   */
  handleDelParamTag = id => {
    const { selectedParams } = this.state;
    const index = selectedParams.findIndex(item => item.id === id);
    if (index >= 0) {
      selectedParams.splice(index, 1);
      this.setState({ selectedParams })
    }
  }

  /**
   * 下拉选择 审批链
   * @param value: 审批链id (processSettingId)
   */
  changeApprovalChainData = value => {
    if (value) {
      service
        .getContent(value)
        .then(res => {
          this.setState({
            approvalNodeData: JSON.parse(res.data.content),
          })
        })
        .catch(err => {
          message.error(err.response.data.message)
        })
    } else {
      this.setState({ approvalNodeData: {} })
    }
  }

  // 关闭侧拉
  handleCancel = () => {
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel();
    }
  }

  // 点击保存- 获取数据- 抛出
  handleTempStorage = () => {
    const { form: { validateFieldsAndScroll }, onSave, params = {} } = this.props;

    validateFieldsAndScroll((err, values) => {
      if (err) return;
      const { notifyFlag, parameters } = this.state;

      values.remark = values.remark.value;
      values.i18n = {
        remark: values.remark.i18n,
      }
      const oldSetting = !!params.settings ? params.settings : {};

      if (onSave) {
        const tempObj = {
          settings: { ...oldSetting, ...values },
          assignmentMethod: {
            type: notifyFlag,
            parameters,
          },
        }
        console.log(tempObj);
        onSave(tempObj); // 回调，将侧拉框数据抛出
      }
    })
  }

  dataChange = node => {
    console.log(node);
    this.setState({
      approvalNodeData: (JSON.parse(JSON.stringify(node)))
    });
  }

  render() {
    const { form: { getFieldDecorator }, params = {}, processTypeId = '' } = this.props;
    const { approvalList, notifyFlag, parameters, paramSelectorView, approvalNodeData } = this.state;
    const editFlag = JSON.stringify(params) !== "{}" && params.settings;
    const editParam = params.settings;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 12 },
    };

    return (
      <div className="wfl-approval-chain">
        <div style={headerTitle}>{this.$t('common.baseInfo')}</div>
        <Form>
          <Form.Item {...formItemLayout} label={this.$t('setting.key1372')} style={formItemStyle}>
            {getFieldDecorator('remark', {
              rules: [{
                required: true,
                message: this.$t('common.please.enter'), // 请输入
              }],
              initialValue: editFlag ? {
                value: editParam.remark,
                i18n: editParam.i18n ? editParam.i18n.remark : [],
              } : undefined,
            })(
              <InputLanguage
                placeholder={this.$t('common.please.enter')}
                style={{ width: '100%' }}
              />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label={this.$t('wfl.approval.chain')}>
            {
              getFieldDecorator('processSettingId', {
                rules: [{
                  required: true,
                  message: this.$t('common.please.select'),
                }],
                initialValue: editFlag ? editParam.processSettingId : undefined,
              })(
                <Select
                  placeholder={this.$t('common.please.select')}
                  onChange={this.changeApprovalChainData}
                  allowClear
                  getPopupContainer={node => node.parentNode}
                  showSearch
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {
                    Array.isArray(approvalList)
                    && !!approvalList.length
                    && approvalList.map(item => (
                      <Select.Option key={item.id}>{item.processName}</Select.Option>
                    ))
                  }
                </Select>
              )
            }
          </Form.Item>
          <Form.Item {...formItemLayout} label={this.$t('common.comment')}>
            {getFieldDecorator('comments', {
              rules: [{
                max: 50,
                message: this.$t('common.max.characters.length', { max: 50 })
              }],
              initialValue: editFlag ? editParam.comments : undefined,
            })(
              <Input.TextArea rows={2} placeholder={this.$t('common.please.enter')} />
            )}
          </Form.Item>
        </Form>
        <div style={headerTitle}>{this.$t('wfl.approval.person')}</div>
        <Radio.Group
          value={notifyFlag}
          onChange={this.handleChangeNotifyFlag}
        >
          <Radio value={1001}>{this.$t('wfl.approval.normal')}</Radio>
          <Radio value={1002} style={{ marginLeft: '10px' }}>{this.$t('wfl.approval.subgroup')}</Radio>
        </Radio.Group>
        {/* 分组审批 */}
        {
          String(notifyFlag) === '1002' && (
            <Row style={{
              borderBottom: '1px solid #e8e8e8',
              paddingBottom: '5px',
              margin: '5px 0',
            }}>
              <Col span={4} style={{ fontWeight: 600 }}>分组审批参数:</Col>
              <Col span={20}>
                {
                  Array.isArray(parameters)
                  && !!parameters.length
                  && parameters.map(param => {
                    return (
                      <Tag
                        key={param.id}
                        closable
                        onClose={() => { this.handleDelParamTag(param.id) }}
                      >
                        {param.name}
                      </Tag>
                    )
                  })
                }
                <span>
                  <a onClick={(e) => { this.handleGroupParamVisible('open', e) }}>
                    <Icon type="plus" />{this.$t('common.add')}
                  </a>
                </span>
              </Col>
            </Row>
          )
        }
        {/* 审批链缩略图 */}
        <div>
          {
            approvalNodeData
            && JSON.stringify(approvalNodeData) !== '{}'
            && (
              <FlowDesign
                id="mini-map"
                data={approvalNodeData}
                onDataChange={this.dataChange}
                disabledEvent
              />
            )
          }
        </div>
        {/* 底部按钮 */}
        <div className="slide-footer">
          <Button className="btn" type="primary" onClick={this.handleTempStorage}>
            {this.$t('common.ok')}
          </Button>
          <Button className="btn" onClick={this.handleCancel}>
            {this.$t('common.cancel')}
          </Button>
        </div>
        {/* 分组审批参数 */}
        <ListSelector
          visible={paramSelectorView}
          type='wfl_group_param_list'
          labelKey="conditionSetName"
          valueKey="id"
          onCancel={() => { this.handleGroupParamVisible('close') }}
          onOk={this.handleSelectParam}
          selectedData={parameters}
          extraParams={{ typeId: processTypeId }}
        />
      </div>
    )
  }
}

export default Form.create()(ApprovalChain);
