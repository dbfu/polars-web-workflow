/** 审批节点 */
import React, { Component } from 'react';
import { Form, Divider, Button, Select, Row, Col, Radio, message, Icon } from 'antd';
import SystemHasten from './system-hasten'
import WithdrawHasten from './withdraw-hasten'
import AssessingOfficer from './assessing-officer'
import InputLanguage from 'components/Widget/Template/input-language'
import debounce from 'lodash/debounce'
import { checkRequiredValue } from '../../components/range-type';
import service from '../../service';

const RadioGroup = Radio.Group;
const formItemStyle = {
  marginBottom: '10px'
}
const headerTitle = {
  marginBottom: '14px',
  borderBottom: '1px solid #e8e8e8',
  fontSize: '16px',
  padding: '10px 0',
  fontWeight: 700,
}
const hastenType = {
  "1002": 'app',
  "1001": 'pc',
  "1003": '邮件',
}

class ApprovalNode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      saveLoading: false,
      pages: [],
      addSystemHastenVisible: false, //系统催办
      addWithdrawHastenVisible: false,//申请人催办
      systemHastenValues: {},
      withdrawHastenValues: {},
      workflowHastenTypesDtoList: [],
      finalSystemHasten: {},  // 最终保存的时候抛出去的系统催办数据
      finalWithdrawHasten: {}, // 最终保存的时候抛出去的申请人催办数据
      processSettingId: props.processSettingId,
      rulesList: [], // assessing-officer中的数据集合
    }
    this.handleSearch = debounce(this.handleSearch, 300)
  }

  componentDidMount() {
    const { params } = this.props;
    if(JSON.stringify(params) !== '{}' && params) {
      this.setState({
        rulesList: params.assignees,
      },() => {
        this.getHastenValueList();
      })
    }
  }

  // 获取催办信息
  getHastenValueList = () => {
    const { processSettingId = '', nodeCode = '' } = this.props;
    if(processSettingId && nodeCode) {
      service
        .getHastenValue(processSettingId,nodeCode)
        .then(res => {
          if(res && Array.isArray(res.data)) {
            const sysValue = this.dealWithHastenTypesDtoList(res.data[0]);
            const manValue = this.dealWithHastenTypesDtoList(res.data[1]);

            this.setState({
              workflowHastenTypesDtoList: [...sysValue,...manValue],
              finalSystemHasten: res.data[0] && res.data[0].hastenType ? (res.data[0].hastenType === 'MANUAL' ? (res.data[1] || {}) : res.data[0]) : {},
              finalWithdrawHasten: res.data[0] && res.data[0].hastenType ? (res.data[0].hastenType === 'MANUAL' ? res.data[0] : (res.data[1]|| {})) : {},
            })
          }
        })
        .catch(err => {
          console.log(err);
          message.error(err.response.data.message)
        })
    }
  }

  // 节点页面
  handleSearch = value => {
    if (!value) {
      this.setState({ pages: [] });
      return;
    }
    service.getPages(value,"WORKFLOW_BACKLOG").then(res => {
      this.setState({ pages: res.data });
    })
  }

  // 关闭
  handleCancel = () => {
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel(true);
    }
  }

  // 添加手动催办
  addWithdrawHasten = () => {
    const { workflowHastenTypesDtoList } = this.state
    const item = Array.isArray(workflowHastenTypesDtoList) && workflowHastenTypesDtoList.find(data => data.hastenType === 'MANUAL')
    this.setState({ addWithdrawHastenVisible: true, withdrawHastenValues: item })
  }

  // 添加系统催办
  addSystemHasten = () => {
    const { workflowHastenTypesDtoList } = this.state;

    const item = Array.isArray(workflowHastenTypesDtoList) &&
      workflowHastenTypesDtoList.find(data => (data.hastenType === 'SYSTEM'));

    this.setState({ addSystemHastenVisible: true, systemHastenValues: item })
  }

  //删除系统催办
  addSystemHastenDelete = (e) => {
    e.preventDefault();
    const { finalSystemHasten } = this.state;
    if(!finalSystemHasten.id) return;

    service
      .deleteHasten(finalSystemHasten.id)
      .then(() => {
        message.success('删除成功');
        this.setState({
          finalSystemHasten: {},
          workflowHastenTypesDtoList: [],
        },() => {
          this.getHastenValueList();
        })
      })
      .catch(err => {
        message.error(err.response.data.message)
      })
  }

  //删除申请人催办
  addWithdrawHastenDelete = (e) => {
    e.preventDefault();
    const { finalWithdrawHasten } = this.state;
    if(!finalWithdrawHasten.id) return;

    service
      .deleteHasten(finalWithdrawHasten.id)
      .then(() => {
        message.success('删除成功');
        this.setState({
          finalWithdrawHasten: {},
          workflowHastenTypesDtoList: [],
        },() => {
          this.getHastenValueList();
        })
      })
      .catch(err => {
        message.error(err.response.data.message)
      })
  }

  //系统催办保存提交
  addSystemHastenHandle = (value) => {
    const { processSettingId } = this.state;
    const { hastenTimeType, hastenTimeValue, holidaysFlag, hastenConditions = [] } = value;

    const conditions = [];
    // const { nodeId, processSettingId } = this.props;
    for (let i = 0; i < hastenTimeType.length; i++) {
      if (hastenTimeType[i] !== undefined) {
        const condition = {}
        condition.hastenTimeType = hastenTimeType[i]
        condition.hastenTimeValue = hastenTimeValue[i]
        condition.holidaysFlag = holidaysFlag[i] || false,
        conditions.push({ ...hastenConditions[i], ...condition});
      }
    }
    value.hastenConditions = conditions
    //避免后台接收时属性转化错误
    value.hastenTimeType = ''
    value.hastenTimeValue = ''
    value.holidaysFlag = ''

    value.processSettingId = processSettingId;
    value.hastenType = 'SYSTEM'
    value.manyTimesHasten = false;

// console.log(value)
    const tempArr = this.dealWithHastenTypesDtoList(value)
    const { workflowHastenTypesDtoList } = this.state;
    const tempArrHasten = workflowHastenTypesDtoList.filter(item => (item.hastenType !== 'SYSTEM'));
    this.setState({
      addSystemHastenVisible: false,
      workflowHastenTypesDtoList: [ ...tempArrHasten,...tempArr ],
      finalSystemHasten: value,
      systemHastenValues: value,
    })
  }

  //申请人催办保存提交
  addWithdrawHastenHandle = (value) => {
    const { hastenTimeType, hastenTimeValue, holidaysFlag, hastenConditions = [] } = value;
    const { processSettingId } = this.state;

    const conditions = [];
    // const { nodeId, processSettingId } = this.props;
    for (let i = 0; i < hastenTimeType.length; i++) {
      if (hastenTimeType[i] !== undefined) {
        const condition = {}
        condition.hastenTimeType = hastenTimeType[i]
        condition.hastenTimeValue = hastenTimeValue[i]
        condition.holidaysFlag = holidaysFlag[i] || false;
        conditions.push({ ...hastenConditions[i], ...condition})
      }
    }
    value.hastenConditions = conditions
    //避免后台接收时属性转化错误

    value.intervalDurationValue = value.intervalDurationValue === "" ? null : value.intervalDurationValue

    value.processSettingId = processSettingId;
    value.hastenType = 'MANUAL'
    value.hastenTimeType = ''
    value.hastenTimeValue = ''
    value.holidaysFlag = ''

    const tempArr = this.dealWithHastenTypesDtoList(value)
    const { workflowHastenTypesDtoList } = this.state;
    const tempArrHasten = workflowHastenTypesDtoList.filter(item => (item.hastenType !== 'MANUAL'));

    this.setState({
      addWithdrawHastenVisible: false,
      workflowHastenTypesDtoList: [ ...tempArrHasten,...tempArr ],
      finalWithdrawHasten: value,
      withdrawHastenValues: value,
    })
  }

  /**
   * 模拟后台保存催办后返回的数据格式
   * （原本催办是实时调用接口，workflowHastenTypesDtoList来自后台处理好的格式）
   * 现在由于接口不在这里实时调用，但迎合原有渲染逻辑，只能通过函数模拟
   */
  dealWithHastenTypesDtoList = value => {
    const workflowHastenTypesDtoList = [];
    if (value) {
      (value.hastenConditions || []).map(item => {
        const temp = {
          ...value,
          ...item,
          id: item.id,
          hastenId: value.id,
          hastenConditions: value.hastenConditions,
        }
        workflowHastenTypesDtoList.push(temp);
      })
    }

    return workflowHastenTypesDtoList
  }


  handleSave = () => {
    const { form: { validateFieldsAndScroll }, onSave, params = {} } = this.props;

    validateFieldsAndScroll((err, values) => {
      if (err) return;
      const { finalSystemHasten, finalWithdrawHasten, rulesList, processSettingId } = this.state;
      if(values.pageId) {
        const temp = values.pageId;
        values.pageId = temp.key;
        values.pageName = temp.label;
      }
      const rules = rulesList;
      let tempRules = [];
      if(rules.length >=0 ) {
        tempRules = rules.map(item => ({
          interfaceId: item.interfaceId,
          id: item.id,
          type: 'condition',
          workflowIdentities: item.workflowIdentities,
          approvalConditionsList: item.approvalConditionsList || [],
          approvalConditions: item.approvalConditions || [],
          approvalRuleName: item.approvalRuleName,
        }))
      }
      // 校验审批人员带出的必输参数是否有值
      const flag = checkRequiredValue(tempRules);
      if(flag) {
        message.warning(`${this.$t("workflow.data.is.not.entered.in.required.fields")}...`)
        return;
      }

      values.remark = values.remark.value;
      values.i18n = { remark: values.remark.i18n };
      const oldSettings = !!params.settings ? params.settings : {};
      const newSettings = { ...oldSettings, ...values  };

      const tempObj = {
        settings: newSettings,
        systemHasten: (finalSystemHasten && finalSystemHasten.hastenType)
          ? finalSystemHasten
          : {
              hastenType: "SYSTEM",
              processSettingId,
            },
        manualHasten: (finalWithdrawHasten && finalWithdrawHasten.hastenType)
          ? finalWithdrawHasten
          : {
            hastenType: "MANUAL",
            processSettingId,
          },
        assignees: tempRules,
      }

      if(onSave) {
        onSave(tempObj);
      }
    })
  }

  // 抛出普通通知人添加的条件数据
  returnValue = rulesList => {
    if (rulesList.length >= 0) {
      this.setState({ rulesList });
    }
  }

  /**
   * 催办切换为否时，清除数据
   * @param str: 催办类型
   * @param value: radio切换的值 boolean (true: 是, false: 否)
   */
  clearHasten = (value,str) => {
    if(value) return;
    const { finalSystemHasten, finalWithdrawHasten } = this.state;
    if(str === 'SYSTEM') {
      this.setState({
        finalSystemHasten: { ...finalSystemHasten, hastenConditions: [] },
        systemHastenValues: {}
      })
    } else if(str === 'MANUAL') {
      this.setState({
        finalWithdrawHasten: { ...finalWithdrawHasten, hastenConditions: [] },
        withdrawHastenValues: {}
      })
    }
  }


  render() {
    const {
      saveLoading,
      pages,
      addSystemHastenVisible,
      addWithdrawHastenVisible,
      workflowHastenTypesDtoList,
      finalSystemHasten,
      finalWithdrawHasten,
      rulesList,
    } = this.state;
    const { form: { getFieldDecorator, getFieldValue }, params = {}, processTypeId = '' } = this.props;
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 19 },
    };
    const editFlag = JSON.stringify(params) !== "{}" && params.settings;
    const editParam = typeof params.settings === 'string'
      ? JSON.parse(params.settings)
      : params.settings;

    return (
      <>
        <Form>
          <div style={headerTitle}>{this.$t('common.baseInfo')}</div>
          <Form.Item {...formItemLayout} label={this.$t("workflow.node.name")} style={formItemStyle}>
            {getFieldDecorator('remark', {
              rules: [{
                required: true,
                message: this.$t('common.please.enter'), // 请输入
              }],
              initialValue: editFlag ? {
                value: editParam.remark,
                i18n: editParam.i18n ? editParam.i18n.remark: [],
              } : undefined ,
            })(
              <InputLanguage
                placeholder={this.$t('common.please.enter')}
                style={{ maxWidth: 300, width: '100%' }}
              />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label='节点页面' style={formItemStyle}>
            {getFieldDecorator('pageId', {
              initialValue: (editFlag && editParam.pageId) ? { key: editParam.pageId, label: editParam.pageName } : { key: '', label: ''},
            })(
              <Select
                labelInValue
                showSearch
                placeholder={this.$t('common.please.select')}
                defaultActiveFirstOption={false}
                showArrow={false}
                filterOption={false}
                onSearch={this.handleSearch}
                notFoundContent={null}
                style={{ maxWidth: 300, width: '100%' }}
                allowClear
                getPopupContainer={node => node.parentNode}
              >
                {pages.map(item => (
                  <Select.Option key={item.id}>{item.pageName}</Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <div style={{...headerTitle, marginTop: '16px'}}>审批规则</div>
          <Form.Item {...formItemLayout} label='审批规则' style={formItemStyle}>
            {getFieldDecorator('countersignRule', {
              initialValue: editFlag ? editParam.countersignRule : 3001,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <Radio.Group>
                <Radio value={3001}>所有人通过</Radio>
                <Radio value={3002}>一票通过/一票驳回</Radio>
                <Radio value={3003}>任一人通过</Radio>
              </Radio.Group>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='包含申请人' style={formItemStyle}>
            {getFieldDecorator('selfApprovalRule', {
              initialValue: editFlag ? editParam.selfApprovalRule : 5001,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <RadioGroup>
                <Radio value={5002}>{this.$t('setting.key1384') /** 不跳过 */}</Radio>
                <Radio value={5001}>{this.$t('setting.key1383') /** 跳过 */}</Radio>
                <Radio value={5003}>{this.$t("workflow.automatic.examination.and.approval") /** 自动审批 */}</Radio>
              </RadioGroup>
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label='包含提单人' style={formItemStyle}>
            {getFieldDecorator('submitterApprovalRule', {
              initialValue: editFlag ? editParam.submitterApprovalRule : 5001,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <RadioGroup>
                <Radio value={5002}>{this.$t('setting.key1384') /** 不跳过 */}</Radio>
                <Radio value={5001}>{this.$t('setting.key1383') /** 跳过 */}</Radio>
                <Radio value={5003}>{this.$t("workflow.automatic.examination.and.approval") /** 自动审批 */}</Radio>
              </RadioGroup>
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label='节点为空时' style={formItemStyle}>
            {getFieldDecorator('nullableRule', {
              initialValue: editFlag ? editParam.nullableRule : 2001,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <Radio.Group>
                <Radio value={2001}>{this.$t('setting.key1383') /** 跳过 */}</Radio>
                <Radio value={2002}>{this.$t('setting.key1384') /** 不跳过 */}</Radio>
              </Radio.Group>
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label='是否重复审批' style={formItemStyle}>
            {getFieldDecorator('repeatRule', {
              initialValue: editFlag ? editParam.repeatRule : 4002,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <RadioGroup>
                <Radio value={4001}>{this.$t('setting.key1393') /** 是 */}</Radio>
                <Radio value={4002}>{this.$t('setting.key1394') /** 否 */}</Radio>
              </RadioGroup>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='是否允许加签' style={formItemStyle}>
            {getFieldDecorator('addsignFlag', {
              initialValue: editFlag ? editParam.addsignFlag : false,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <Radio.Group>
                <Radio value>{this.$t('setting.key1393') /** 是 */}</Radio>
                <Radio value={false}>{this.$t('setting.key1394') /** 否 */}</Radio>
              </Radio.Group>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='是否允许转交' style={formItemStyle}>
            {getFieldDecorator('transferFlag', {
              initialValue: editFlag ? editParam.transferFlag : false,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <Radio.Group>
                <Radio value>是</Radio>
                <Radio value={false}>否</Radio>
              </Radio.Group>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='允许退回指定节点' style={formItemStyle}>
            {getFieldDecorator('returnFlag', {
              initialValue: editFlag ? editParam.returnFlag : true,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <Radio.Group onChange={this.returnFlagChange}>
                <Radio value>{this.$t('setting.key1393') /** 是 */}</Radio>
                <Radio value={false}>{this.$t('setting.key1394') /** 否 */}</Radio>
              </Radio.Group>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='可退回节点' style={formItemStyle}>
            {getFieldDecorator('returnType', {
              initialValue: editFlag ? editParam.returnType : 1001,
            })(
              <Radio.Group>
                <Radio value={1001}>本节点前该子流程及主流程内节点</Radio>
                <Radio value={1002}>子流程内任意节点</Radio>
                <Radio value={1003}>自选节点</Radio>
              </Radio.Group>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='系统催办' style={formItemStyle}>
            {getFieldDecorator('systemFlag', {
              initialValue: editFlag ? editParam.systemFlag : false,
            })(
              <Radio.Group onChange={(e) => { this.clearHasten(e.target.value,'SYSTEM') }}>
                <Radio value>{this.$t('setting.key1393') /** 是 */}</Radio>
                <Radio value={false}>{this.$t('setting.key1394') /** 否 */}</Radio>
                {getFieldValue('systemFlag') === true && (
                  <span>
                    {getFieldValue('systemFlag') === true && workflowHastenTypesDtoList
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM')
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM').length === 0 && (
                        <Button icon="plus-circle" type='link' onClick={() => this.addSystemHasten()}>添加催办</Button>
                      )}
                    {getFieldValue('systemFlag') === true && workflowHastenTypesDtoList
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM')
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM').length > 0 && (
                        <span>
                          <a onClick={() => this.addSystemHasten()}>{this.$t('common.edit')}</a>
                          <Divider type='vertical' />
                          <a onClick={(e) => this.addSystemHastenDelete(e)}>{this.$t('common.delete')}</a>
                        </span>
                      )}
                  </span>
                )}
              </Radio.Group>
            )}
          </Form.Item>
          {
            getFieldValue('systemFlag') === true && workflowHastenTypesDtoList
            && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM')
            && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM').map((item, index) => {
              const dataIndex = `system${index}`
              return (
                <Row key={dataIndex}>
                  <Col offset={5}>
                    <Row>
                      <Col span={2}>
                        催办{index + 1}:
                        </Col>
                      <Col span={10}>
                        <div>距{item.hastenTimeType === '1001' ? '到达节点时间' : '单据提交时间'}超过
                            {item.hastenTimeValue}天({item.holidaysFlag === false ? '含' : '不含'}节假日)
                          </div>
                        {item.hastenChannel && (
                          <div>
                            通过{item.hastenChannel.map(item => hastenType[item]).join("、")}提醒当前节点审批人
                            </div>)}
                      </Col>

                    </Row>
                  </Col>
                </Row>
              )
            })
          }

          <Form.Item {...formItemLayout} label='申请人催办' style={formItemStyle}>
            {getFieldDecorator('withdrawFlag', {
              initialValue: editFlag ? editParam.withdrawFlag : false,
            })(
              <Radio.Group onChange={(e) => { this.clearHasten(e.target.value,'MANUAL') }}>
                <Radio value={true}>{this.$t('setting.key1393') /** 是 */}</Radio>
                <Radio value={false}>{this.$t('setting.key1394') /** 否 */}</Radio>
                {getFieldValue('withdrawFlag') === true && (
                  <span>
                    {getFieldValue('withdrawFlag') === true && workflowHastenTypesDtoList
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL')
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL').length === 0 && (
                        <Button icon="plus-circle" type='link' onClick={() => this.addWithdrawHasten()}>添加催办</Button>
                      )}
                    {getFieldValue('withdrawFlag') === true && workflowHastenTypesDtoList
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL')
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL').length > 0 && (
                        <span>
                          <a onClick={() => this.addWithdrawHasten()}>{this.$t('common.edit')}</a>
                          <Divider type='vertical' />
                          <a onClick={(e) => this.addWithdrawHastenDelete(e)}>{this.$t('common.delete')}</a>
                        </span>
                      )}
                  </span>
                )}
              </Radio.Group>
            )}
          </Form.Item>
          {
            getFieldValue('withdrawFlag') === true && workflowHastenTypesDtoList
            && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL')
            && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL').map((item, index) => {
              const dataIndex = `manual${index}`;
              return (
                <Row key={dataIndex}>
                  <Col offset={5}>
                    <Row>
                      <Col span={2}>
                        催办{index + 1}:
                        </Col>
                      <Col span={10}>
                        <div>距{item.hastenTimeType === '1001' ? '到达节点时间' : '单据提交时间'}超过
                            {item.hastenTimeValue}天({item.holidaysFlag === false ? '含' : '不含'}节假日)
                          </div>
                        {!item.intervalDurationValue && (<div>{item.manyTimesHasten === true ? '可' : '不可'}重复催办</div>)}
                        {(item.intervalDurationValue || String(item.intervalDurationValue) === '0') &&
                          (<div>
                            间隔时长:{item.intervalDurationValue}
                            {item.manualHastenType === '1001' ? '天' : '小时'}</div>)
                        }
                        {item.hastenChannel && (
                          <div>通过{
                            item.hastenChannel.map(item => hastenType[item]).join("、")
                          }提醒当前节点审批人</div>)}
                        {(item.manualHastenType === '1002' || item.manualHastenType === '1003') && (
                          <div>
                            通过{item.manualHastenType === '1002' ? '自主选择' : '手工添加催办消息'}提醒当前节点审批人
                            </div>
                        )}
                      </Col>
                    </Row>
                  </Col>
                </Row>
              )
            })
          }
          <div style={{...headerTitle, marginTop: '16px'}}>审批人员</div>
          <Form.Item wrapperCol={{ span: 16 }} style={formItemStyle}>
            {getFieldDecorator('notifyFlag', {
              initialValue: (editFlag && params.assignmentMethod && params.assignmentMethod.type)
                ? params.assignmentMethod.type
                : 1001,
            })(
              <Radio.Group onChange={this.handleChangeNotifyFlag}>
                <Radio value={1001}>普通审批人</Radio>
                <Radio value={1002}>分组审批</Radio>
              </Radio.Group>
            )
            }
          </Form.Item>
          {/* 普通审批 */}
          <AssessingOfficer
            notifyFlag={getFieldValue('notifyFlag')}
            processTypeId={processTypeId}
            ref={ref=> {this.assess = ref}}
            rulesList={rulesList}
            returnValue={this.returnValue}
          />
        </Form>


        {/* 添加系统催办 */}
        {addSystemHastenVisible && (
          <SystemHasten
            onCancel={() => { this.setState({ addSystemHastenVisible: false }) }}
            visible={addSystemHastenVisible}
            onOk={this.addSystemHastenHandle}
            values={finalSystemHasten}
          />
        )}


        {/* 添加申请人催办 */}
        {addWithdrawHastenVisible && (
          <WithdrawHasten
            onCancel={() => { this.setState({ addWithdrawHastenVisible: false }) }}
            visible={addWithdrawHastenVisible}
            onOk={this.addWithdrawHastenHandle}
            values={finalWithdrawHasten}
          />)}

        <div className="slide-footer">
          <Button className="btn" type="primary" loading={saveLoading} onClick={this.handleSave}>
            {this.$t('common.ok')}
          </Button>
          <Button className="btn" onClick={this.handleCancel}>
            {this.$t('common.cancel')}
          </Button>
        </div>
      </>
    )
  }
}

export default Form.create()(ApprovalNode);
