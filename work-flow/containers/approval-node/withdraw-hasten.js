import React, { Component } from 'react';
import { Modal, Checkbox, Row, Col, Form, Select, Radio, Icon, Button, InputNumber } from 'antd'
import { connect } from 'dva'

const CheckboxGroup = Checkbox.Group;
const FormItem = Form.Item;
const { Option } = Select;

let id = 0;

class WithdrawHasten extends Component {
  constructor(props) {
    super(props);

    this.state = {
      values:{},
    };

  }

  componentDidMount() {
    id=0;
    const {values} = this.props;
    if (values){
      if (values.hastenConditions && values.hastenConditions.length-1){
        for (let i = 0; i < values.hastenConditions.length-1; i+=1) {
          this.add()
        }
      }
      this.setState({values})
    }

  }


  onOk = (e) => {
    e.preventDefault();
    const { onOk ,form,values } = this.props;
    form.validateFieldsAndScroll((err, value) => {
      if (err){
        return;
      }

      const parame = {...values}
      parame.hastenTimeType = value.hastenTimeType;
      parame.hastenTimeValue=value.hastenTimeValue;
      parame.holidaysFlag= value.holidaysFlag || false;
      parame.intervalDurationUnit= value.intervalDurationUnit;
      parame.intervalDurationValue=value.intervalDurationValue;
      parame.manualHastenType= value.manualHastenType;
      parame.manyTimesHasten= value.manyTimesHasten;
      parame.hastenChannel= value.hastenChannel;
      if (!parame.manyTimesHasten){
        parame.intervalDurationUnit ="";
        parame.intervalDurationValue="";
      }
      if (!(parame.manualHastenType === "1001")) {
        parame.hastenChannel = [];
        parame.hastenChannelValue = [];
      }

      onOk(parame);
    })
  };

  remove = k => {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    if (keys.length === 0) {
      return;
    }

    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  };

  add = () => {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    id +=1;
    const nextKeys = keys.concat(id);
    form.setFieldsValue({
      keys: nextKeys,
    });
  };

  render() {
    const { visible, onCancel, loading ,form} = this.props;
    const { values } = this.state;
    const { getFieldDecorator ,getFieldValue} =form;
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 16 },
    };

    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k) => (
      <Row gutter={28} key={`withdrawHasten${k}`}>
        <Col span={12} offset={2}>
          <FormItem {...formItemLayout} label='催办条件'>
                距&nbsp;&nbsp;
            {getFieldDecorator(`hastenTimeType[${k}]`, {
                  initialValue:values && values.hastenConditions && values.hastenConditions[k] && values.hastenConditions[k].hastenTimeType || "1001",
                  rules: [{
                    required: true,
                    message: this.$t('common.please.enter'), // 请输入
                  }],
                })(
                  <Select
                    placeholder="请选择"
                    style={{ maxWidth: 200, width: '60%' }}
                  >
                    <Option value="1001">到达节点时间</Option>
                    <Option value="1002">单据提交时间</Option>
                  </Select>
                )}
          </FormItem>
        </Col>
        <Col span={6} pull={4}>
          <FormItem>
            {getFieldDecorator(`hastenTimeValue[${k}]`, {
                  rules: [{
                    required: true,
                    message: this.$t('common.please.enter'), // 请输入
                  }],
                  initialValue: (
                    values
                    && values.hastenConditions
                    && values.hastenConditions[k]
                    && (values.hastenConditions[k].hastenTimeValue >=0)
                  )
                    ? values.hastenConditions[k].hastenTimeValue
                    : null,
                })(<InputNumber precision={0} step={1} placeholder={this.$t({ id: 'common.please.enter' })} min={0} style={{ maxWidth: 300, width: '60%' }} />)}
                &nbsp; 天 &nbsp;
          </FormItem>
        </Col>
        <Col span={3} pull={5}>
          <FormItem>
            {getFieldDecorator(`holidaysFlag[${k}]`, {
                  valuePropName: 'checked',
                  initialValue: values && values.hastenConditions && values.hastenConditions[k] && values.hastenConditions[k].holidaysFlag,
                })(
                  <Checkbox style={{width: '180%',marginTop:'10px',textOverflow:'ellipsis',whiteSpace: 'nowrap'}}>不计算节假日</Checkbox>
                )}
          </FormItem>
        </Col>

        {keys.length > 0 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            onClick={() => this.remove(k)}
            style={{color:'red'}}
          />
        ) : null}

      </Row>
    ));

    return (
      <Modal
        title="添加申请人催办"
        visible={visible}
        onOk={this.onOk}
        onCancel={onCancel}
        confirmLoading={loading}
        width="60%"
      >
        <Form>
          <Row gutter={28}>
            <Col span={12} offset={2}>
              <FormItem {...formItemLayout} label='催办条件'>
                距&nbsp;&nbsp;
                {getFieldDecorator('hastenTimeType[0]', {
                  initialValue:(values && values.hastenConditions && values.hastenConditions[0] && values.hastenConditions[0].hastenTimeType) || "1001",
                  rules: [{
                    required: true,
                    message: this.$t('common.please.enter'), // 请输入
                  }],
                })(
                  <Select
                    placeholder="请选择"
                    style={{ maxWidth: 200, width: '60%' }}
                  >
                    <Option value="1001">到达节点时间</Option>
                    <Option value="1002">单据提交时间</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={6} pull={4}>
              <FormItem>
                {getFieldDecorator('hastenTimeValue[0]', {
                  rules: [{
                    required: true,
                    message: this.$t('common.please.enter'), // 请输入
                  }],
                  initialValue: (
                    values
                    && values.hastenConditions
                    && values.hastenConditions[0]
                    && values.hastenConditions[0].hastenTimeValue >= 0
                  )
                    ? values.hastenConditions[0].hastenTimeValue
                    : null,
                })(<InputNumber precision={0} step={1} placeholder={this.$t({ id: 'common.please.enter' })} min={0} style={{ maxWidth: 300, width: '60%' }} />)}
                &nbsp; 天 &nbsp;
              </FormItem>
            </Col>
            <Col span={3} pull={5}>
              <FormItem>
                {getFieldDecorator('holidaysFlag[0]', {
                  valuePropName: 'checked',
                  initialValue: (
                    values
                    && values.hastenConditions
                    && values.hastenConditions[0]
                    && values.hastenConditions[0].holidaysFlag
                  ) || false,
                })(
                  <Checkbox style={{width: '180%',marginTop:'10px',textOverflow:'ellipsis',whiteSpace: 'nowrap'}}>不计算节假日</Checkbox>
                )}
              </FormItem>
            </Col>
          </Row>
          {formItems}

          <FormItem {...formItemLayout} label=' ' colon={false}>
            {getFieldDecorator('add', {
              initialValue: values && values.hastenChannel|| null,
            })(
              <Button type="dashed" onClick={this.add} style={{ width: '60%' }}>
                <Icon type="plus" /> 添加催办条件
              </Button>
            )}
          </FormItem>

          <Row>
            <Col span={24}>
              <FormItem {...formItemLayout} label='可多次催办'>
                {getFieldDecorator('manyTimesHasten', {
                  initialValue: values && values.manyTimesHasten|| false,
                })(
                  <Radio.Group>
                    <Radio value>是</Radio>
                    <Radio value={false}>否</Radio>
                  </Radio.Group>
                )}
              </FormItem>
            </Col>
          </Row>
          {form.getFieldValue('manyTimesHasten') === true && (
          <Row>
            <Col span={24}>
              <FormItem {...formItemLayout} label='间隔时长'>
                {getFieldDecorator('intervalDurationValue', {
                  rules: [{
                    required: true,
                    message: this.$t('common.please.enter'), // 请输入
                  }],
                  initialValue: (values && (values.intervalDurationValue >= 0)) ? values.intervalDurationValue : null,
                })(<InputNumber precision={0} step={1} placeholder={this.$t({ id: 'common.please.enter' })} min={0} style={{ maxWidth: 300, width: '30%' }} />)}
                &nbsp;&nbsp;
                {getFieldDecorator('intervalDurationUnit', {
                  initialValue:values && values.intervalDurationUnit || "1001",
                  rules: [{
                    required: true,
                    message: this.$t('common.please.enter'), // 请输入
                  }],
                })(
                  <Select
                    placeholder="请选择"
                    style={{ maxWidth: 300, width: '50%' }}
                  >
                    <Option value="1001">天</Option>
                    <Option value="1002">小时</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
          </Row>
          )}

          <Row>
            <Col span={24}>
              <FormItem {...formItemLayout} label='催办方式'>
                {getFieldDecorator('manualHastenType', {
                  rules: [{
                    required: true,
                    message: this.$t('common.please.enter'), // 请输入
                  }],
                  initialValue: values && values.manualHastenType|| "1001",
                })(
                  <Radio.Group style={{ width: '100%' }}>
                    <Radio value="1001">固定方式</Radio>
                    <Radio value="1002">自主选择</Radio>
                    <Radio value="1003">自定义消息</Radio>
                  </Radio.Group>
                )}
              </FormItem>
            </Col>
          </Row>
          {form.getFieldValue('manualHastenType') === "1001" && (
          <Row>
            <Col span={24}>
              <FormItem {...formItemLayout} label=' ' colon={false}>
                {getFieldDecorator('hastenChannel', {
                  initialValue: values && values.hastenChannel|| null,
                  rules: [{
                    required: true,
                    message: this.$t('common.please.enter'), // 请输入
                  }],
                })(
                  <CheckboxGroup
                    // defaultValue={values && values.hastenChannel|| null}
                    style={{ width: '100%' }}
                  >
                    <Col span={6}>
                      <Checkbox value="1001">pc</Checkbox>
                    </Col>
                    <Col span={6}>
                      <Checkbox value="1002">app</Checkbox>
                    </Col>
                    <Col span={6}>
                      <Checkbox value="1003">邮件</Checkbox>
                    </Col>
                  </CheckboxGroup>
                )}
              </FormItem>
            </Col>
          </Row>
          )}
        </Form>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    language: state.languages,
  };
}

const wrappedWithdrawHasten = Form.create()(WithdrawHasten);

export default connect(mapStateToProps)(wrappedWithdrawHasten);

