import React, { Component } from 'react';
import { Modal, Checkbox, Row, Col, Form, Select, Icon, Button, InputNumber } from 'antd'
import { connect } from 'dva'

const CheckboxGroup = Checkbox.Group;
const FormItem = Form.Item;
const { Option } = Select;

let id = 0;

class SystemHasten extends Component {
  constructor(props) {
    super(props);
    this.state = {
      values: {},
    };

  }

  componentDidMount() {
    id = 0;
    const { values } = this.props;
    if (values) {
      // const hastenConditions = values && values.hastenConditions.filter(data => data.hastenType === "SYSTEM") || [];
      // values.hastenConditions = hastenConditions;
      if (values.hastenConditions && values.hastenConditions.length - 1) {
        for (let i = 0; i < values.hastenConditions.length - 1; i += 1) {
          this.add()
        }
      }
      this.setState({ values })
    }
  }

  onOk = (e) => {
    const { onOk, form, values } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, value) => {
      if (err) {
        return;
      }
      const parame = { ...values, ...value }
      onOk(parame);
    })
  };

  remove = k => {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    if (keys.length === 0) {
      return;
    }

    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  };

  add = () => {
    const { form } = this.props;
    const keys = form.getFieldValue('keys');
    id += 1;
    const nextKeys = keys.concat(id);
    form.setFieldsValue({
      keys: nextKeys,
    });
  };

  render() {
    const { visible, onCancel, loading, form } = this.props;
    const { values } = this.state;
    const { getFieldDecorator, getFieldValue } = form;
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 16 },
    };

    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k) => (
      <Row gutter={28} key={`systemHasten${k}`}>
        <Col span={12} offset={2}>
          <FormItem {...formItemLayout} label='催办条件'>
            距&nbsp;&nbsp;
            {getFieldDecorator(`hastenTimeType[${k}]`, {
              initialValue: values && values.hastenConditions && values.hastenConditions[k] && values.hastenConditions[k].hastenTimeType || "1001",
              rules: [{
                required: true,
                message: this.$t('common.please.enter'), // 请输入
              }],
            })(
              <Select
                placeholder="请选择"
                style={{ maxWidth: 200, width: '60%' }}
              >
                <Option value="1001">到达节点时间</Option>
                <Option value="1002">单据提交时间</Option>
              </Select>
            )}
          </FormItem>
        </Col>
        <Col span={6} pull={4}>
          <FormItem>
            {getFieldDecorator(`hastenTimeValue[${k}]`, {
              rules: [{
                required: true,
                message: this.$t('common.please.enter'), // 请输入
              }],
              initialValue: (
                values
                && values.hastenConditions
                && values.hastenConditions[k]
                && values.hastenConditions[k].hastenTimeValue >= 0)
                ? values.hastenConditions[k].hastenTimeValue
                : null,
            })(<InputNumber precision={0} step={1} placeholder={this.$t({ id: 'common.please.enter' })} min={0} style={{ maxWidth: 300, width: '60%' }} />)}
            &nbsp; 天 &nbsp;
          </FormItem>
        </Col>
        <Col span={3} pull={5}>
          <FormItem>
            {getFieldDecorator(`holidaysFlag[${k}]`, {
              valuePropName: 'checked',
              initialValue: (values && values.hastenConditions && values.hastenConditions[k] && values.hastenConditions[k].holidaysFlag) || false,
            })(
              <Checkbox style={{ width: '180%', marginTop: '10px', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>不计算节假日</Checkbox>
            )}
          </FormItem>
        </Col>

        {keys.length > 0 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            onClick={() => this.remove(k)}
            style={{ color: 'red' }}
          />
        ) : null}

      </Row>
    ));

    return (
      <Modal
        title="添加系统催办"
        visible={visible}
        onOk={this.onOk}
        onCancel={onCancel}
        confirmLoading={loading}
        width="60%"
      >
        <Form>
          <Row gutter={28}>
            <p syule={{ padding: '0 80px' }}>
              <Icon type="info-circle" style={{ color: '#52ACFF' }} />
              不同催办条件之间为OR的关系
            </p>
            <Col span={12} offset={2}>
              <FormItem {...formItemLayout} label='催办条件'>
                距&nbsp;&nbsp;
                {getFieldDecorator('hastenTimeType[0]', {
                  initialValue: values && values.hastenConditions && values.hastenConditions && values.hastenConditions[0] && values.hastenConditions[0].hastenTimeType || "1001",
                  rules: [{
                    required: true,
                    message: this.$t('common.please.enter'), // 请输入
                  }],
                })(
                  <Select
                    placeholder={this.$t('common.please.select')}
                    style={{ maxWidth: 200, width: '60%' }}
                  >
                    <Option value="1001">到达节点时间</Option>
                    <Option value="1002">单据提交时间</Option>
                  </Select>
                )}
              </FormItem>
            </Col>
            <Col span={6} pull={4}>
              <FormItem>
                {getFieldDecorator('hastenTimeValue[0]', {
                  rules: [{
                    required: true,
                    message: this.$t('common.please.enter'), // 请输入
                  }],
                  initialValue: (
                    values
                    && values.hastenConditions
                    && values.hastenConditions[0]
                    && values.hastenConditions[0].hastenTimeValue >=0)
                    ? values.hastenConditions[0].hastenTimeValue
                    : null,
                })(<InputNumber precision={0} step={1} placeholder={this.$t({ id: 'common.please.enter' })} min={0} style={{ maxWidth: 300, width: '60%' }} />)}
                &nbsp; 天 &nbsp;
              </FormItem>
            </Col>
            <Col span={3} pull={5}>
              <FormItem>
                {getFieldDecorator('holidaysFlag[0]', {
                  valuePropName: 'checked',
                  initialValue: (
                    values
                    && values.hastenConditions
                    && values.hastenConditions[0]
                    && values.hastenConditions[0].holidaysFlag
                  ) || false ,
                })(
                  <Checkbox style={{ width: '180%', marginTop: '10px', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>不计算节假日</Checkbox>
                )}
              </FormItem>
            </Col>
          </Row>

          {formItems}

          <FormItem {...formItemLayout} label=' ' colon={false}>
            {getFieldDecorator('add', {
            })(
              <Button type="dashed" onClick={this.add} style={{ width: '60%' }}>
                <Icon type="plus" /> 添加催办条件
              </Button>
            )}
          </FormItem>

          <Row>
            <Col span={24}>
              <FormItem {...formItemLayout} label='催办方式'>
                {getFieldDecorator('hastenChannel', {
                  initialValue: values && values.hastenChannel || null,
                  rules: [{
                    required: true,
                    message: this.$t('common.please.enter'), // 请输入
                  }],
                })(
                  <CheckboxGroup
                    // defaultValue={values && values.hastenChannel || null}
                    style={{ width: '100%' }}
                  >
                    <Col span={6}>
                      <Checkbox value="1001">pc</Checkbox>
                    </Col>
                    <Col span={6}>
                      <Checkbox value="1002">app</Checkbox>
                    </Col>
                    <Col span={6}>
                      <Checkbox value="1003">邮件</Checkbox>
                    </Col>
                  </CheckboxGroup>
                )}
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    language: state.languages,
  };
}

const wrappedSystemHasten = Form.create()(SystemHasten);

export default connect(mapStateToProps)(wrappedSystemHasten);
