import React, { Component } from 'react';
import { Icon, Card, Badge, Divider, Row, Tag, Col, Popover, Empty } from 'antd';
import ListSelector from 'widget/list-selector'
import Lov from "widget/Template/lov/list-selector"
import RuleListSelector from '../../components/rule-list-selector'
import { symbolText } from '../../components/range-type';

class AssessingOfficer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selPersonVisible: false,
      // processTypeId: props.processTypeId, // 工作流类型id
      rulesList: [], // 审批人（审批规则）数据
      selectorView: false,
      selectedRules: [], // 审批条件
      lovCode: '',
      lovVisible: false,
      id: '',
      paramIndex: '',
      curSelectArr: [],
      idByCon: '', // 审批人员id,用于确定数据结构中目标对象位置
      defaultRulesList: [], // 默认勾选的审批人员数据
      valueCode: '',
      valueCodeVisible: false,
      singleFlag: false, // 审批人员（规则）的字段值单选或多选
    }
  }

  componentDidMount() {
    const { rulesList } = this.props;
    if (rulesList && Array.isArray(rulesList)) {
      this.setState({ rulesList });
    }
    /**
     * 编辑状态下，由于后台返回数据无法全部用于界面展示，故可能需要接口
     * 根据rulesList ids(list集合) 获取其对应数据，并合并当前rulesList
     */
  }

  componentWillReceiveProps(nextProps) {
    const { rulesList } = nextProps;
    if (rulesList && Array.isArray(rulesList)) {
      this.setState({ rulesList });
    }
  }

  componentWillUnmount() {
    this.setState = () => {
      return false;
    }
  }

  // 展开模态框-根据radio单选结果
  handleSpreadSelect = e => {
    e.preventDefault();
    const { notifyFlag } = this.props;
    const { rulesList } = this.state;
    if (notifyFlag === 1001) {
      this.setState({
        selPersonVisible: true,
        defaultRulesList: Array.isArray(rulesList)
          ? rulesList.map(item => ({ id: item.id }))
          : [],
      })
    }
  }

  // 回调获取多选的审批人（审批规则）数据
  okHandleSelPersonList = values => {
    if (values.result && values.result.length) {
      const { rulesList } = this.state;
      const newRulesList = values.result;
      const tempArr = [];

      newRulesList.forEach(item => {
        if (item.ruleInterfaceInfo) {
          const infoArr = JSON.parse(item.ruleInterfaceInfo);

          const workflowIdentities = infoArr.map(info => {
            const temp = {
              keyCode: info.fieldName,
              enKeyCode: info.name, // 用于keyCode的多语言展示
              rangeType: info.approaches,
              value: [],
              paramType: info.paramType,
            }
            if (info.approaches === "manual" && info.lovCode) {
              temp.lovCode = info.lovCode;
            } else if (info.approaches === "manual" && info.valueCode) {
              temp.valueCode = info.valueCode; // (值列表取valueCode)
            }
            if(info.approaches === 'dataField') {
              const tempCodeObj = info.matchingfield.constructor === Object ? info.matchingfield : {};
              temp.value = [tempCodeObj.structCode,tempCodeObj.fieldCode]
            }
            return temp;
          })
          tempArr.push({
            // id: item.id,
            // ruleid: item.ruleid,
            id: item.id,
            interfaceId: item.ruleInterfaceId,
            approvalRuleName: item.approvalRuleName,
            workflowIdentities,
          });
        }
      })

      this.setState({ rulesList: [ ...rulesList, ...tempArr], selPersonVisible: false },() => {
        const { returnValue } = this.props;
        if(returnValue) { // 向上级传递数据
          returnValue([ ...rulesList, ...tempArr])
        }
      });
    }
  }

  // 回调获取审批条件模态框数据
  handleSelectConditions = values => {
    const { rulesList, idByCon } = this.state;
    const index = rulesList.findIndex(rule => rule.id === idByCon);
    if (index < 0 || typeof index !== 'number') return;
    rulesList[index].approvalConditionsList = (values.result || []).map(item => (
      {id: item.id, name: item.conditionSetName, conditionInfo: item.conditionInfo, code: item.conditionSetCode}
    ));
    rulesList[index].approvalConditions = (values.result || []).map(item => item.id);
    this.setState({
      selectedRules: values.result,
      rulesList,
    }, () => {
      this.handleCancelSelector();
      const { returnValue } = this.props;
      if(returnValue) {
        returnValue(rulesList)
      }
    });
  }

  // 关闭审批条件模态框
  handleCancelSelector = () => {
    this.setState({
      selectorView: false,
      idByCon: '',
    });
  }

  /**
   * 删除已选条件
   * @param id: 需删除的条件id
   * @param ruleIndex: 该条件id对应数组在ruleList中的位置
   */
  handleCloseTag = (id,ruleIndex) => {
    const { rulesList } = this.state;
    if(
      !Array.isArray(rulesList)
      || !rulesList[ruleIndex]
      || !Array.isArray(rulesList[ruleIndex].approvalConditionsList)
    ) return;
    const conditionIndex = rulesList[ruleIndex].approvalConditionsList.findIndex(item => item.id === id);
    if(conditionIndex >= 0) {
      rulesList[ruleIndex].approvalConditionsList.splice(conditionIndex, 1);
      rulesList[ruleIndex].approvalConditions.splice(conditionIndex, 1);
      this.setState({ rulesList },() => {
        const { returnValue } = this.props;
        if(returnValue) {
          returnValue(rulesList)
        }
      })
    }
  }

  /**
   * 渲染已选条件 tag 上的 popover
   */
  renderPop = (infoList) => {
    return (infoList && typeof infoList === 'string' && Array.isArray(JSON.parse(infoList))) ? (
      <div className="ant-popover-box" style={{overflow: 'scroll', maxHeight: '280px', minHeight: '160px'}}>
        {
          JSON.parse(infoList).map((info,index) => {
            const dataIndex = `ruleInfoPop${index}`
            return (
              <Row key={dataIndex} style={{width: '320px'}}>
                <Col
                  span={4}
                  className="ant-form-item-required"
                  style={{textAlign: 'right', padding: '0 5px'}}
                >
                  {`${info.paramName} :`}
                </Col>
                {
                  info.paramType !== 'TEXT' ? (
                    <Col span={18}>{`${info.numberData1} ${symbolText[info.symbol1]} 金额 ${symbolText[info.symbol2]} ${info.numberData2}`}</Col>
                  ) : (
                    <>
                      <Col span={4}>{info.contains === 'include' ? '包含' : '排除'}</Col>
                      {
                        info.limit
                          ? (<Col span={16}>{`${info.limit} ~ ${info.upper}`}</Col>)
                          : (
                            <Col span={16}>
                              {this.renderTag(info.lovParams)}
                            </Col>
                          )
                      }
                    </>
                    )
                }
              </Row>
            )
          })
        }
      </div>
    ) : (
      <Empty />
    )
  }

  renderTag = lists => {
    const list = typeof lists === 'string' ? JSON.parse(lists) : lists;
    if(list) {
      if(Array.isArray(list)) {
        return list.map((param,index) => {
          const dataIndex = `paramName${index}`
          return (<Tag key={dataIndex}>{param.name}</Tag>)
        })
      } else if(
        list.selectedRows
        && Array.isArray(list.selectedRows)
      ) {
        return list.selectedRows.map((param,index) => {
          const dataIndex = `paramName${index}`
          return (<Tag key={dataIndex}>{param.name}</Tag>)
        })
      } else return null
    } else return null
  }

  /**
   * 展开参数选择模态框，定位需要添加参数的数据处于ruleList中的位置
   * @param id: 审批人员（审批条件id）
   * @param paramIndex: id对应ruleList数组中存放参数的位置，每个参数通过模态框获取list集合
   */
  handleOpenLovToAdd = (e, item, id, paramIndex) => {
    e.preventDefault();
    const { rulesList } = this.state;
    const interfaceIndex = rulesList.findIndex(rule => rule.id === id);
    if (interfaceIndex < 0) return;
    const curSelectArr = rulesList[interfaceIndex].workflowIdentities[paramIndex].valueObj || [];

    if (item.lovCode) {
      this.setState({
        lovCode: item.lovCode,
        lovVisible: true,
        id,
        paramIndex,
        curSelectArr, // 当前参数已勾选的值
        singleFlag: !(item.paramType === 'object' || item.paramType === 'array'),
      });
    } else if(item.valueCode) {
      // 值列表展开
      this.setState({
        valueCode: item.valueCode,
        valueCodeVisible: true,
        id,
        paramIndex,
        curSelectArr, // 当前参数已勾选的值
        singleFlag: !(item.paramType === 'object' || item.paramType === 'array'),
      });
    }
  }

  // 将参数的弹窗勾选数据保存到审批人员数组中
  handleGetLovValue = (value,type) => {
    const { id, paramIndex, rulesList } = this.state;
    const index = rulesList.findIndex(item => item.id === id);
    const values = Array.isArray(value) ? value : [value];

    if (index < 0 || paramIndex < 0 || typeof paramIndex !== 'number') return;

    const tempValueCodeList = values.map(item => (
      {code: item.code, id: type === 'valueCode' ? item.value : item.id, name: item.name, value: item.value}
    ));
    // const tempValueCodeList = values.map(item => item.code);
    const tempValueIdList = values.map(item => {
      return (type === 'valueCode' ? item.value : item.id)
    });
    if (rulesList[index].workflowIdentities[paramIndex]) {
      rulesList[index].workflowIdentities[paramIndex].value = tempValueIdList;
      rulesList[index].workflowIdentities[paramIndex].valueObj = tempValueCodeList;
    }
    else {
      rulesList[index].workflowIdentities[paramIndex] = {};
      rulesList[index].workflowIdentities[paramIndex].value = tempValueIdList;
      rulesList[index].workflowIdentities[paramIndex].valueObj = tempValueCodeList;
    }

    this.setState({
      lovVisible: false,
      id: '',
      paramIndex: '',
      rulesList,
      valueCodeVisible: false,
    },() => {
      const { returnValue } = this.props;
      if(returnValue) {
        returnValue(rulesList)
      }
    });
  }

  /**
   * 删除参数对应选择的数据
   * @param valueCode: 弹窗勾选的唯一性标识,同时也是渲染在tag上的
   * @param paramIndex: id确定的ruleIndex中的workflowIdentityRuleMap数组中的对象索引
   * @param id: 审批人员（审批条件id）
   */
  handleDelParamTag = (valueCode, paramIndex, id) => {
    const { rulesList } = this.state;
    const index = rulesList.findIndex(rule => rule.id === id);
    const codeIndex = rulesList[index].workflowIdentities[paramIndex].value.findIndex(param => param.id === valueCode);

    rulesList[index].workflowIdentities[paramIndex].value.splice(codeIndex, 1);
    rulesList[index].workflowIdentities[paramIndex].valueObj.splice(codeIndex, 1);
    this.setState({ rulesList },() => {
      const { returnValue } = this.props;
      if(returnValue) {
        returnValue(rulesList)
      }
    });
  }

  /**
   * 渲染参数
   * @param rule: ruleList中的每个成员
   * rule: { id:'', workflowIdentities: [{value:[]},{}]}
   * id: 审批人员/规则id
   * value: 根据类型定义渲染的参数对应的值集合
   */
  renderParameters = (rule) => {
    const { workflowIdentities } = rule;
    return (
      workflowIdentities.map((item, index) => {
        const dataIndex = `item${index}`;
        return (
          <Row style={{ padding: '10px 16px' }} key={dataIndex}>
            <Col
              span={5}
              style={{paddingRight: '16px', textAlign: 'right'}}
              className={item.rangeType !== 'manual' ? "" : "ant-form-item-required"}
            >
              {/* {item.enKeyCode ? (`${this.$t(item.enKeyCode)}： `) : (`${item.keyCode} :`)} */}
              {
                item.enKeyCode
                  ? this.renderParametersTitle(this.$t(item.enKeyCode))
                  : this.renderParametersTitle(item.keyCode)
              }
            </Col>
            <Col span={18}>
              {
                item.rangeType === 'dataField' ? (
                  <Tag>{`${item.value[0] || ''}-${item.value[1] || ''}`}</Tag>
                ) : (
                    item.rangeType === 'empty' ? `${this.$t('constants.bookerType.blank')}` : (
                      <span>
                        {
                          item.valueObj && Array.isArray(item.valueObj) && item.valueObj.map(value => (
                            <Tag
                              key={value.id}
                              closable
                              onClose={() => { this.handleDelParamTag(value.id, index, rule.id) }}
                            >
                              {value.name || value.id}
                            </Tag>
                          ))
                        }
                        <a onClick={e => { this.handleOpenLovToAdd(e, item, rule.id, index) }}>
                          <Icon type="plus" style={{ marginRight: '4px' }} />{this.$t('common.add')}
                        </a>
                      </span>
                    )
                  )
              }
            </Col>
          </Row>
        )
      })
    )
  }

  /** 截取渲染参数的字段名 */
  renderParametersTitle = title => {
    if(!title) return '-'
    return (title.length > 9) ? (
      <Popover content={title}>{`${title.substring(0,9)}... :`}</Popover>
    ) : (
      <Popover content={title}>{`${title} :`}</Popover>
    )
  }

  /**
   * 添加条件
   * @param idByCon: ruleList中每个成员的id
   * 此处用于确定条件添加的目标对象
   */
  handleAddCondition = (e, idByCon) => {
    e.preventDefault();
    const { rulesList } = this.state;
    const index = rulesList.findIndex(rule => rule.id === idByCon);
    const temp = rulesList[index].approvalConditionsList;

    this.setState({
      selectorView: true,
      idByCon,
      selectedRules: Array.isArray(temp) ? temp : [],
    })
  }

  // 删除添加的审批人员
  handleDelete = (e, id) => {
    e.preventDefault();
    const { rulesList } = this.state;
    const index = rulesList.findIndex(rule => rule.id === id);
    rulesList.splice(index, 1);
    this.setState({ rulesList },() => {
      const { returnValue } = this.props;
      if(returnValue) {
        returnValue(rulesList)
      }
    })
  }

  render() {
    const {
      selPersonVisible,
      rulesList,
      selectorView,
      selectedRules,
      lovCode,
      lovVisible,
      curSelectArr,
      defaultRulesList,
      valueCode,
      valueCodeVisible,
      singleFlag,
    } = this.state;
    const { processTypeId, type = 'approval' } = this.props;

    return (
      <>
        <a onClick={e => { this.handleSpreadSelect(e) }}>
          <Icon type="plus" style={{ marginRight: '4px' }} />
          {type === 'notice' ? '添加通知人' : '添加审批人员'}
        </a>
        {
          Array.isArray(rulesList) && !!rulesList.length ? (
            rulesList.map((rule, index) => {
              const dataIndex = `rule${index}`
              return (
                <Card
                  title={(
                    <span>
                      <Badge status="default" text={rule.approvalRuleName} />
                    </span>
                  )}
                  style={{ marginBottom: '10px', borderBottom: '1px solid #e8e8e8' }}
                  headStyle={{ height: '24px', borderBottom: '0' }}
                  bodyStyle={{ padding: '10px' }}
                  bordered={false}
                  extra={(
                    <span>
                      <a onClick={e => { this.handleAddCondition(e, rule.id) }}>添加条件</a>
                      <Divider type="vertical" />
                      <a onClick={e => { this.handleDelete(e, rule.id) }}>{this.$t('common.delete')}</a>
                    </span>
                  )}
                  key={dataIndex}
                >
                  {
                    rule.workflowIdentities
                    && !!rule.workflowIdentities.length
                    && this.renderParameters(rule, index)
                  }
                  {
                    Array.isArray(rule.approvalConditionsList)
                    && !!rule.approvalConditionsList.length
                    && (
                      <Row style={{ padding: '10px 16px' }}>
                        <Col span={5} style={{paddingRight: '16px', textAlign: 'right'}}>
                          <span>{`${this.$t('setting.key1330')} :`}</span>
                        </Col>
                        <Col span={18}>
                          {
                            rule.approvalConditionsList.map(item => (
                              <span key={item.id}>
                                <Popover
                                  title={`${item.code}-${item.name}`}
                                  content={this.renderPop(item.conditionInfo)}
                                  getPopupContainer={node => node.parentNode}
                                  overlayClassName="pop-card"
                                  overlayStyle={{width: '400px'}}
                                >
                                  <Tag key={item.id} closable onClose={() => { this.handleCloseTag(item.id,index) }}>
                                    {item.name || item.id}
                                  </Tag>
                                </Popover>
                              </span>
                            ))
                          }
                        </Col>
                      </Row>
                    )
                  }
                </Card>
              )
            })
          ) : null
        }
        {/* 添加审批人员 */}
        <ListSelector
          onOk={this.okHandleSelPersonList}
          onCancel={() => { this.setState({ selPersonVisible: false }) }}
          visible={selPersonVisible}
          valueKey="id"
          labelKey="approvalRuleName"
          type="wfl_approval_rules"
          extraParams={{ typeId: processTypeId }}
          selectedData={defaultRulesList}
        />
        {/* 审批条件 */}
        <RuleListSelector
          visible={selectorView}
          type='wfl_approval_condition'
          labelKey="conditionSetName"
          valueKey="id"
          onCancel={this.handleCancelSelector}
          onOk={this.handleSelectConditions}
          selectedData={selectedRules}
          extraParams={{ typeId: processTypeId }}
        />
        <Lov
          code={lovCode}
          valueKey='id'
          onOk={this.handleGetLovValue}
          onCancel={() => { this.setState({ lovVisible: false }) }}
          allowClear={false}
          visible={lovVisible}
          value={singleFlag ? curSelectArr[0] : curSelectArr}
          single={singleFlag}
        />
        {/* 值列表 */}
        <ListSelector
          visible={valueCodeVisible}
          type='wfl_value_list'
          valueKey="value"
          onCancel={() => { this.setState({ valueCodeVisible: false }) }}
          onOk={({result}) => {this.handleGetLovValue(result,'valueCode')}}
          selectedData={curSelectArr}
          extraParams={{code: valueCode}}
          single={singleFlag}
        />
      </>
    )
  }
}

export default AssessingOfficer;
