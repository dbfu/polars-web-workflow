/** 通知设置 */
import React, { Component } from 'react';
import { Row, Switch, Icon, Button } from 'antd';
import AddNoticeAction from '../../components/add-notice-action'
import SingleNoticeCard from './single-notice-card';
import service from '../../service';

const defaultActionList = {
  1001: '审批人手动通知',
  1002: '节点到达',
  1003: '审批通过',
  1004: '审批驳回',
  1005: '转交',
  1006: '加签',
  1007: '退回指定节点',
  1008: '撤回',
}

class NoticeSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      informFlag: false, // 手动通知
      addNoticeActionVisible: false, // 通知动作模态框
      finalActionsList: [], // 通知数据 [{noticeType:[],noticeActions:[]},{}]
      selectedValue: {}, // 单条勾选的通知信息，(selectedCheckedValues,selectedType)
      nodeTypeFlag: false,
      finalSystemHasten: {},
      finalWithdrawHasten: {},
    };
  }

  componentWillMount() {
    const { nodeType = '' } = this.props;
    if (nodeType === 'start' || nodeType === 'end') {
      this.setState({ nodeTypeFlag: true });
      // 判断节点类型是否是开始或结束节点
    }
  }

  componentDidMount() {
    const { params = {} } = this.props;

    if (params && JSON.stringify(params) !== '{}') {
      this.setState({
        informFlag: params.settings ? params.settings.informFlag : false,
        finalActionsList: params.notifications || [],
      })
    }
    // 这里需要单独调用一次查询催办信息的接口，保证只在修改了通知的情况下，催办的版本号能正确
    // 催办信息是单独存表
    this.getHastenValueList();
  }

  componentWillReceiveProps() {
    const { nodeType = '', params = {} } = this.props;
    if (nodeType === 'start' || nodeType === 'end') {
      this.setState({ nodeTypeFlag: true });
      // 判断节点类型是否是开始或结束节点
    }
    if (params && JSON.stringify(params) !== '{}') {
      this.setState({
        informFlag: params.settings ? params.settings.informFlag : false,
        finalActionsList: params.notifications || [],
      })
    }
  }

  componentWillUnmount() {
    this.setState = () => {
      return false;
    }
  }

  // 获取催办信息
  getHastenValueList = () => {
    const { processSettingId = '', nodeCode = '' } = this.props;
    if (processSettingId && nodeCode) {
      service
        .getHastenValue(processSettingId, nodeCode)
        .then(res => {
          if (res && Array.isArray(res.data)) {
            this.setState({
              finalSystemHasten: res.data[0] && res.data[0].hastenType ? (res.data[0].hastenType === 'MANUAL' ? res.data[1] : res.data[0]) : {},
              finalWithdrawHasten: res.data[0] && res.data[0].hastenType ? (res.data[0].hastenType === 'MANUAL' ? res.data[0] : res.data[1]) : {},
            })
          }
        })
        .catch(err => {
          message.error(err.response.data.message)
        })
    }
  }

  // 手动通知 switch 切换
  handleChangeInformFlag = checked => {
    this.setState({ informFlag: checked });
  }

  // 展开通知方式模态框
  handleSpreadModal = (e, values) => {
    if (e) e.preventDefault();
    this.setState({
      addNoticeActionVisible: true,
      selectedValue: {
        selectedType: values && Array.isArray(values.noticeType) ? values.noticeType : ['1002'],
        selectedCheckedValues: values && Array.isArray(values.noticeActions) ? values.noticeActions : [],
        selectedIndex: values && values.keyInArr,
      }
    })
  }

  // 关闭通知方式模态框
  handleCancelActionModal = () => {
    this.setState({
      addNoticeActionVisible: false,
      selectedValue: {},
    })
  }

  /**
   * @param values: 回调获取勾选的通知方式，动作数据
   */
  addNoticeActionHandle = (values, type) => {
    const { finalActionsList, selectedValue } = this.state;
    // 判断， 如果selectedValue存在值，表示编辑，否则新增
    const flag = selectedValue.selectedCheckedValues && selectedValue.selectedCheckedValues.length;

    if (flag) {
      const { selectedIndex } = selectedValue;
      finalActionsList[selectedIndex] = {
        noticeType: type,
        noticeActions: values,
      }
    } else {
      finalActionsList.push({
        noticeType: type,
        noticeActions: values,
      })
    }

    this.setState({
      finalActionsList,
      addNoticeActionVisible: false,
    })
  }

  /**
   * 删除通知动作
   * @param index: 当前处于finalActionList中的下标
   */
  deleteNoticeAction = index => {
    if (!!index || String(index) === '0') {
      const { finalActionsList } = this.state;
      finalActionsList.splice(index, 1);
      this.setState({ finalActionsList });
    }
  }

  // 处理finalActionList 数组中下标index对应的数据
  handleDealList = (index, values, valueStr, operation) => {
    if (index < 0 || !operation) return;
    const { finalActionsList } = this.state;
    const newValueStr = valueStr;
    const newValueStr2 = valueStr.indexOf('Template') !== -1 ? `${valueStr}Id` : '';

    if (operation === 'change') {
      finalActionsList[index][newValueStr] = newValueStr2 ? values[newValueStr] : values;
      if (newValueStr2) {
        finalActionsList[index][newValueStr2] = values[newValueStr2]
      }
    } else if (operation === 'delete') {
      delete finalActionsList[index][newValueStr];
      if (newValueStr2) {
        delete finalActionsList[index][newValueStr2];
      }
    } else if (operation === 'add') {
      finalActionsList[index] = { ...finalActionsList[index], [valueStr]: values };
    } else if (operation === 'twiceChange') {
      // twiceChange 仅限于同时改变notifyFlag和identityList
      // 这里是判断当修改了通知人方式的radio时，清除数组中其对应值
      finalActionsList[index][newValueStr] = values;
      finalActionsList[index].identityList = [];
    }

    this.setState({ finalActionsList });
  }

  // save-btn
  handleTempStorage = () => {
    const { finalActionsList, informFlag, nodeTypeFlag, finalSystemHasten, finalWithdrawHasten } = this.state;
    const { onSave, params, processSettingId = '' } = this.props;
    const oldSettings = !!params.settings ? params.settings : {};

    /**
     * 催办信息--判断从父级传过来的和当前查询的版本是否一致，如果一致使用父级的，否则使用当前查询的
     * 从而避免先操作催办信息，不保存再执行通知时发生的催办信息覆盖问题
     */
    const tempSystemHasten = (finalSystemHasten && finalSystemHasten.versionNumber)
      ? (
        (
          params.systemHasten
          && (params.systemHasten.versionNumber === finalSystemHasten.versionNumber)
        )
          ? params.systemHasten
          : finalSystemHasten
      )
      : (
        {
          hastenType: "SYSTEM",
          processSettingId,
        }
      );
    const tempManualHasten = (finalWithdrawHasten && finalWithdrawHasten.versionNumber)
      ? (
        (
          params.withdrawHasten
          && (params.withdrawHasten.versionNumber === finalWithdrawHasten.versionNumber)
        )
          ? params.withdrawHasten
          : finalWithdrawHasten
      )
      : (
        {
          hastenType: "MANUAL",
          processSettingId,
        }
      );

    const tempObj = {
      ...params,
      settings: { ...oldSettings, informFlag },
      systemHasten: tempSystemHasten,
      manualHasten: tempManualHasten,
      notifications: finalActionsList,
    }

    if (onSave) {
      // 保存的时候需要将数据处理
      if (nodeTypeFlag) tempObj.settings = {}; // 开始或结束节点不存在settings
      onSave(tempObj)
    }
  }

  // cancel- btn
  handleCancel = () => {
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel();
    }
  }

  render() {
    const {
      informFlag,
      addNoticeActionVisible,
      finalActionsList,
      selectedValue,
      nodeTypeFlag,
    } = this.state;
    const { processTypeId } = this.props;

    return (
      <>
        {
          nodeTypeFlag ? (<></>) : (
            <Row>
              <span>审批人手动通知</span>
              <Switch
                checked={informFlag}
                onChange={this.handleChangeInformFlag}
                style={{ margin: '0 10px' }}
              />
              <span>{informFlag ? '启用' : '禁用'}</span>
            </Row>
          )
        }
        {
          (
            <div>
              {// 当节点类型为开始或结束时，只添加一条通知数据
                (nodeTypeFlag && !!finalActionsList.length) ? (null) : (
                  <Row style={{ margin: '10px 0' }}>
                    <a onClick={this.handleSpreadModal}>
                      <Icon type="plus" style={{ marginRight: '4px' }} />添加通知
                    </a>
                  </Row>
                )
              }
              {
                !!finalActionsList.length && (
                  finalActionsList.map((action, index) => {
                    const dataIndex = `通知${index}`;
                    return (
                      <SingleNoticeCard
                        type={action.noticeType}
                        noticeMsg={action.noticeActions}
                        key={dataIndex}
                        keyInArr={index}
                        defaultActionList={defaultActionList}
                        handleSpreadModal={this.handleSpreadModal}
                        handleDel={index => { this.deleteNoticeAction(index) }}
                        handleDealList={this.handleDealList}
                        finalActionsList={finalActionsList}
                        processTypeId={processTypeId}
                        // processTypeId='1186469917988843522'
                        ref={ref => { this.notice = ref }}
                        nodeTypeFlag={nodeTypeFlag}
                      />
                    )
                  })
                )
              }
            </div>
          )
        }
        {/* 底部按钮 */}
        <div className="slide-footer">
          <Button className="btn" type="primary" onClick={this.handleTempStorage}>
            {this.$t('common.ok')}
          </Button>
          <Button className="btn" onClick={this.handleCancel}>
            {this.$t('common.cancel')}
          </Button>
        </div>
        {/* 通知动作模态框 */}
        <AddNoticeAction
          onCancel={this.handleCancelActionModal}
          visible={addNoticeActionVisible}
          onOk={this.addNoticeActionHandle}
          checkedValues={selectedValue.selectedCheckedValues || []}
          loading={false}
          type={selectedValue.selectedType || null}
          actions={defaultActionList}
          nodeTypeFlag={nodeTypeFlag}
        />
      </>
    )
  }
}

export default NoticeSetting;
