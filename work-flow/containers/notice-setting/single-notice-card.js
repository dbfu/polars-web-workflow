import React, { Component } from 'react';
import { Card, Divider, Row, Icon, Radio, message, Modal, Tag, Badge, Popover, Empty, Col } from 'antd';
import ListSelector from 'widget/list-selector'
import MsgTemplate from '../../components/msg-template';
import AssessingOfficer from '../approval-node/assessing-officer';
import RuleListSelector from '../../components/rule-list-selector';
import { symbolText } from '../../components/range-type';

const { CheckableTag } = Tag;

const tagsFromServer = {
  '1001': '当前节点未审批人',
  '1002': '当前节点已审批人',
  '1003': '历史节点未审批人',
};

class SingleNoticeCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifyFlag: 1001, // 普通通知人 1001 , 审批通知人 1002
      approvalVisible: false, // 审批通知人弹框可见
      addConditionVisible: false, // 添加条件弹框
      selectedTags: [], // 审批人弹框标签选中
      curAddConditionList: [], // 添加条件数组
      msgVisible: false, // 通知模板弹框可见
      templateMsg: {}, // 勾选得通知模板数据
      msgDetailVisible: false, // 通知模板详情可见
      approvalNoticeObj: {}, // 用于关联审批通知人类型和其对应条件
      tempApprovalNoticeObj: {}, // approvalNoticeObj的副本，用于弹框中change类型
      curApprovalNoticeType: '', // 当前审批通知人类型（用于确定添加条件至什么类型的通知人）
    }
  }

  componentWillMount() {
    const { handleDealList, keyInArr, finalActionsList } = this.props;
    if (handleDealList && !(finalActionsList[keyInArr].notifyFlag)) {
      // 当父组件中的finalList中不存在notifyFlag时初始化通知人类型
      handleDealList(keyInArr, 1001, 'notifyFlag', 'change');
    }
  }

  componentDidMount() {
    const { finalActionsList, keyInArr } = this.props;
    this.initValue(finalActionsList,keyInArr)
  }

  componentWillReceiveProps(nextProps) {
    const { finalActionsList, keyInArr } = nextProps;
    this.initValue(finalActionsList,keyInArr)
  }

  initValue = (finalActionsList, keyInArr) => {
    const templateMsg = {};
    if (finalActionsList[keyInArr].messageTemplate) {
      templateMsg.messageTemplate = finalActionsList[keyInArr].messageTemplate;
      templateMsg.messageTemplateId = finalActionsList[keyInArr].messageTemplateId;
    } else if (finalActionsList[keyInArr].emailTemplate) {
      templateMsg.emailTemplate = finalActionsList[keyInArr].emailTemplate;
      templateMsg.emailTemplateId = finalActionsList[keyInArr].emailTemplateId;
    }

    const selectedTags = [];
    const approvalNoticeObj = {};
    // 审批通知人的数据格式处理
    if (String(finalActionsList[keyInArr].notifyFlag) === "1002") {
      finalActionsList[keyInArr].identityList.forEach(item => {
        if(
          item.workflowIdentities
          && item.workflowIdentities.length >= 0
        ) {
          const type = item.workflowIdentities[0].keyCode;
          selectedTags.push(type);
          approvalNoticeObj[type] = item.approvalConditionsList;
        }
      });
    }

    this.setState({
      templateMsg,
      notifyFlag: finalActionsList[keyInArr].notifyFlag || 1001,
      selectedTags,
      approvalNoticeObj,
    })
  }

  // 渲染card title
  renderCardTitle = () => {
    // type: (pc,app通知,邮件通知) noticeMsg(通知动作)
    const { type, noticeMsg, defaultActionList } = this.props;
    const noticeTitle = type && type.includes(1002) && type.includes(1003)
      ? 'pc、app通知：'
      : ((type || []).includes(1002)
        ? 'pc通知：'
        : ((type || []).includes(1003)
          ? 'app通知：'
          : '邮件通知：'));
    const actionTitle = (noticeMsg || []).map(msg => defaultActionList[msg]).join('、');
    return `${noticeTitle} ${actionTitle}`
  }

  // 编辑
  handleEdit = e => {
    e.preventDefault();
    const { handleSpreadModal, type, noticeMsg, keyInArr } = this.props;

    if (handleSpreadModal) {
      handleSpreadModal(undefined, { noticeType: type, noticeActions: noticeMsg, keyInArr });
    }
  }

  // 删除
  handleDelete = e => {
    e.preventDefault();
    const { handleDel, keyInArr } = this.props;
    if (handleDel) {
      handleDel(keyInArr)
    }
  }

  // 添加通知模板-展开
  handleSpreadModal = flag => {
    this.setState({ msgVisible: !!flag });
  }

  /**
   * @param values: 回调获取弹窗中单选的模板数据 [{id,..}] length = 1
   */
  okHandleMsgTemplate = values => {
    const { type } = this.props;

    if (values.result.length) {
      let tmpObj = {};
      let tempStr = '';
      if (type.includes(1001)) {
        tmpObj = {
          emailTemplate: values.result[0],
          emailTemplateId: values.result[0].id,
        }
        tempStr = 'emailTemplate';
      } else {
        tmpObj = {
          messageTemplate: values.result[0],
          messageTemplateId: values.result[0].id,
        }
        tempStr = 'messageTemplate'
      }
      this.setState({
        templateMsg: tmpObj,
      }, () => {
        const { handleDealList, keyInArr } = this.props;
        // 这里将组件内部设置的模板信息传至父组件
        if (handleDealList) handleDealList(keyInArr, tmpObj, tempStr, 'change');
        this.handleSpreadModal();
      })
    } else {
      message.error("请至少选择一个模版！");
    }
  }

  // 单选通知人
  handleChangeNotifyFlag = e => {
    this.setState({
      notifyFlag: e.target.value,
      approvalNoticeObj: {},
      selectedTags: [],
      tempApprovalNoticeObj: {}, // 改变通知人方式时，清空上一次方式对应数据
    }, () => {
      const { handleDealList, keyInArr } = this.props;
      if (handleDealList) {
        handleDealList(keyInArr, e.target.value, 'notifyFlag', 'twiceChange');
      }
    });
  }

  // 点击查看通知模板详情
  handleMsgDetail = e => {
    e.preventDefault();
    this.setState({
      msgDetailVisible: true,
    })
  }

  // 删除通知模板
  handleDeleteMsgDetail = e => {
    e.preventDefault();
    const { handleDealList, keyInArr, type } = this.props;
    if (handleDealList) {
      let tempStr = 'messageTemplate'
      if (type.includes(1001)) {
        tempStr = 'emailTemplate';
      }
      handleDealList(keyInArr, undefined, tempStr, 'delete');
    }
  }

  // 审批通知人
  handleApprovalNotice = () => {
    this.setState({ approvalVisible: true })
  }

  // 选择标签
  handleChangeApproval = (tag, checked) => {
    const { selectedTags, approvalNoticeObj } = this.state;
    const nextSelectedTags = checked ? [...selectedTags, tag] : selectedTags.filter(t => t !== tag);
    const tempApprovalNoticeObj = {};
    nextSelectedTags.forEach(item => {
      tempApprovalNoticeObj[item] = approvalNoticeObj[item] || [];
    })

    this.setState({ selectedTags: nextSelectedTags, tempApprovalNoticeObj });
  }

  // 选择通知人弹框关闭
  handleCancelApproval = () => {
    this.setState({
      approvalVisible: false,
    });
  }

  // 选择通知弹框确定
  handleOkApproval = () => {
    const { tempApprovalNoticeObj } = this.state;
    this.setState({
      approvalVisible: false,
      approvalNoticeObj: tempApprovalNoticeObj,
    }, this.returnFinalData)
  }

  /**
   * 删除审批通知人
   * @param type: 当前删除的审批通知人类型
   */
  conditionDelete = (e, type) => {
    e.preventDefault();
    const { approvalNoticeObj } = this.state;
    delete approvalNoticeObj[type];
    this.setState({ approvalNoticeObj }, this.returnFinalData);
  }

  /**
   * 添加条件,同时赋予弹框初始值
   * @param type: 需要删除的条件所对应的审批通知人类型
   */
  addCondition = (e, type) => {
    e.preventDefault();
    const { approvalNoticeObj } = this.state;

    this.setState({
      addConditionVisible: true,
      curApprovalNoticeType: type,
      curAddConditionList: approvalNoticeObj[type] || [],
    })
  }

  /**
   * 删除已添加的通知条件
   * @param id: 当前需要删除的条件id
   * @param type: 需要删除的条件所对应的审批通知人类型
   */
  handleDelConditions = (id, type) => {
    const { approvalNoticeObj } = this.state;
    const index = approvalNoticeObj[type].findIndex(rule => rule.id === id);
    approvalNoticeObj[type].splice(index, 1);
    this.setState({ approvalNoticeObj }, this.returnFinalData);
  }

  // 关闭添加条件模态框
  handleCancelAddCondition = () => {
    this.setState({ addConditionVisible: false });
  }

  // 回调获取审批条件模态框数据
  handleAddCondition = values => {
    const { approvalNoticeObj, curApprovalNoticeType } = this.state;
    approvalNoticeObj[curApprovalNoticeType] =
      values.result.map(item => (
        {id: item.id, name: item.conditionSetName, conditionInfo: item.conditionInfo, code: item.conditionSetCode}
      ));
    this.setState({ approvalNoticeObj, addConditionVisible: false }, this.returnFinalData);
  }

  // 抛出审批通知人添加的条件数据
  returnFinalData = () => {
    const { approvalNoticeObj } = this.state;
    const tempFinal =
      Object.keys(approvalNoticeObj).map(key => ({
        interfaceId: '-1',
        type: 'notification',
        workflowIdentities: [{
          keyCode: key,
          rangeType: 'notification',
          value: [],
        }],
        // interfaceType: key,
        approvalConditionsList: approvalNoticeObj[key],
        approvalConditions: (approvalNoticeObj[key] || []).map(item => item.id),
      }));
    const { handleDealList, keyInArr } = this.props;

    if (handleDealList) {
      handleDealList(keyInArr, tempFinal, 'identityList', 'add')
    }
  }

  // 抛出普通通知人添加的条件数据
  returnValue = rulesList => {
    if (rulesList.length >= 0) {
      const tempFinal = rulesList.map(item => ({
        interfaceId: item.interfaceId,
        id: item.id,
        workflowIdentities: item.workflowIdentities,
        approvalConditions: item.approvalConditions || [],
        approvalConditionsList: item.approvalConditionsList || [],
        approvalRuleName: item.approvalRuleName,
        type: 'condition',
      }))
      const { handleDealList, keyInArr } = this.props;
      if (handleDealList) {
        handleDealList(keyInArr, tempFinal, 'identityList', 'add')
      }
    }
  }

  /**
   * 渲染已选条件 tag 上的 popover
   */
  renderPop = (infoList) => {
    return (infoList && typeof infoList === 'string' && Array.isArray(JSON.parse(infoList))) ? (
      <div className="ant-popover-box" style={{overflow: 'scroll', maxHeight: '280px', minHeight: '100px'}}>
        {
          JSON.parse(infoList).map((info,index) => {
            const dataIndex = `ruleInfoPop${index}`
            return (
              <Row key={dataIndex} style={{width: '360px'}}>
                <Col
                  span={4}
                  className="ant-form-item-required"
                  style={{textAlign: 'right', padding: '0 5px'}}
                >
                  {`${info.paramName} :`}
                </Col>
                {
                  info.paramType !== 'TEXT' ? (
                    <Col span={18}>{`${info.numberData1} ${symbolText[info.symbol1]} 金额 ${symbolText[info.symbol2]} ${info.numberData2}`}</Col>
                  ) : (
                    <>
                      <Col span={4}>{info.contains === 'include' ? '包含' : '排除'}</Col>
                      {
                        info.limit
                          ? (<Col span={16}>{`${info.limit} ~ ${info.upper}`}</Col>)
                          : (
                            <Col span={16}>
                              {this.renderTag(info.lovParams)}
                            </Col>
                          )
                      }
                    </>
                    )
                }
              </Row>
            )
          })
        }
      </div>
    ) : (
      <Empty />
    )
  }

  renderTag = lists => {
    const list = typeof lists === 'string' ? JSON.parse(lists) : lists;
    if(list) {
      if(Array.isArray(list)) {
        return list.map((param,index) => {
          const dataIndex = `paramName${index}`
          return (<Tag key={dataIndex}>{param.name}</Tag>)
        })
      } else if(
        list.selectedRows
        && Array.isArray(list.selectedRows)
      ) {
        return list.selectedRows.map((param,index) => {
          const dataIndex = `paramName${index}`
          return (<Tag key={dataIndex}>{param.name}</Tag>)
        })
      } else return null
    } else return null
  }

  render() {
    const {
      notifyFlag,
      msgVisible,
      templateMsg,
      msgDetailVisible,
      approvalVisible,
      selectedTags,
      addConditionVisible,
      curAddConditionList,
      approvalNoticeObj,
    } = this.state;
    const { type, processTypeId, keyInArr, finalActionsList } = this.props;

    return (
      <>
        <Card
          style={{ marginTop: 16 }}
          type="inner"
          title={this.renderCardTitle()}
          extra={(
            <span>
              <a onClick={this.handleEdit}>编辑</a>
              <Divider type="vertical" />
              <a onClick={this.handleDelete}>删除</a>
            </span>
          )}
        >
          <Row style={{ margin: '10px 0', borderBottom: '1px solid #e8e8e8' }}>
            {
              JSON.stringify(templateMsg) !== '{}' ? (
                <span style={{ fontSize: '16px', fontWeight: '700' }}>
                  {type.includes(1001) ? '邮件模板:' : '通知模板:'}
                  {(templateMsg.messageTemplate || templateMsg.emailTemplate || {}).pushMethodName}
                  <span style={{ float: 'right', fontWeight: 'normal' }}>
                    <a onClick={this.handleMsgDetail}>详情</a>
                    <Divider type="vertical" />
                    <a onClick={this.handleDeleteMsgDetail}>删除</a>
                  </span>
                </span>
              ) :
                (
                  <a onClick={() => { this.handleSpreadModal(true) }}>
                    <Icon type="plus" style={{ marginRight: '4px' }} />
                    添加{type.includes(1001) ? '邮件' : '通知'}模板
                  </a>
                )
            }
          </Row>
          <Row>
            <Radio.Group onChange={this.handleChangeNotifyFlag} value={notifyFlag}>
              <Radio value={1001}>普通通知人</Radio>
              <Radio value={1002}>审批通知人</Radio>
            </Radio.Group>
          </Row>
          <Row style={{ margin: '10px 0' }}>
            { /**
             * 由于assessing-officer组件中存在类似的超链接，所以当noticeFlag 为1001时
             * 隐藏这个超链接改用组件内部的
             */
              notifyFlag === 1002 ? (
                <a onClick={this.handleApprovalNotice}>
                  <Icon type="plus" style={{ marginRight: '4px' }} />添加通知人
                </a>
              ) :
                (
                  // 普通通知人
                  <AssessingOfficer
                    notifyFlag={notifyFlag}
                    processTypeId={processTypeId}
                    ref={ref => { this.assess = ref }}
                    type="notice"
                    returnValue={this.returnValue}
                    rulesList={
                      String(notifyFlag) === '1001'
                      && (finalActionsList[keyInArr].identityList || [])
                    }
                  />
              )
            }
          </Row>
          {/* 审批通知人 */}
          <div>
            {
              Object.keys(approvalNoticeObj).map((item, index) => {
                const dataIndex = `row${index}`;
                return (
                  <Row style={{ borderBottom: '1px solid #e8e8e8', margin: '10px 0' }} key={dataIndex}>
                    <Badge status="default" text={tagsFromServer[item]} />
                    {
                      <span
                        style={{
                          float: "right",
                          paddingTop: '10px',
                          display: 'inline',
                        }}
                      >
                        <a onClick={e => { this.addCondition(e, item) }}>添加条件</a>
                        <Divider type="vertical" />
                        <a onClick={(e) => this.conditionDelete(e, item)}>删除</a>
                      </span>
                    }
                    <div style={{ margin: '10px' }}>
                      {
                        Array.isArray(approvalNoticeObj[item])
                        && !!approvalNoticeObj[item].length
                        && approvalNoticeObj[item].map(rule => (
                          <Popover
                            title={`${rule.code}-${rule.name}`}
                            content={this.renderPop(rule.conditionInfo)}
                            getPopupContainer={node => node.parentNode}
                            overlayClassName="pop-card"
                            overlayStyle={{width: '400px'}}
                            key={rule.id}
                          >
                            <Tag key={rule.id} closable onClose={() => { this.handleDelConditions(rule.id, item) }}>
                              {rule.name}
                            </Tag>
                          </Popover>
                        ))
                      }
                    </div>
                  </Row>
                )
              })
            }
          </div>
        </Card>

        {/* 弹窗，勾选通知模板数据 */}
        <ListSelector
          onOk={this.okHandleMsgTemplate}
          onCancel={() => { this.handleSpreadModal(false) }}
          visible={msgVisible}
          valueKey="id"
          labelKey="pushMethodName"
          type={type.includes(1001) ? "email_template" : "message_template"}
          extraParams={{ mouldType: type.includes(1001) ? 1001 : 1002 }}
          single
        />
        {/* 弹框，展示勾选的模板详情 */}
        <MsgTemplate
          visible={msgDetailVisible}
          params={templateMsg.messageTemplate || templateMsg.emailTemplate || {}}
          onCancel={() => { this.setState({ msgDetailVisible: false }) }}
          onOk={() => { this.setState({ msgDetailVisible: false }) }}
        />
        {/* 审批通知人 */}
        <Modal
          title="选择通知人"
          visible={approvalVisible}
          onOk={this.handleOkApproval}
          onCancel={this.handleCancelApproval}
        >
          {Object.keys(tagsFromServer).map(tag => (
            <CheckableTag
              key={tag}
              checked={selectedTags.indexOf(tag) > -1}
              onChange={checked => this.handleChangeApproval(tag, checked)}
            >
              {tagsFromServer[tag]}
            </CheckableTag>
          ))}
        </Modal>
        {/* 添加条件 */}
        <RuleListSelector
          visible={addConditionVisible}
          type='wfl_approval_condition'
          labelKey="conditionSetName"
          valueKey="id"
          onCancel={this.handleCancelAddCondition}
          onOk={this.handleAddCondition}
          selectedData={curAddConditionList}
          extraParams={{ typeId: processTypeId }}
        // extraParams={{ typeId: "1186469917988843522"}}
        />
      </>
    )
  }

}

export default SingleNoticeCard;
