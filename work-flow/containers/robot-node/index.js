/** 机器人节点 */
import React, { Component } from 'react';
import { Form, Radio, Input, Badge, Col, Divider, Tag, Row, Button, message, Popover, Empty } from 'antd';
import RuleListSelector from '../../components/rule-list-selector'
import InputLanguage from 'components/Widget/Template/input-language'
import { symbolText } from '../../components/range-type';

const formItemLayout = {
  labelCol: { span: 5 },
  wrapperCol: { span: 16 },
}
const headerTitle = {
  marginBottom: '14px',
  borderBottom: '1px solid #e8e8e8',
  fontSize: '16px',
  padding: '10px 0',
  fontWeight: 700,
}

class RobotNodeFrame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      robotRules: [], // 机器人审批条件数组
      selectorView: false,
    }
  }

  componentDidMount() {
    const { params = {} } = this.props;
    if(JSON.stringify(params) !== '{}' && params.assignees && params.assignees[0]) {
      this.setState({
        robotRules: params.assignees[0].approvalConditionList,
      })
    }
  }

  componentWillUnmount() {
    this.setState = () => {
      return false;
    }
  }


  // 关闭侧拉
  handleCancel = () => {
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel();
    }
  }

  // 点击保存- 获取数据- 抛出
  handleTempStorage = () => {
    const { form: { validateFieldsAndScroll }, onSave, params = {} } = this.props;
    const { robotRules } = this.state;

    validateFieldsAndScroll((err, values) => {
      if (err) return;

      const approvalConditions = robotRules.map(rule => rule.id);

      values.remark = values.remark.value;
      values.i18n = {
        remark: values.remark.i18n,
      }
      const oldSetting = !!params.settings ? params.settings : {};

      if (onSave) {
        const tempObj = {
          settings:  { ...oldSetting, ...values },
          assignees: [{
            approvalConditionList: robotRules,
            approvalConditions,
          }],
        }
        onSave(tempObj); // 回调，将侧拉框数据抛出
      }
    })
  }

  // 删除已选条件
  handleCloseTag = id => {
    const { robotRules } = this.state;
    const index = robotRules.findIndex(rule => id === rule.id);

    if (index >= 0) {
      robotRules.splice(index, 1);
      this.setState({ robotRules })
    }
  }

  // 关闭审批条件模态框
  handleCancelSelector = () => {
    this.setState({ selectorView: false });
  }

  // 展开审批条件模态框
  handleOpenSelector = e => {
    e.preventDefault();
    this.setState({ selectorView: true });
  }

  // 回调获取审批条件模态框数据
  handleSelectConditions = values => {
    const robotRules = (values.result || []).map(item => (
      {id: item.id, name: item.conditionSetName, code: item.conditionSetCode, conditionInfo: item.conditionInfo}
    ))
    this.setState({ robotRules }, this.handleCancelSelector);
  }

  /**
   * 渲染已选条件 tag 上的 popover
   */
  renderPop = (infoList) => {
    return (infoList && typeof infoList === 'string' && Array.isArray(JSON.parse(infoList))) ? (
      <div className="ant-popover-box" style={{overflow: 'scroll', maxHeight: '280px', minHeight: '100px'}}>
        {
          JSON.parse(infoList).map((info,index) => {
            const dataIndex = `ruleInfoPop${index}`
            return (
              <Row key={dataIndex} style={{width: '360px'}}>
                <Col
                  span={4}
                  className="ant-form-item-required"
                  style={{textAlign: 'right', padding: '0 5px'}}
                >
                  {`${info.paramName} :`}
                </Col>
                {
                  info.paramType !== 'TEXT' ? (
                    <Col span={18}>{`${info.numberData1} ${symbolText[info.symbol1]} 金额 ${symbolText[info.symbol2]} ${info.numberData2}`}</Col>
                  ) : (
                    <>
                      <Col span={4}>{info.contains === 'include' ? '包含' : '排除'}</Col>
                      {
                        info.limit
                          ? (<Col span={16}>{`${info.limit} ~ ${info.upper}`}</Col>)
                          : (
                            <Col span={16}>
                              {this.renderTag(info.lovParams)}
                            </Col>
                          )
                      }
                    </>
                    )
                }
              </Row>
            )
          })
        }
      </div>
    ) : (
      <Empty />
    )
  }

  renderTag = lists => {
    const list = typeof lists === 'string' ? JSON.parse(lists) : lists;
    if(list) {
      if(Array.isArray(list)) {
        return list.map((param,index) => {
          const dataIndex = `paramName${index}`
          return (<Tag key={dataIndex}>{param.name}</Tag>)
        })
      } else if(
        list.selectedRows
        && Array.isArray(list.selectedRows)
      ) {
        return list.selectedRows.map((param,index) => {
          const dataIndex = `paramName${index}`
          return (<Tag key={dataIndex}>{param.name}</Tag>)
        })
      } else return null
    } else return null
  }

  render() {
    const { form: { getFieldDecorator }, params = {}, processTypeId = '' } = this.props;
    const { robotRules, selectorView } = this.state;
    const editFlag = JSON.stringify(params) !== '{}' && params;
    const editParam = editFlag ? params.settings : {};

    return (
      <div>
        {/* 基本信息 */}
        <div style={headerTitle}>{this.$t('common.baseInfo')}</div>
        <Form>
          <Form.Item {...formItemLayout} label={this.$t('setting.key1372'/*节点名称*/)}>
            {getFieldDecorator('remark', {
              initialValue: editFlag
                ? { value: editParam.remark, i18n: editParam.i18n ? editParam.i18n.remark : [] }
                : undefined,
            })(
              <InputLanguage placeholder={this.$t('common.please.enter')} />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label={this.$t('setting.key1373'/*节点类型*/)}>
            {getFieldDecorator('nodeType', {
              initialValue: editFlag ? editParam.nodeType : '8001',
            })(
              <Radio.Group>
                <Radio value="8001">
                  {this.$t('setting.key1374'/*通过*/)}
                  <span style={{ marginLeft: '10px' }}>
                    {this.$t('setting.key1375'/*符合审批条件则系统自动审批通过,否则自动跳过到下一个节点*/)}
                  </span>
                </Radio>
                <Radio value="8002">
                  {this.$t('setting.key1376'/*驳回*/)}
                  <span style={{ marginLeft: '10px' }}>
                    {this.$t('setting.key1377'/*符合审批条件则系统自动审批驳回,否则自动跳过到下一个节点*/)}
                  </span>
                </Radio>
              </Radio.Group>
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label={this.$t('setting.key1378'/*审批意见*/)}>
            {getFieldDecorator('comments', {
              rules: [{
                max: 50,
                message: this.$t('common.max.characters.length', { max: 50 })
              }],
              initialValue: editFlag ? editParam.comments : undefined,
            })(
              <Input.TextArea rows={2} placeholder={this.$t('common.please.enter')} />
            )}
          </Form.Item>
        </Form>
        {/* 审批人员 */}
        <div style={{...headerTitle, marginTop: '16px'}}>审批人员</div>
        <div style={{ padding: '10px' }}>
          <Row>
            <Col span={6}><Badge status="default" text={this.$t('setting.key1249')} /></Col>
            <Col span={18} style={{ textAlign: 'right' }}>
              <a onClick={e => { this.handleOpenSelector(e) }}>添加条件</a>
              <Divider type="vertical" />
              <a>{this.$t('common.delete')}</a>
            </Col>
          </Row>
          {
            (Array.isArray(robotRules) && !!robotRules.length)
            && (
              <Row style={{ padding: '10px 16px' }}>
                <span style={{ marginRight: '10px' }}>{`${this.$t('setting.key1330')} :`}</span>
                {
                  robotRules.map(rule => (
                    <Popover
                      title={`${rule.code}-${rule.name}`}
                      content={this.renderPop(rule.conditionInfo)}
                      getPopupContainer={node => node.parentNode}
                      overlayClassName="pop-card"
                      overlayStyle={{width: '400px'}}
                      key={rule.id}
                    >
                      <Tag key={rule.id} closable onClose={() => { this.handleCloseTag(rule.id) }}>
                        {rule.name}
                      </Tag>
                    </Popover>
                  ))
                }
              </Row>
            )
          }
        </div>
        {/* 底部按钮 */}
        <div className="slide-footer">
          <Button className="btn" type="primary" onClick={this.handleTempStorage}>
            {this.$t('common.ok')}
          </Button>
          <Button className="btn" onClick={this.handleCancel}>
            {this.$t('common.cancel')}
          </Button>
        </div>
        {/* 机器人审批条件 */}
        <RuleListSelector
          visible={selectorView}
          type='wfl_approval_condition'
          labelKey="conditionSetName"
          valueKey="id"
          onCancel={this.handleCancelSelector}
          onOk={this.handleSelectConditions}
          selectedData={robotRules}
          extraParams={{ typeId: processTypeId }}
        />
      </div>
    )
  }
}

export default Form.create()(RobotNodeFrame);
