import React, { Component } from 'react';
import { Form, Divider, Button, Select, Input, Switch, InputNumber, message } from 'antd';
import service from '../../service';

class NewWorkflowLineValue extends Component {
  constructor(props) {
    super(props);
    this.state = {
      saveLoading: false,
      documentType: [], // 工作流类型
    }
  }

  componentDidMount() {
    const { workflowTypeList } = this.props;
    if(workflowTypeList && Array.isArray(workflowTypeList)) {
      this.setState({ documentType: [...workflowTypeList] });
    } else {
      this.getWorkflowTypeList()
    }
  }

  getWorkflowTypeList = () => {
    service.getWorkflowTypeLineData()
      .then(({ data }) => {
        const workflowTypeList = data.map(item => ({
          label: item.typeName,
          value: item.id,
        }))
        this.setState({ documentType: [ ...workflowTypeList ] });
      })
      .catch(() => { });
  }

  // 关闭
  handleCancel = () => {
    const { onClose } = this.props;
    if (onClose) {
      onClose();
    }
  }

  handleSave = () => {
    const { form: { validateFieldsAndScroll }, params = {} } = this.props;
    validateFieldsAndScroll((err, values) => {
      if (err) return;
      this.setState({ saveLoading: true });
      let handleMethod = null;
      if (params.id) {
        handleMethod = service.editWorkflowLineData;
      } else {
        handleMethod = service.saveWorkflowLineData;
      }

      handleMethod({ ...params, ...values })
        .then((res) => {
          message.success(`${this.$t('common.operate.success')}`);
          this.setState({ saveLoading: false }, () => {
            if(params.id) {
              const { onClose } = this.props;
              onClose(true);
            } else {
              this.pushPageToDetail(res.data.id);
            }
          });
        })
        .catch(error => {
          message.error(error.response.data.message);
          this.setState({ saveLoading: false });
        })
    })
  }

  // 跳转详情
  pushPageToDetail = id => {
    this.$pushPage({
      functionCode: 'workflow_definition',
      pageCode: 'workflow_definition_detail',
      params: [id,'0'],
    })
  }

  render() {
    const { saveLoading, documentType } = this.state;
    const { form: { getFieldDecorator, getFieldValue }, params = {} } = this.props;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 10 },
    };

    return (
      <>
        <Form>
          <div>
            <span style={{ fontSize: '16px', marginLeft: '16px' }}>{this.$t('common.baseInfo')}</span>
            <Divider style={{ margin: '15px -15px' }} />
          </div>
          <Form.Item label={this.$t("workflow.workflow.type")} {...formItemLayout}>
            {
              getFieldDecorator('processTypeId', {
                rules: [{ required: true, message: this.$t('common.please.select') }],
                initialValue: params.id ? params.processTypeId : undefined,
              })(
                <Select
                  placeholder={this.$t('common.please.select')}
                  allowClear
                  getPopupContainer={node => node.parentNode}
                  disabled={!!params.id}
                  showSearch
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {documentType.map(type => (
                    <Select.Option value={type.value} key={type.value}>{type.label}</Select.Option>
                  ))}
                </Select>
              )
            }
          </Form.Item>
          <Form.Item label="工作流代码" {...formItemLayout}>
            {
              getFieldDecorator('processCode', {
                rules: [
                  { required: true, message: this.$t('common.please.enter') },
                  {
                    pattern: /^[0-9a-zA-Z_]{1,}$/,
                    message: '只能输入英文数字下划线!',
                  },
                ],
                initialValue: params.id ? params.processCode : undefined,
              })(<Input placeholder={this.$t('common.please.enter')} disabled={!!params.id} />)
            }
          </Form.Item>
          <Form.Item label="工作流名称" {...formItemLayout}>
            {
              getFieldDecorator('processName', {
                rules: [{ required: true, message: this.$t('common.please.enter') }],
                initialValue: params.id ? params.processName : undefined,
              })(<Input placeholder={this.$t('common.please.enter')} />)
            }
          </Form.Item>
          <Form.Item label="工作流版本" {...formItemLayout}>
            {
              getFieldDecorator('contentVersion', {
                rules: [{ required: true, message: this.$t('common.please.enter') }],
                initialValue: params.id ? params.contentVersion : 1,
              })(
                <InputNumber
                  placeholder={this.$t('common.please.enter')}
                  disabled
                  style={{ width: '100%' }}
                  formatter={value => `V${Number(value).toFixed(1)}`}
                  parser={value => Number(value.replace('V', '').trim())}
                />)
            }
          </Form.Item>
          <Form.Item label="允许撤回" {...formItemLayout}>
            {
              getFieldDecorator('withdrawFlag', {
                rules: [{ required: true, message: this.$t('common.please.enter') }],
                valuePropName: 'checked',
                initialValue: params.id ? params.withdrawFlag : true,
              })(<Switch />)
            }
            <span style={{ marginLeft: '6px' }}>
              {getFieldValue('withdrawFlag') ? this.$t('common.enabled') : this.$t('common.disabled')}
            </span>
          </Form.Item>
          {
            getFieldValue('withdrawFlag') && (
              <Form.Item label="撤回模式" {...formItemLayout}>
                {
                  getFieldDecorator('withdrawRule', {
                    rules: [{ required: true, message: this.$t('common.please.enter') }],
                    initialValue: params.id ? params.withdrawRule : undefined,
                  })(
                    <Select
                      placeholder={this.$t('common.please.select')}
                      allowClear
                      getPopupContainer={node => node.parentNode}
                    >
                      <Select.Option value={1001}>无审批记录时可撤回</Select.Option>
                      <Select.Option value={1002}>审批流未结束均可撤回</Select.Option>
                    </Select>
                  )
                }
              </Form.Item>
            )
          }
          <Form.Item label={this.$t('base.state')} {...formItemLayout}>
            {
              getFieldDecorator('enabled', {
                valuePropName: 'checked',
                initialValue: params.id ? params.enabled : true,
              })(<Switch />)
            }
            <span style={{ marginLeft: '6px' }}>
              {getFieldValue('enabled') ? this.$t('common.enabled') : this.$t('common.disabled')}
            </span>
          </Form.Item>
          <Form.Item label={this.$t('common.comment')} {...formItemLayout}>
            {
              getFieldDecorator('remark', {
                initialValue: params.id ? params.remark : undefined,
              })(
                <Input.TextArea
                  placeholder={this.$t('common.please.enter')}
                />)
            }
          </Form.Item>
        </Form>
        <div className="slide-footer">
          <Button className="btn" type="primary" loading={saveLoading} onClick={this.handleSave}>
            {params.id ? this.$t('common.save') : this.$t('acp.next' /** 下一步 */)}
          </Button>
          <Button className="btn" onClick={this.handleCancel}>
            {this.$t('common.cancel')}
          </Button>
        </div>
      </>
    )
  }
}

export default Form.create()(NewWorkflowLineValue);
