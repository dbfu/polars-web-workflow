import React, { Component } from 'react';
import ContentHeader from "widget/content-header"
import Content from 'widget/content'
import ContentScroller from 'widget/content-scroller'
import SearchArea from 'widget/search-area';
import { Button, Badge, Divider, Modal, Popconfirm, Popover, message } from 'antd';
import CustomTable from 'widget/custom-table';
import moment from 'moment';
import config from 'config';
import SlideFrame from 'widget/slide-frame';
import NewWorkflowLineValue from './new-workflow-line';
import service from '../../service';

class WorkflowDefinition extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newWorkflowModalView: false,
      searchFormItems: [
        {
          label: this.$t('工作流类型'),
          id: 'processType',
          placeholder: this.$t('common.please.select'),
          type: 'select',
          options: [],
        },
        {
          label: '审批流代码/名称',
          id: 'codeOrName',
          placeholder: this.$t('common.please.enter'),
          type: 'input',
        },
        {
          label: this.$t({ id: 'common.remark' }), /* 备注 */
          id: 'remark',
          placeholder: this.$t('common.please.enter'),
          type: 'input',
        },
        {
          label: '状态',
          id: 'enabled',
          type: 'select',
          options: [
            { value: true, label: "启用" },
            { value: false, label: "禁用" },
          ],
        },
      ],
      columns: [
        {
          title: '工作流类型',
          dataIndex: 'processTypeName',
          tooltips: true,
          align: 'center',
          width: '120px',
        },
        {
          title: '审批流代码',
          dataIndex: 'processCode',
          tooltips: true,
          align: 'center',
          width: '150px',
        },
        {
          title: '审批流名称',
          dataIndex: 'processName',
          tooltips: true,
          align: 'center',
          width: '150px',
        },
        {
          title: '备注',
          dataIndex: 'remark',
          tooltips: true,
          align: 'center',
          width: '150px',
        },
        {
          title: '当前版本',
          dataIndex: 'contentVersion',
          align: 'center',
          width: '150px',
          render: (value, record) => {
            return (
              <Popover content={`V${value}`}>
                {`V${value}`}
              </Popover>
            )
          },
        },
        {
          title: this.$t('common.column.status' /* 状态 */),
          dataIndex: 'enabled',
          align: 'center',
          width: '100px',
          render: valid => (
            <Badge
              status={valid ? 'success' : 'error'}
              text={valid ? this.$t('common.status.enable') : this.$t('common.status.disable')}
            />
          ),
        },
        {
          title: '操作',
          dataIndex: 'operate',
          width: '200px',
          align: 'center',
          render: (desc, record) => (
            <span>
              <a onClick={e => this.handleJumpToDetail(e, record.id)}>详情</a>
              <Divider type="vertical" />
              {(
                !!this.state.sourceFormId
                && this.state.sourceFormId !== record.id
              ) ? (
                  <a onClick={e => this.showConfirmModal(e, record)}>{this.$t('common.paste')}</a>
                ) : (
                  <a onClick={e => this.handleCopy(e, record)}>{this.$t('common.copy')}</a>
                )}
            </span>
          ),
        },
        {
          title: '版本记录',
          dataIndex: 'versionRecord',
          align: 'center',
          width: '120px',
          render: (value, record) => (
            <a onClick={e => this.handleHistoryView(e, record)}>历史版本查看</a>
          ),
        }
      ],
      sourceFormId: '', // 当前被复制的审批流id
      formIdForHistoryVersion: '', // 当前查看历史版本的审批流id,同时控制模态框可见
      historyColumns: [
        {
          title: '版本号',
          dataIndex: 'contentVersion',
          align: 'center',
          render: (value, record) => (
            <Popover content={`V${value}`}>
              <a onClick={e => this.handleJumpToDetail(e, record.processSettingId,record.parentDefinitionId)}>{`V${value}`}</a>
            </Popover>
          ),
        },
        {
          title: '版本日期',
          dataIndex: 'lastUpdatedDate',
          align: 'center',
          render: value => (
            <Popover content={moment(value).format('YYYY-MM-DD HH:mm:ss')}>
              {moment(value).format('YYYY-MM-DD HH:mm:ss')}
            </Popover>
          ),
        },
        {
          title: '操作人',
          dataIndex: 'operator',
          align: 'center',
          tooltips: true,
        },
        {
          title: '操作',
          dataIndex: 'operation',
          align: 'center',
          render: (value, record) => (
            <Popconfirm
              title={(
                <div>
                  <p>确认后将更新工作流为此版本内容</p>
                  <p>你确定要应用此版本为当前版本吗？</p>
                </div>
              )}
              onConfirm={() => { this.handleApplyVersion(record) }}
            >
              <a>应用此版本</a>
            </Popconfirm>
          ),
        },
      ],
      workflowTypeList: [], // 工作流类型数组
    }
  }

  componentDidMount() {
    service.getWorkflowTypeLineData()
      .then(({ data }) => {
        const { searchFormItems } = this.state;

        const workflowTypeList = data.map(item => ({
          label: item.typeName,
          value: item.id,
        }))
        searchFormItems[0].options = [...workflowTypeList];
        this.setState({ searchFormItems, workflowTypeList });
      })
      .catch(() => { });
  }

  // btn-新建审批流-模态框展示
  handleNewWorkflow = () => {
    this.setState({ newWorkflowModalView: true });
  }

  // 搜索
  handleSearch = values => {
    this.table.search(values)
  }

  // 清空搜索
  handleClearSearch = () => {
    this.table.search({})
  }

  // 查看历史版本
  handleHistoryView = (e, record) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      formIdForHistoryVersion: record.id,
    })
  }

  // 关闭历史版本弹窗
  handleCancelHistoryModal = (flag) => {
    this.setState({ formIdForHistoryVersion: '' }, () => {
      if (flag) {
        this.table.search();
      }
    })
  }

  // 复制
  handleCopy = (e, record) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({ sourceFormId: record.id });
  };

  // 粘贴前的提示框
  showConfirmModal = (e, record) => {
    e.preventDefault();
    e.stopPropagation();
    Modal.confirm({
      title: this.$t('setting.key1426' /*是否确认更改*/), //是否确认更改
      content: this.$t('setting.key1427' /*粘贴后将覆盖原审批流*/), //粘贴后将覆盖原审批流
      onOk: () => this.handlePaste(record.id),
    });
  }

  /**
   * 确认粘贴
   * @param formId: 审批流id
   */
  handlePaste = formId => {
    const { sourceFormId } = this.state;
    const obj = {
      sourceId: sourceFormId,
      destinationId: formId,
    }
    service
      .copyWorkflowNodeValue(obj)
      .then(() => {
        message.success('粘贴成功');
        // 调用接口，完成后清除sourceFormId
        this.setState({ sourceFormId: '' }, () => {
          this.table.search({})
        }).catch(err => {
          message.error(err.response.data.message);
        })
      })
  }

  // 跳转详情
  handleJumpToDetail = (e, id,parentDefinitionId) => {
    e.stopPropagation();
    e.preventDefault();
    let params = [id,'0'];
    if(parentDefinitionId) {
      params = [id,parentDefinitionId]
    }
    this.$pushPage({
      functionCode: 'workflow_definition',
      pageCode: 'workflow_definition_detail',
      params,
    })
  }

  // 历史版本模态框-应用版本
  handleApplyVersion = (record) => {
    if (!record) return;
    const obj = {
      processSettingId: record.processSettingId,
      definitionId: record.id,
    }
    service
      .resetWorkflowLineVersion(obj)
      .then(() => {
        message.success('应用成功');
        this.handleCancelHistoryModal(true);
      })
      .catch(err => { message.error(err.response.data.message) })
  }

  // 关闭新建侧拉框
  handleCloseSlideFrame = flag => {
    this.setState({ newWorkflowModalView: false }, () => {
      if (!!flag) {
        this.table.search();
      }
    });
  }

  render() {
    const {
      searchFormItems,
      columns,
      formIdForHistoryVersion,
      historyColumns,
      newWorkflowModalView,
      workflowTypeList,
    } = this.state;

    return (
      <div className="workflow-definition">
        <ContentHeader title={this.$t('menu.workflow')} />
        <ContentScroller>
          <Content>
            <SearchArea
              searchForm={searchFormItems}
              submitHandle={this.handleSearch}
              clearHandle={this.handleClearSearch}
            />
            <div>
              <Button
                type="primary"
                onClick={this.handleNewWorkflow}
                style={{ marginBottom: '16px' }}
              >
                {this.$t("workflow.new.approval.process")}
              </Button>
            </div>
            <CustomTable
              ref={ref => { this.table = ref }}
              columns={columns}
              url={`${config.wflUrl}/api/wfl/process/setting`}
              scroll={{ x: 1000 }}
            />
          </Content>
        </ContentScroller>
        <Modal
          title="历史版本记录"
          footer={null}
          visible={!!formIdForHistoryVersion}
          onCancel={this.handleCancelHistoryModal}
          width={720}
        >
          <CustomTable
            columns={historyColumns}
            ref={ref => { this.table = ref }}
            url={`${config.wflUrl}/api/wfl/process/definition?processSettingId=${formIdForHistoryVersion}`}
          />
        </Modal>
        <SlideFrame
          title={this.$t("workflow.new.approval.process")}
          show={newWorkflowModalView}
          onClose={() => { this.handleCloseSlideFrame() }}
        >
          <NewWorkflowLineValue
            onClose={flag => { this.handleCloseSlideFrame(flag) }}
            workflowTypeList={workflowTypeList}
          />
        </SlideFrame>
      </div>
    )
  }
}

export default WorkflowDefinition;
