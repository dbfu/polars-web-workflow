
import React, { Component } from 'react';
import { SelectDepOrPerson } from 'widget/index';
import SelectEmployeeGroup from 'widget/Template/select-employee-group';
import PropTypes from 'prop-types';
import ListSelector from 'widget/list-selector';
import {
  WFL_IDENTITY_COMPANY,
  WFL_IDENTITY_USER_GROUP,
  WFL_IDENTITY_CONTACT,
  WFL_IDENTITY_DEPARTMENT,
} from '../../components/range-type';

class VisibleListSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSelectDepartment: false,
      showSelectEmployeeGroup: false,
      showSelectEmployee: false,
      showSelectCompany: false,
      selectedList: [],
    }
  }

  componentDidMount() {
    const { type, selectedData } = this.props;
    this.setVisibleValueByType(type, selectedData);
  }

  componentWillReceiveProps(nextProps) {
    const { type } = this.props;
    if (type !== nextProps.type) {
      this.setVisibleValueByType(nextProps.type, nextProps.selectedData);
    }
  }

  // 根据type设置响应组件visible
  setVisibleValueByType = (type, selectedData) => {
    // 部门传递过来的data是对象，而其他的data是id字符串
    const temp = selectedData.map(data => (
      {
        contactId: type === WFL_IDENTITY_USER_GROUP ? data : null,
        id: type === WFL_IDENTITY_DEPARTMENT ? data.id : data,
        name: type === WFL_IDENTITY_DEPARTMENT ? data.name : null,
        label: type === WFL_IDENTITY_DEPARTMENT ? data.name : null,
        value: type === WFL_IDENTITY_DEPARTMENT ? data.id : data,
        key: type === WFL_IDENTITY_DEPARTMENT ? data.id : data,
      }
    ));

    if (type) {
      switch (type) {
        case WFL_IDENTITY_COMPANY:
          this.setState({ showSelectCompany: true, selectedList: [...temp] });
          break;
        case WFL_IDENTITY_DEPARTMENT:
          this.setState({ showSelectDepartment: true, selectedList: [...temp] });
          break;
        case WFL_IDENTITY_USER_GROUP:
          this.setState({ showSelectEmployeeGroup: true, selectedList: [...temp] });
          break;
        case WFL_IDENTITY_CONTACT:
          this.setState({ showSelectEmployee: true, selectedList: [...temp] });
          break;
        default: break;
      }
    }
  }

  handleListCancel = () => {
    this.setState({
      showSelectEmployee: false,
      showSelectEmployeeGroup: false,
      showSelectDepartment: false,
      showSelectCompany: false,
    }, () => {
      const { onCancel } = this.props;
      onCancel();
    });
  };

  // 编辑审批人范围
  handleEditData = values => {
    const { onEdit, type } = this.props;

    const valueList = type === WFL_IDENTITY_DEPARTMENT
      ? values : (type === WFL_IDENTITY_USER_GROUP ? values.checkedKeys : values.result);

    const selectIds =  Array.isArray(valueList) ? valueList.map(item => item.id) : [];
    const selectObjs = Array.isArray(valueList) ? valueList.map(item => ({id: item.id, name: item.name || item.label})) : [];

    if (onEdit) {
      const temp = type !== WFL_IDENTITY_DEPARTMENT ? selectIds : selectObjs;
      onEdit(temp);
      this.handleListCancel();
    }
  }

  render() {
    const {
      showSelectDepartment,
      showSelectEmployeeGroup,
      showSelectEmployee,
      showSelectCompany,
      selectedList,
    } = this.state;
    const { mode } = this.props;
    return (
      <div>
        {showSelectEmployeeGroup && (
          <SelectEmployeeGroup
            visible={showSelectEmployeeGroup}
            onCancel={this.handleListCancel}
            single
            selectedData={selectedList}
            mode={mode}
            onOk={this.handleEditData}
          />
        )}

        <SelectDepOrPerson
          visible={showSelectDepartment}
          onCancel={this.handleListCancel}
          single
          onlyDep
          title="选择部门"
          renderButton={false}
          noFooter
          onConfirm={this.handleEditData}
          selectedData={selectedList}
          mode={mode}
        />
        <ListSelector
          visible={showSelectEmployee}
          onCancel={this.handleListCancel}
          type="bgtUser"
          extraParams={{ roleType: 'TENANT' }}
          selectedData={[...selectedList]}
          valueKey="id"
          showDetail={false}
          onOk={this.handleEditData}
        />
        <ListSelector
          visible={showSelectCompany}
          onCancel={this.handleListCancel}
          type="vendor_approve_company"
          selectedData={[...selectedList]}
          valueKey="id"
          showDetail={false}
          onOk={this.handleEditData}
        />
      </div>
    )
  }
}

VisibleListSelector.protoTypes = {
  type: PropTypes.string,
  selectedData: PropTypes.array,
  mode: PropTypes.string,
  onCancel: PropTypes.func,
}

VisibleListSelector.defaultProps = {
  type: 'all',
  selectedData: [],
  mode: 'id',
  onCancel: () => { },
}


export default VisibleListSelector;
