/** 自选节点 */
import React, { Component } from 'react';
import { Form, Divider, Button, Select, Row, Col, Radio, Icon, Badge, Tag, message, Popover, Empty } from 'antd';
import SystemHasten from '../approval-node/system-hasten'
import WithdrawHasten from '../approval-node/withdraw-hasten'
import InputLanguage from 'components/Widget/Template/input-language'
import debounce from 'lodash/debounce'
import ApprovalRange from '../../components/approve-person-range-form';
import RuleListSelector from '../../components/rule-list-selector'
import VisibleList from './visible-list-selector';
import {
  WFL_IDENTITY_ALL,
  WFL_IDENTITY_COMPANY,
  WFL_IDENTITY_USER_GROUP,
  WFL_IDENTITY_CONTACT,
  WFL_IDENTITY_DEPARTMENT,
  symbolText,
} from '../../components/range-type';
import service from '../../service';

const RadioGroup = Radio.Group;
const formItemStyle = {
  marginBottom: '10px'
}
const headerTitle = {
  marginBottom: '14px',
  borderBottom: '1px solid #e8e8e8',
  fontSize: '16px',
  padding: '10px 0',
  fontWeight: 700,
}
const hastenType = {
  "1002": 'app',
  "1001": 'pc',
  "1003": '邮件',
}
const typeText = {
  [WFL_IDENTITY_ALL]: '全部人员',
  [WFL_IDENTITY_COMPANY]: '公司',
  [WFL_IDENTITY_USER_GROUP]: '人员组',
  [WFL_IDENTITY_CONTACT]: '人员',
  [WFL_IDENTITY_DEPARTMENT]: '部门',
}


class CustomApprovalNode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      saveLoading: false,
      pages: [],
      addSystemHastenVisible: false, //系统催办
      addWithdrawHastenVisible: false,//申请人催办
      systemHastenValues: {}, // 作用已被finalSystemHasten替代，后期可删
      withdrawHastenValues: {},
      workflowHastenTypesDtoList: [],
      finalSystemHasten: {},  // 最终保存的时候抛出去的系统催办数据
      finalWithdrawHasten: {}, // 最终保存的时候抛出去的申请人催办数据
      rangeList: [], // 用于渲染，保存人员范围及其相关数据
      selectorView: false,
      nodeIndex: 0,
      nodeRules: [],
      warningFlag: false,
      curRangeType: 0, // 用于重编辑人员范围的已选type
      curRangeList: [], // 用于重编辑人员范围的已勾选数据
      curRangeIndex: 0, // 用于确认重编辑人员范围的目标对象在rangeList中的下标
      processSettingId: props.processSettingId,
    }
    this.handleSearch = debounce(this.handleSearch, 300)
  }

  componentDidMount() {
    const { params } = this.props;
    if (JSON.stringify(params) !== '{}' && params) {
      let rangeList = [];
      if (params.assignees && params.assignees.length) {
        rangeList = params.assignees.map(item => {
          if(Array.isArray(item.workflowIdentities) && item.workflowIdentities.length) {
            return {
              type: item.workflowIdentities[0].keyCode,
              typeList: item.workflowIdentities[0].valueObj,
              rulesList: item.approvalConditionsList,
            }
          } else {
            return {
              type: '',
              typeList: [],
              rulesList: [],
            };
          }
        })
      }
      this.setState({
        rangeList,
      },() => {
        this.getHastenValueList();
      })
    }
  }

  // 获取催办信息
  getHastenValueList = () => {
    const { processSettingId = '', nodeCode = '' } = this.props;
    if(processSettingId && nodeCode) {
      service
        .getHastenValue(processSettingId,nodeCode)
        .then(res => {
          if(res && Array.isArray(res.data)) {
            const sysValue = this.dealWithHastenTypesDtoList(res.data[0]);
            const manValue = this.dealWithHastenTypesDtoList(res.data[1]);
            this.setState({
              workflowHastenTypesDtoList: [...sysValue,...manValue],
              finalSystemHasten: res.data[0].hastenType === 'MANUAL' ? res.data[1] : res.data[0],
              finalWithdrawHasten: res.data[0].hastenType === 'MANUAL' ? res.data[0] : res.data[1],
            })
          }
        })
        .catch(err => {
          message.error(err.response.data.message)
        })
    }
  }

  // 节点页面
  handleSearch = value => {
    if (!value) {
      this.setState({ pages: [] });
      return;
    }
    service.getPages(value,"WORKFLOW_BACKLOG").then(res => {
      this.setState({ pages: res.data });
    })
  }

  // 关闭
  handleCancel = () => {
    const { onCancel } = this.props;
    if (onCancel) {
      onCancel(true);
    }
  }

  // 添加手动催办
  addWithdrawHasten = () => {
    const { workflowHastenTypesDtoList } = this.state
    const item = Array.isArray(workflowHastenTypesDtoList) && workflowHastenTypesDtoList.find(data => data.hastenType === 'MANUAL')
    this.setState({ addWithdrawHastenVisible: true, withdrawHastenValues: item })
  }

  // 添加系统催办
  addSystemHasten = () => {
    const { workflowHastenTypesDtoList } = this.state;

    const item = Array.isArray(workflowHastenTypesDtoList) &&
      workflowHastenTypesDtoList.find(data => (data.hastenType === 'SYSTEM'));

    this.setState({ addSystemHastenVisible: true, systemHastenValues: item })
  }

  //删除系统催办
  addSystemHastenDelete = (e) => {
    e.preventDefault();
    const { workflowHastenTypesDtoList } = this.state;
    workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL');
    this.setState({
      finalSystemHasten: {},
      workflowHastenTypesDtoList,
    })
  }

  //删除申请人催办
  addWithdrawHastenDelete = (e) => {
    e.preventDefault();
    const { workflowHastenTypesDtoList } = this.state;
    workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM');
    this.setState({
      finalWithdrawHasten: {},
      workflowHastenTypesDtoList,
    })
  }

  //系统催办保存提交
  addSystemHastenHandle = (value) => {
    const { hastenTimeType, hastenTimeValue, holidaysFlag } = value;
    const { processSettingId } = this.state;

    const conditions = [];
    // const { nodeId, processSettingId } = this.props;
    for (let i = 0; i < hastenTimeType.length; i++) {
      if (hastenTimeType[i] !== undefined) {
        const condition = {}
        condition.hastenTimeType = hastenTimeType[i]
        condition.hastenTimeValue = hastenTimeValue[i]
        condition.holidaysFlag = holidaysFlag[i] || false,
        conditions.push(condition);
      }
    }
    value.hastenConditions = conditions
    //避免后台接收时属性转化错误
    value.hastenTimeType = ''
    value.hastenTimeValue = ''
    value.holidaysFlag = ''

    value.hastenType = 'SYSTEM'
    value.manyTimesHasten = false;
    value.processSettingId = processSettingId;

    const tempArr = this.dealWithHastenTypesDtoList(value)
    const { workflowHastenTypesDtoList } = this.state;
    const tempArrHasten = workflowHastenTypesDtoList.filter(item => (item.hastenType !== 'SYSTEM'));

    this.setState({
      addSystemHastenVisible: false,
      workflowHastenTypesDtoList: [ ...tempArrHasten,...tempArr ],
      finalSystemHasten: value,
    })
  }

  //申请人催办保存提交
  addWithdrawHastenHandle = (value) => {
    const { hastenTimeType, hastenTimeValue, holidaysFlag } = value;
    const { processSettingId } = this.state;

    const conditions = [];
    // const { nodeId, processSettingId } = this.props;
    for (let i = 0; i < hastenTimeType.length; i++) {
      if (hastenTimeType[i] !== undefined) {
        const condition = {}
        condition.hastenTimeType = hastenTimeType[i]
        condition.hastenTimeValue = hastenTimeValue[i]
        condition.holidaysFlag = holidaysFlag[i] || false;
        conditions.push(condition)
      }
    }
    value.hastenConditions = conditions
    //避免后台接收时属性转化错误
    value.hastenTimeType = ''
    value.hastenTimeValue = ''
    value.holidaysFlag = ''
    value.intervalDurationValue = value.intervalDurationValue == "" ? null : value.intervalDurationValue

    value.hastenType = 'MANUAL'
    value.processSettingId = processSettingId;
    value.hastenTimeType = ''
    value.hastenTimeValue = ''
    value.holidaysFlag = ''

    const tempArr = this.dealWithHastenTypesDtoList(value);
    const { workflowHastenTypesDtoList } = this.state;
    const tempArrHasten = workflowHastenTypesDtoList.filter(item => (item.hastenType !== 'MANUAL'));

    this.setState({
      addWithdrawHastenVisible: false,
      workflowHastenTypesDtoList: [ ...tempArrHasten,...tempArr ],
      finalWithdrawHasten: value,
    })
  }

  /**
   * 模拟后台保存催办后返回的数据格式
   * （原本催办是实时调用接口，workflowHastenTypesDtoList来自后台处理好的格式）
   * 现在由于接口不在这里实时调用，但迎合原有渲染逻辑，只能通过函数模拟
   */
  dealWithHastenTypesDtoList = value => {
    const workflowHastenTypesDtoList = [];
    if (value) {
      value.hastenConditions.map(item => {
        const temp = {
          ...value,
          ...item,
          hastenConditions: value.hastenConditions,
        }
        workflowHastenTypesDtoList.push(temp);
      })
    }

    return workflowHastenTypesDtoList
  }


  handleSave = () => {
    const { warningFlag } = this.state;
    if (warningFlag) {
      message.warning('您还有未编辑完的操作....');
      return;
    }
    const { form: { validateFieldsAndScroll }, onSave, params = {} } = this.props;

    validateFieldsAndScroll((err, values) => {
      if (err) return;
      const { finalSystemHasten, finalWithdrawHasten, rangeList } = this.state;
      if (values.pageId) {
        values.pageId = values.pageId.key;
        values.pageName = values.pageId.label;
      }

      values.remark = values.remark.value;
      values.i18n = { remark: values.remark.i18n };

      const tempArr = rangeList.map(item => {
        const valueList = item.type === WFL_IDENTITY_DEPARTMENT ? item.typeList.map(item => item.id) : item.typeList;
        const valueListObj = item.typeList;
        return {
          interfaceId: -1,
          type: 'optional',
          workflowIdentities: [{
            keyCode: item.type,
            rangeType: 'manual',
            value: valueList,
            valueObj: valueListObj,
          }],
          approvalConditions: item.rulesList.map(item => item.id),
          approvalConditionsList: item.rulesList,
        }
      })

      const oldSetting = !!params.settings ? params.settings : {};
      const tempObj = {
        settings: { ...oldSetting, ...values },
        systemHasten: finalSystemHasten,
        manualHasten: finalWithdrawHasten,
        assignees: tempArr,
      }

      if (onSave) {
        onSave(tempObj);
      }
    })
  }

  // 添加人员范围
  handleAddRangeList = e => {
    e.preventDefault();
    const temp = {
      type: WFL_IDENTITY_ALL, // 人员范围类型
      typeList: [], // 人员范围数据
      rulesList: [], // 人员范围对应条件数据集合
      editType: 'edit'
    }
    const { rangeList } = this.state;
    rangeList.push(temp);
    this.setState({ rangeList, warningFlag: true });
  }

  /**
   * 对应人员范围数据
   * @param type: 人员范围类型： 公司，部门，人员等
   * @param ids: 部门人员等范围数据id数组
   * @param dataList: 范围数据的对象数组 [{},{}]
   * @param index: 用于确认当前添加的范围数据目标源处于rangeList数据结构中的位置
   */
  handleGetRange = (type, ids, dataList, index) => {
    if (type !== WFL_IDENTITY_ALL && dataList.length === 0) {
      message.warning('请至少选择一条数据');
      return;
    }
    const { rangeList } = this.state;
    rangeList[index].type = type;
    rangeList[index].typeList = type === WFL_IDENTITY_DEPARTMENT ? dataList : ids;
    delete rangeList[index].editType;
    this.setState({ rangeList, warningFlag: false });
  }

  /**
   * 删除人员范围
   * @param index: 当前添加条件的目标数据在rangeList中的下标
   */
  handleDeleteRange = (e, index) => {
    if (e) { e.preventDefault(); }
    if (index >= 0) {
      const { rangeList } = this.state;
      rangeList.splice(index, 1);
      this.setState({ rangeList, warningFlag: false });
    }
  }

  // 关闭审批条件模态框
  handleCancelSelector = () => {
    this.setState({ selectorView: false, nodeIndex: 0 });
  }

  /**
   * 展开审批条件模态框
   * @param index: 当前添加条件的目标数据在rangeList中的下标
   */
  handleOpenSelector = (e, index) => {
    e.preventDefault();
    if (index >= 0) {
      const { rangeList } = this.state;
      const nodeRules = rangeList[index].rulesList;
      this.setState({ selectorView: true, nodeIndex: index, nodeRules });
    }
  }

  /**
   * 重编辑人员范围数据-展开模态框
   * @param index: 当前添加条件的目标数据在rangeList中的下标
   */
  handleEditSelRange = (e, index) => {
    e.preventDefault();
    if (index >= 0) {
      const { rangeList } = this.state;
      this.setState({
        curRangeType: rangeList[index].type,
        curRangeList: rangeList[index].typeList,
        curRangeIndex: index,
      })
    }
  }

  handleCancelReEditRange = () => {
    this.setState({
      curRangeType: 0,
      curRangeList: [],
      curRangeIndex: 0,
    })
  }

  /**
   * 重编辑人员范围数据-勾选数据
   * @param values: 组件回调勾选数据
   */
  handleReEditRange = values => {
    const { rangeList, curRangeIndex } = this.state;
    rangeList[curRangeIndex].typeList = values;
    this.setState({
      rangeList,
    })
  }

  // 回调获取审批条件模态框数据
  handleSelectConditions = values => {
    const { rangeList, nodeIndex } = this.state;
    rangeList[nodeIndex].rulesList = (values.result || []).map(item =>
      ({id: item.id, name: item.conditionSetName, conditionInfo: item.conditionInfo, code: item.conditionSetCode})
    );
    this.setState({ rangeList }, this.handleCancelSelector);
  }

  handleCloseTag = (id, index) => {
    const { rangeList } = this.state;
    const dataIndex = Array.isArray(rangeList[index].rulesList)
      && rangeList[index].rulesList.findIndex(rule => rule.id === id);
    if (dataIndex >= 0) {
      rangeList[index].rulesList.splice(dataIndex, 1);
    }
    this.setState({ rangeList })
  }

  /**
   * 渲染已选条件 tag 上的 popover
   */
  renderPop = (infoList) => {
    return (infoList && typeof infoList === 'string' && Array.isArray(JSON.parse(infoList))) ? (
      <div className="ant-popover-box" style={{overflow: 'scroll', maxHeight: '280px', minHeight: '100px'}}>
        {
          JSON.parse(infoList).map((info,index) => {
            const dataIndex = `ruleInfoPop${index}`
            return (
              <Row key={dataIndex} style={{width: '360px'}}>
                <Col
                  span={4}
                  className="ant-form-item-required"
                  style={{textAlign: 'right', padding: '0 5px'}}
                >
                  {`${info.paramName} :`}
                </Col>
                {
                  info.paramType !== 'TEXT' ? (
                    <Col span={18}>{`${info.numberData1} ${symbolText[info.symbol1]} 金额 ${symbolText[info.symbol2]} ${info.numberData2}`}</Col>
                  ) : (
                    <>
                      <Col span={4}>{info.contains === 'include' ? '包含' : '排除'}</Col>
                      {
                        info.limit
                          ? (<Col span={16}>{`${info.limit} ~ ${info.upper}`}</Col>)
                          : (
                            <Col span={16}>
                              {this.renderTag(info.lovParams)}
                            </Col>
                          )
                      }
                    </>
                    )
                }
              </Row>
            )
          })
        }
      </div>
    ) : (
      <Empty />
    )
  }

  renderTag = lists => {
    const list = typeof lists === 'string' ? JSON.parse(lists) : lists;
    if(list) {
      if(Array.isArray(list)) {
        return list.map((param,index) => {
          const dataIndex = `paramName${index}`
          return (<Tag key={dataIndex}>{param.name}</Tag>)
        })
      } else if(
        list.selectedRows
        && Array.isArray(list.selectedRows)
      ) {
        return list.selectedRows.map((param,index) => {
          const dataIndex = `paramName${index}`
          return (<Tag key={dataIndex}>{param.name}</Tag>)
        })
      } else return null
    } else return null
  }

  render() {
    const {
      saveLoading,
      pages,
      addSystemHastenVisible,
      addWithdrawHastenVisible,
      workflowHastenTypesDtoList,
      finalSystemHasten,
      finalWithdrawHasten,
      rangeList,
      selectorView,
      nodeRules,
      curRangeType,
      curRangeList,
    } = this.state;
    const { form: { getFieldDecorator, getFieldValue }, params = {}, processTypeId = '' } = this.props;
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 19 },
    };
    const editFlag = JSON.stringify(params) !== "{}" && params.settings;
    const editParam = typeof params.settings === 'string'
      ? JSON.parse(params.settings)
      : params.settings;

    return (
      <>
        <Form>
          <div style={headerTitle}>基本信息</div>
          <Form.Item {...formItemLayout} label='节点名称' style={formItemStyle}>
            {getFieldDecorator('remark', {
              rules: [{
                required: true,
                message: this.$t('common.please.enter'), // 请输入
              }],
              initialValue: editFlag ? {
                value: editParam.remark,
                i18n: editParam.i18n ? editParam.i18n.remark : [],
              } : undefined,
            })(
              <InputLanguage
                placeholder={this.$t('common.please.enter')}
                style={{ maxWidth: 300, width: '100%' }}
              />
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label='节点页面' style={formItemStyle}>
            {getFieldDecorator('pageId', {
              initialValue: editFlag ? { value: editParam.pageId, label: editParam.pageName } : undefined,
            })(
              <Select
                labelInValue
                showSearch
                placeholder="请输入"
                defaultActiveFirstOption={false}
                showArrow={false}
                filterOption={false}
                onSearch={this.handleSearch}
                notFoundContent={null}
                style={{ maxWidth: 300, width: '100%' }}
                allowClear
              >
                {pages.map(item => (
                  <Select.Option key={item.id}>{item.pageName}</Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <div style={{ ...headerTitle, marginTop: '16px' }}>审批规则</div>
          <Form.Item {...formItemLayout} label='审批规则' style={formItemStyle}>
            {getFieldDecorator('countersignRule', {
              initialValue: editFlag ? editParam.countersignRule : 3001,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <Radio.Group>
                <Radio value={3001}>所有人通过</Radio>
                <Radio value={3002}>一票通过/一票驳回</Radio>
                <Radio value={3003}>任一人通过</Radio>
              </Radio.Group>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='包含申请人' style={formItemStyle}>
            {getFieldDecorator('selfApprovalRule', {
              initialValue: editFlag ? editParam.selfApprovalRule : 5001,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <RadioGroup onChange={this.selfApprovalRuleChange}>
                <Radio value={5002}>不跳过</Radio>
                <Radio value={5001}>跳过</Radio>
                <Radio value={5003}>自动审批</Radio>
              </RadioGroup>
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label='包含提单人' style={formItemStyle}>
            {getFieldDecorator('submitterApprovalRule', {
              initialValue: editFlag ? editParam.submitterApprovalRule : 5001,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <RadioGroup>
                <Radio value={5002}>不跳过</Radio>
                <Radio value={5001}>跳过</Radio>
                <Radio value={5003}>自动审批</Radio>
              </RadioGroup>
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label='节点为空时' style={formItemStyle}>
            {getFieldDecorator('nullableRule', {
              initialValue: editFlag ? editParam.nullableRule : 2001,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <Radio.Group>
                <Radio value={2001}>跳过</Radio>
                <Radio value={2002}>不跳过</Radio>
              </Radio.Group>
            )}
          </Form.Item>
          <Form.Item {...formItemLayout} label='是否重复审批' style={formItemStyle}>
            {getFieldDecorator('repeatRule', {
              initialValue: editFlag ? editParam.repeatRule : 4002,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <RadioGroup onChange={this.selfApprovalRuleChange}>
                <Radio value={4001}>是</Radio>
                <Radio value={4002}>否</Radio>
              </RadioGroup>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='是否允许加签' style={formItemStyle}>
            {getFieldDecorator('addsignFlag', {
              initialValue: editFlag ? editParam.addsignFlag : false,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <Radio.Group>
                <Radio value>是</Radio>
                <Radio value={false}>否</Radio>
              </Radio.Group>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='是否允许转交' style={formItemStyle}>
            {getFieldDecorator('transferFlag', {
              initialValue: editFlag ? editParam.transferFlag : false,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <Radio.Group>
                <Radio value>是</Radio>
                <Radio value={false}>否</Radio>
              </Radio.Group>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='允许退回指定节点' style={formItemStyle}>
            {getFieldDecorator('returnFlag', {
              initialValue: editFlag ? editParam.returnFlag : true,
              rules: [{
                required: true,
                message: this.$t('common.please.select'),
              }],
            })(
              <Radio.Group onChange={this.returnFlagChange}>
                <Radio value>是</Radio>
                <Radio value={false}>否</Radio>
              </Radio.Group>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='可退回节点' style={formItemStyle}>
            {getFieldDecorator('returnType', {
              initialValue: editFlag ? editParam.returnType : 1001,
            })(
              <Radio.Group>
                <Radio value={1001}>本节点前该子流程及主流程内节点</Radio>
                <Radio value={1002}>子流程内任意节点</Radio>
                <Radio value={1003}>自选节点</Radio>
              </Radio.Group>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label='系统催办' style={formItemStyle}>
            {getFieldDecorator('systemFlag', {
              initialValue: editFlag ? editParam.systemFlag : false,
            })(
              <Radio.Group>
                <Radio value>是</Radio>
                <Radio value={false}>否</Radio>
                {getFieldValue('systemFlag') === true && (
                  <span>
                    {getFieldValue('systemFlag') === true && workflowHastenTypesDtoList
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM')
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM').length === 0 && (
                        <Button icon="plus-circle" type='link' onClick={() => this.addSystemHasten()}>添加催办</Button>
                      )}
                    {getFieldValue('systemFlag') === true && workflowHastenTypesDtoList
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM')
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM').length > 0 && (
                        <span>
                          <a onClick={() => this.addSystemHasten()}>编辑</a>
                          <Divider type='vertical' />
                          <a onClick={(e) => this.addSystemHastenDelete(e)}>删除</a>
                        </span>
                      )}
                  </span>
                )}
              </Radio.Group>
            )}
          </Form.Item>
          {
            getFieldValue('systemFlag') === true && workflowHastenTypesDtoList
            && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM')
            && workflowHastenTypesDtoList.filter(item => item.hastenType === 'SYSTEM').map((item, index) => {
              const dataIndex = `system${index}`
              return (
                <Row key={dataIndex}>
                  <Col offset={5}>
                    <Row>
                      <Col span={2}>
                        催办{index + 1}:
                        </Col>
                      <Col span={10}>
                        <div>距{item.hastenTimeType === '1001' ? '到达节点时间' : '单据提交时间'}超过
                            {item.hastenTimeValue}天({item.holidaysFlag === false ? '含' : '不含'}节假日)
                          </div>
                        {item.hastenChannel && (
                          <div>
                            通过{item.hastenChannel.map(item => hastenType[item]).join("、")}提醒当前节点审批人
                            </div>)}
                      </Col>

                    </Row>
                  </Col>
                </Row>
              )
            })
          }

          <Form.Item {...formItemLayout} label='申请人催办' style={formItemStyle}>
            {getFieldDecorator('withdrawFlag', {
              initialValue: editFlag ? editParam.withdrawFlag : false,
            })(
              <Radio.Group>
                <Radio value={true}>是</Radio>
                <Radio value={false}>否</Radio>
                {getFieldValue('withdrawFlag') === true && (
                  <span>
                    {getFieldValue('withdrawFlag') === true && workflowHastenTypesDtoList
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL')
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL').length === 0 && (
                        <Button icon="plus-circle" type='link' onClick={() => this.addWithdrawHasten()}>添加催办</Button>
                      )}
                    {getFieldValue('withdrawFlag') === true && workflowHastenTypesDtoList
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL')
                      && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL').length > 0 && (
                        <span>
                          <a onClick={() => this.addWithdrawHasten()}>编辑</a>
                          <Divider type='vertical' />
                          <a onClick={(e) => this.addWithdrawHastenDelete(e)}>删除</a>
                        </span>
                      )}
                  </span>
                )}
              </Radio.Group>
            )}
          </Form.Item>
          {
            getFieldValue('withdrawFlag') === true && workflowHastenTypesDtoList
            && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL')
            && workflowHastenTypesDtoList.filter(item => item.hastenType === 'MANUAL').map((item, index) => {
              const dataIndex = `manual${index}`;
              return (
                <Row key={dataIndex}>
                  <Col offset={5}>
                    <Row>
                      <Col span={2}>
                        催办{index + 1}:
                        </Col>
                      <Col span={10}>
                        <div>距{item.hastenTimeType === '1001' ? '到达节点时间' : '单据提交时间'}超过
                            {item.hastenTimeValue}天({item.holidaysFlag === false ? '含' : '不含'}节假日)
                          </div>
                        {!item.intervalDurationValue && (<div>{item.manyTimesHasten === true ? '可' : '不可'}重复催办</div>)}
                        {item.intervalDurationValue &&
                          (<div>{item.manyTimesHasten === true ? '可' : '不可'}重复催办
                            间隔时长:{item.intervalDurationValue}
                            {item.manualHastenType === '1001' ? '天' : '小时'}</div>)
                        }
                        {item.hastenChannel && (
                          <div>通过{
                            item.hastenChannel.map(item => hastenType[item]).join("、")
                          }提醒当前节点审批人</div>)}
                        {(item.manualHastenType === '1002' || item.manualHastenType === '1003') && (
                          <div>
                            通过{item.manualHastenType === '1002' ? '自主选择' : '手工添加催办消息'}提醒当前节点审批人
                            </div>
                        )}
                      </Col>
                    </Row>
                  </Col>
                </Row>
              )
            })
          }
        </Form>
        <div style={{ ...headerTitle, marginTop: '16px' }}>审批人员范围</div>
        <Row style={{ ...headerTitle, marginTop: '16px', fontWeight: 'normal', paddingTop: 0 }}>
          <a onClick={e => { this.handleAddRangeList(e) }}>
            <Icon type="plus" style={{ marginRight: '4px' }} />添加审批人员
          </a>
        </Row>
        {/* 人员范围 */}
        {
          Array.isArray(rangeList)
          && !!rangeList.length
          && rangeList.map((range, index) => {
            const dataIndex = `range${index}`;
            return (
              <div
                style={{ padding: '10px 0', borderBottom: '1px solid #e8e8e8', margin: '0 14px' }}
                key={dataIndex}
              >
                <Row>
                  {range.editType === 'edit' ? (
                    <>
                      <Col span={4}>{`人员范围${index + 1}`}</Col>
                      <Col span={20}>
                        <ApprovalRange
                          onOk={(values, ids, dataList) => { this.handleGetRange(values, ids, dataList, index) }}
                          handleCancelRangeForm={() => { this.handleDeleteRange(undefined, index) }}
                          range={{selectOids: range.rangeList, approverType: range.type}}
                        />
                      </Col>
                    </>
                  ) : (
                      <>
                        <Col span={4}>
                          <Badge
                            status="default"
                            text={String(range.type) === WFL_IDENTITY_ALL ? '全部人员' : `按${typeText[range.type]}添加`}
                          />
                        </Col>
                        <Col span={20}>
                          <Col span={18}>
                            <span>
                              {
                                String(range.type) === WFL_IDENTITY_ALL ? (
                                  `已选择${typeText[range.type]}`
                                ) : (
                                    <a onClick={e => { this.handleEditSelRange(e, index) }}>{`已选择 ${range.typeList.length} ${typeText[range.type]}`}</a>
                                  )
                              }
                            </span>
                          </Col>
                          <Col span={6}>
                            <span>
                              <a onClick={e => { this.handleOpenSelector(e, index) }}>添加条件</a>
                              <Divider type="vertical" />
                              <a onClick={e => { this.handleDeleteRange(e, index) }}>删除</a>
                            </span>
                          </Col>
                        </Col>
                      </>
                    )
                  }
                </Row>

                {
                  Array.isArray(range.rulesList)
                  && !!range.rulesList.length
                  && range.rulesList.map((rule, index) => {
                    const dataIndex = `rule${index}`
                    return (
                      <Row style={{ padding: '10px 0' }} key={dataIndex}>
                        <Col span={4} style={{ textAlign: 'right', paddingRight: '10px' }}>
                          {`${this.$t('setting.key1330')} :`}
                        </Col>
                        <Col span={20}>
                          <Popover
                            title={`${rule.code}-${rule.name}`}
                            content={this.renderPop(rule.conditionInfo)}
                            getPopupContainer={node => node.parentNode}
                            overlayClassName="pop-card"
                            overlayStyle={{width: '400px'}}
                            key={rule.id}
                          >
                            <Tag key={rule.id} closable onClose={() => { this.handleCloseTag(rule.id, index) }}>
                              {rule.name}
                            </Tag>
                          </Popover>
                        </Col>
                      </Row>
                    )
                  })
                }

              </div>
            )
          })
        }
        {/* 查看勾选的人员范围，并修改 */}
        <VisibleList
          type={curRangeType}
          selectedData={curRangeList}
          onCancel={this.handleCancelReEditRange}
          onEdit={this.handleReEditRange}
        />
        {/* 审批条件 */}
        <RuleListSelector
          visible={selectorView}
          type='wfl_approval_condition'
          labelKey="conditionSetName"
          valueKey="id"
          onCancel={this.handleCancelSelector}
          onOk={this.handleSelectConditions}
          selectedData={nodeRules}
          extraParams={{ typeId: processTypeId }}
        />

        {/* 添加系统催办 */}
        {addSystemHastenVisible && (
          <SystemHasten
            onCancel={() => { this.setState({ addSystemHastenVisible: false }) }}
            visible={addSystemHastenVisible}
            onOk={this.addSystemHastenHandle}
            values={finalSystemHasten}
          />
        )}


        {/* 添加申请人催办 */}
        {addWithdrawHastenVisible && (
          <WithdrawHasten
            onCancel={() => { this.setState({ addWithdrawHastenVisible: false }) }}
            visible={addWithdrawHastenVisible}
            onOk={this.addWithdrawHastenHandle}
            values={finalWithdrawHasten}
          />)}
        <div className="slide-footer">
          <Button className="btn" type="primary" loading={saveLoading} onClick={this.handleSave}>
            {this.$t('common.ok')}
          </Button>
          <Button className="btn" onClick={this.handleCancel}>
            {this.$t('common.cancel')}
          </Button>
        </div>
      </>
    )
  }
}

export default Form.create()(CustomApprovalNode);
