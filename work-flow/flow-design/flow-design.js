/* eslint-disable */
import React from "react"
import { Icon, Input } from 'antd'
import { uuid, createNode, handleGetTreeExtent, handleGetTreeDeep } from "../utils"
import NodeMenu from "./node-menu"
import G6 from '@antv/g6';
import registerNode from './register-node'

let leftSpan = 100;
const topSpan = 60;

const span = 180;

const ySpan = 60;
class WorkFlow extends React.Component {

  constructor(props) {
    super(props);
    const id = uuid();
    this.state = {
      nodes: props.data || {
        id: "start",
        type: "node",
        dataType: "start",
        title: "开始",
        children: [{
          id,
          parent: "start",
          type: 'add',
        }],
        end: {
          id: "end",
          type: "node",
          dataType: "end",
          title: "结束",
        }
      },
      nodeContextMenuX: 0,
      nodeContextMenuY: 0,
      showBranchMenu: false,
      showNormalMenu: false,
      showNodeMenu: false,
      nodeType: "",
      scale: 1,
    }
  }

  data = {
    nodes: [],
    edges: [],
  };

  currentNode = null;
  parentNode = null;

  maxWidth = 0;

  componentDidMount() {

    const { nodes } = this.state;

    const { onDataChange, id = "flow-box", disabledEvent = false } = this.props;

    onDataChange(nodes);

    this.maxWidth = handleGetTreeExtent([nodes]);

    this.registerEdge();
    this.registerCustomEdge();
    registerNode.start();
    registerNode.end();
    registerNode.approval();
    registerNode.condition();

    this.graph = new G6.Graph({
      container: id,
      width: window.screen.width - 300,
      height: window.screen.height - 64,
    });

    if (!disabledEvent) {
      // 监听节点上面右键菜单事件
      this.graph.on('node:mouseenter', evt => {
        const { item } = evt;
        const model = item.getModel();
        const { x, y, type, id } = model;

        this.currentNode = this.getNodeById([this.state.nodes], id);
        this.parentNode = this.getNodeById([this.state.nodes], this.currentNode.parent);

        const point = graph.getCanvasByPoint(x, y);

        const offsetTop = document.querySelector(".flow-design-container .flow-design-box").scrollTop;

        this.setState({
          nodeContextMenuX: point.x,
          nodeContextMenuY: point.y + 50 - offsetTop,
          showNodeMenu: true,
        });
      })

      this.graph.on('click', () => {
        this.setState({
          showBranchMenu: false,
          showNormalMenu: false,
          showNodeMenu: false,
        });
      })
    }



    this.renderNode(this.state.nodes);

    this.graph.read(this.data);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.data !== nextProps.data) {
      this.setState({ nodes: nextProps.data }, () => {
        const { nodes } = this.state;
        this.data = {
          nodes: [],
          edges: [],
        };
        this.renderGraph(nodes);
        this.setState({
          nodes,
          showBranchMenu: false,
          showNormalMenu: false,
          showNodeMenu: false,
        });
      });
    }
  }

  renderNode = (root, parent, level = 0, offset = 0, deep = 0, total = 0, parentX = 0) => {

    const node = this.createNode(root.type, root.dataType);
    if (parentX) {
      node.x = parentX;
    } else {
      if (offset) {
        if (root.children) {
          const maxChildrenLength = handleGetTreeExtent([root]);
          const temp = (this.maxWidth - offset - maxChildrenLength - total) / 2;
          node.x = leftSpan + offset * span + ((maxChildrenLength - 1) / 2 + temp) * span;
        } else {
          const temp = (this.maxWidth - offset - 1 - total) / 2;
          node.x = leftSpan + offset * span + temp * span;
        }
      } else {
        if (level > 0) {
          node.x = ((this.maxWidth - (total + 1)) / 2) * span + leftSpan;
        } else {
          node.x = ((this.maxWidth - 1) / 2) * span + leftSpan;
        }
      }
    }

    if (deep) {
      node.y = (deep + level) * ySpan + topSpan;
    } else {
      node.y = level * ySpan + topSpan;
    }
    node.id = root.id;
    node.title = root.title;

    if (root.children) {
      this.handleChildren(root, parent, level, offset, deep, total, 0, 0);
    }

    if (root.end) {
      deep += handleGetTreeDeep({ ...root, end: null });
      this.renderNode(root.end, root, level, offset, deep, total, node.x);
    }

    // 父子连线
    if (parent && parent.end.id !== root.id) {
      let line;
      if (root.type === "node") {
        line = this.createLine();
      } else {
        line = this.createLine("add-line");
      }
      line.source = parent.id;
      line.target = root.id;
    }
  }

  handleChildren = (node, parent, level, offset, deep, total, parentX, max) => {
    node.children.forEach((item, index) => {
      let rightLength = 0;
      const length = handleGetTreeExtent([item]) || 1;
      for (let i = index + 1; i < node.children.length; i++) {
        rightLength += handleGetTreeExtent([node.children[i]]) || 1;
      }

      // 闭合连线
      if (node.end) {
        let line;
        let end = item.end;
        while (end && end.end) {
          end = end.end;
        }

        if (node.end.type === "node") {
          line = this.createLine();
        } else {
          line = this.createLine("line");
        }

        if (end) {
          line.source = end.id;
        } else {
          line.source = item.id;
        }
        line.target = node.end.id;
      }
      this.renderNode(item, node, level + 1, offset, deep, total + rightLength, parentX);
      offset += length;
    });
  }

  createLine = (shape = "custom-line") => {
    const line = {
      shape,
      sourceAnchor: 1,
      targetAnchor: 0,
      style: {
        stroke: '#87e8de',
      },
    }
    this.data.edges.push(line);
    return line;
  }

  click = (item) => {

    const { nodes } = this.state;
    const { id } = this.currentNode;
    const record = this.getNodeById([nodes], id);

    switch (item.key) {
      case "child":
        this.addChildBranch(record);
        break;
      case "approval":
        this.addAppvoreNode(record, "approval", "审批");
        break;
      case "robot":
        this.addAppvoreNode(record, "robot", "机器人");
        break;
      case "current":
        this.addCurrentBranch(record);
        break;
      case "main":
        this.addMainBranch(record);
        break;
      case "branch":
        this.addChildBranch(record);
        break;
      case "condition":
        this.addAppvoreNode(record, "condition", "条件");
        break;
      case "attr":
        const { onAttrClick } = this.props;
        onAttrClick(record);
        break;
      case "notice":
        const { onNoticeClick } = this.props;
        onNoticeClick(record);
        break;
      case "setting":
        const { onConditionClick } = this.props;
        onConditionClick(record);
        break;
      case "custom-approval":
        this.addAppvoreNode(record, "custom-approval", "自选审批");
        break;
      case "delete":
        this.deleteNode(record);
        break;
      case "approval-chain":
        this.addAppvoreNode(record, "approval-chain", "审批链");
        break;
    }
    const { onDataChange } = this.props;
    onDataChange(nodes);
  }

  // 删除
  deleteNode = (record) => {
    if (!record.parent) {
      record.children = null;
      record.end = null;
      return;
    }
    const parent = this.getNodeById([this.state.nodes], record.parent);
    if (parent && parent.children) {
      if (parent.children.length > 2) {
        const index = parent.children.findIndex(o => o.id === record.id);
        if (index >= 0) {
          parent.children.splice(index, 1);
        }
      } else if (parent.children.length === 2) {
        if (parent.children[0].id === record.id) {
          const superior = this.getNodeById([this.state.nodes], parent.parent);
          parent.children[1].parent = superior.id;
          for (let i = 0; i < superior.children.length; i++) {
            if (superior.children[i].id === parent.id) {
              superior.children[i] = parent.children[1];
            }
          }
        }
        if (parent.children[1].id === record.id) {
          const superior = this.getNodeById([this.state.nodes], parent.parent);
          parent.children[0].parent = superior.id;
          for (let i = 0; i < superior.children.length; i++) {
            if (superior.children[i].id === parent.id) {
              superior.children[i] = parent.children[0];
            }
          }
        }
      } else if (parent.children.length === 1) {
        if (parent.id === 'start') {
          parent.children = [{
            id: record.id,
            parent: record.parent,
            type: record.type,
          }];
          parent.end = {
            id: "end",
            type: "node",
            dataType: "end",
            title: "结束",
          };
        } else {
          if (parent.end && parent.end.children) {
            parent.end.children.forEach(item => { item.parent = parent.id });
            parent.children = [...parent.end.children];
            parent.end = parent.end.end;
          } else {
            parent.children = null;
            parent.end = null;
          }
        }
      }
    }
  }

  // 添加分支条件
  addChildBranch = (record) => {
    if (record.children) {
      const children = [...record.children];
      const { end } = record;
      if (record.children.length === 1) {
        record.children = [{
          id: uuid(),
          parent: record.id,
          type: 'add'
        }, {
          id: uuid(),
          parent: record.id,
          type: 'add'
        }];
        record.end = {
          id: uuid(),
          children,
          end,
          type: 'add'
        }
      } else {
        record.children.push({
          id: uuid(),
          parent: record.id,
          type: 'add'
        });
      }
    } else {
      record.children = [
        { id: uuid(), parent: record.id, type: 'add' },
        { id: uuid(), parent: record.id, type: 'add' },
      ];
      record.end = {
        id: uuid(),
        type: 'add',
      }
    }
  }

  // 添加审批节点
  addAppvoreNode = (record, type, name) => {
    if (record.children) {
      const children = [...record.children];
      const { end } = record;
      record.children = [{
        id: uuid(),
        title: name,
        type: "node",
        dataType: type,
        parent: record.id,
      }];
      record.end = { id: uuid(), children, end, type: 'add' };
    } else {
      record.children = [{
        id: uuid(),
        title: name,
        type: "node",
        dataType: type,
        parent: record.id,
      }];
      record.end = { id: uuid(), type: 'add' };
    }
  }

  // 添加当前分支
  addCurrentBranch = record => {
    const children = [...record.children];
    const { end } = record;
    record.children = [{
      id: uuid(),
      type: 'add',
      parent: record.id,
    }, {
      id: uuid(),
      type: 'add',
      parent: record.id,
    }];
    const id = uuid();
    children.forEach(item => { item.parent = id });
    record.end = {
      id,
      children,
      end,
      type: 'add',
    };
  }

  // 添加主干分支
  addMainBranch = record => {
    const children = [...record.children];
    const { end } = record;

    const id = uuid();
    if (children) {
      children.forEach(item => { item.parent = id })
    }
    record.children = [{
      id,
      children,
      end,
      type: 'add',
      parent: record.id,
    }, {
      id: uuid(),
      type: 'add',
      parent: record.id,
    }];
    record.end = {
      id: uuid(),
      type: 'add',
    };
  }

  renderGraph = (nodes) => {
    const { scale } = this.state;
    this.maxWidth = handleGetTreeExtent([nodes]);

    const width = Math.max(this.maxWidth * span, window.screen.width - 300);
    const height = Math.max(handleGetTreeDeep(nodes) * span + topSpan, window.screen.height - 64);

    leftSpan = width - (this.maxWidth - 1) * span;
    leftSpan = leftSpan / 2;
    leftSpan = leftSpan / scale;
    this.graph.changeSize(width, height);
    this.renderNode(nodes);
    this.graph.data(this.data);
    this.graph.render();
    this.graph.zoom(scale);

  }

  getNodeById = (data, id) => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].id === id) return data[i];
      if (data[i].children) {
        const node = this.getNodeById(data[i].children, id);
        if (node) return node;
      }
      if (data[i].end) {
        if (data[i].end.id === id) return data[i].end;
        const node = this.getNodeById([data[i].end], id);
        if (node) return node;
      }
    }
  }

  createNode(type, dataType) {
    let node = createNode(type);
    if (type === 'node') {
      node = createNode(dataType);
    }
    this.data.nodes.push(node);
    return node;
  }

  registerEdge() {
    G6.registerEdge('custom-line', {
      draw(cfg, group) {
        const startPoint = cfg.startPoint;
        const endPoint = cfg.endPoint;
        let height = endPoint.y - startPoint.y;
        height = Math.abs(height);
        const shape = group.addShape('path', {
          attrs: {
            stroke: '#abb6c2',
            path: [
              ['M', startPoint.x, startPoint.y],
              ['L', startPoint.x, endPoint.y - 10],
              ['L', endPoint.x, endPoint.y - 10],
              ['L', endPoint.x, endPoint.y]
            ],
            endArrow: {
              path: 'M 5,0 L -5,-5 L -5,5 Z',
              d: 10
            }
          }
        });
        return shape;
      }
    });
    G6.registerEdge('line', {
      draw(cfg, group) {
        const startPoint = cfg.startPoint;
        const endPoint = cfg.endPoint;
        let height = endPoint.y - startPoint.y;
        height = Math.abs(height);
        const shape = group.addShape('path', {
          attrs: {
            stroke: '#abb6c2',
            path: [
              ['M', startPoint.x, startPoint.y],
              ['L', startPoint.x, endPoint.y - 10],
              ['L', endPoint.x, endPoint.y - 10],
              ['L', endPoint.x, endPoint.y]
            ],
            endArrow: false,
          }
        });
        return shape;
      }
    });
  }

  registerCustomEdge() {
    G6.registerNode("base-node", {
      draw(cfg, group) {
        const style = this.getShapeStyle(cfg);

        const shape = group.addShape('rect', {
          attrs: {
            ...style,
            width: style.width || 86,
            height: style.height || 46,
          },
        });
        if (cfg.icon && cfg.icon.img) {
          group.addShape('image', {
            attrs: {
              x: style.x + (cfg.icon.x || 0),
              y: style.y + (cfg.icon.y || 0),
              width: cfg.icon.width || 12,
              height: cfg.icon.height || 12,
              img: cfg.icon.img,
            }
          });
        }

        if (cfg.title) {
          let { title } = cfg;
          if (title.length > 5) {
            title = title.substr(0, 5);
            title += '.'.repeat(3);
          }
          group.addShape('text', {
            attrs: {
              x: style.x + style.width / 2,
              y: style.y + style.height / 2,
              textAlign: 'center',
              textBaseline: 'middle',
              text: title,
              fill: '#4390FF',
              fontSize: 14,
            }
          });
        }
        return shape;
      },
      setState(name, value, item) {
        const group = item.getContainer();
        const { style } = item.getModel();
        if (name === 'selected') {
          const rect = group.getChildByIndex(0);

          if (!style) {
            rect.attr('fill', '#95D6FB');
            return;
          }

          if (value) {
            rect.attr('fill', value);
          } else {
            if (style.selected && style.selected.fill) {
              rect.attr('fill', style.selected.fill);
            } else {
              rect.attr('fill', '#95D6FB');
            }
          }
        }
      },
    }, "rect");
  }

  renderMenu = () => {
    const { showNodeMenu, nodeContextMenuX, nodeContextMenuY, scale, nodes } = this.state;
    return (
      <React.Fragment>
        {showNodeMenu && <NodeMenu node={this.currentNode} parentNode={this.parentNode}
          onClick={this.click} x={nodeContextMenuX} y={nodeContextMenuY} />}
      </React.Fragment>
    )
  }

  render() {
    const { scale, nodes } = this.state;
    const { id = 'flow-box' } = this.props;
    return (
      <div>
        <div style={{ backgroundColor: '#f7f8fa' }} id={id}>
          <div style={{ position: 'absolute', top: 10, right: 10, zIndex: 103 }}>
            <Input
              style={{ width: 150, textAlign: 'center' }}
              addonBefore={<Icon type="plus" onClick={() => {
                if (scale > 1.7) return;
                this.setState({ scale: scale + 0.1 }, () => {
                  this.data = {
                    nodes: [],
                    edges: [],
                  };
                  this.renderGraph(nodes);
                })
              }} />}
              addonAfter={<Icon type="minus" onClick={() => {
                if (scale < 0.5) return;
                this.setState({ scale: scale - 0.1 }, () => {
                  this.data = {
                    nodes: [],
                    edges: [],
                  };
                  this.renderGraph(nodes);
                })
              }} />}
              disabled
              size="small"
              value={`${parseInt(scale * 100)}%`}
            />
          </div>
        </div>
        {this.renderMenu()}
      </div>
    )
  }
}

export default WorkFlow
