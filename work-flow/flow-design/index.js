import React, { useState, useEffect } from "react"
import { Button, message, Icon, Card, Spin, Badge, Modal } from "antd"
import { uuid } from "../utils"

import { pushPage } from "utils/utils"

import FlowDesign from "./flow-design"
import SlideFrame from "widget/slide-frame"

import ApprovalNode from "../containers/approval-node"
import RobotNode from "../containers/robot-node"
import NoticeSetting from "../containers/notice-setting"
import ConditionsNode from "../containers/conditions-node"
import WorkflowDefineFrame from "../containers/workflow-definition/new-workflow-line"
import CustomApprovalNode from "../containers/custom-approval-node"
import ApprovalChainNode from "../containers/approval-chain"
import service from "../service"

import "../styles/flow-design.less"

function getNodeById(data, id) {
  for (let i = 0; i < data.length; i++) {
    if (data[i].id === id) return data[i];
    if (data[i].children) {
      const node = getNodeById(data[i].children, id);
      if (node) return node;
    }
    if (data[i].end) {
      if (data[i].end.id === id) return data[i].end;
      const node = getNodeById([data[i].end], id);
      if (node) return node;
    }
  }
}

function WorkFlow({ match }) {

  const [attrVisible, setAttrVisible] = useState(false);
  const [noticeVisible, setNoticeVisible] = useState(false);
  const [conditionVisible, setConditionVisible] = useState(false);

  const [data, setData] = useState({});
  const [currentNode, setCurrentNode] = useState({});
  const [type, setType] = useState();

  const [submiting, setSubmiting] = useState(false);
  const [getting, setGetting] = useState(true);
  const [settingData, setSettingData] = useState({});

  const [editVisible, setEditVisible] = useState(false);

  const [hasten, setHasten] = useState({});

  useEffect(() => {
    getData();
  }, [])

  function attrClick(node) {
    setCurrentNode(node);
    setAttrVisible(true);
    setType(node.dataType);
  }

  function noticeClick(node) {
    setCurrentNode(node);
    setNoticeVisible(true);
    setType(node.dataType);
  }

  function conditionClick(node) {
    setCurrentNode(node);
    setConditionVisible(true);
    setType(node.dataType);
  }


  function save(result, type) {

    hasten[currentNode.id] = {
      systemHasten: { ...result.systemHasten, nodeCode: currentNode.id },
      manualHasten: { ...result.manualHasten, nodeCode: currentNode.id },
    }
    const record = getNodeById([data], currentNode.id);
    if (type !== "condition") {
      record.title = result.settings.remark;
      if (record.attribute) {
        record.attribute = { ...record.attribute, ...result };
      } else {
        record.attribute = result;
      }
    } else {
      record.title = result.settings.remark;
      record.conditions = result;
    }

    setAttrVisible(false);
    setNoticeVisible(false);
    setConditionVisible(false);
    setData(JSON.parse(JSON.stringify(data)));
    setHasten({ ...hasten });
  }

  // 重置数据
  function reset() {
    Modal.confirm({
      title: '提示',
      content: '确认重置？重置后所做的更改将会丢失。',
      okText: '确认',
      cancelText: '取消',
      onOk: () => {
        setData({
          id: "start",
          type: "node",
          dataType: "start",
          title: "开始",
          children: [{
            id: uuid(),
            parent: "start",
            type: 'add',
          }],
          end: {
            id: "end",
            type: "node",
            dataType: "end",
            title: "结束",
          }
        });
      }
    });
  }


  async function getData() {
    const parentDefinitionId = match.params.definitionId || '';
    const [{ data: settingData }, { data }] = await Promise.all([service.getProcessType(match.params.processId), service.getContent(match.params.processId,parentDefinitionId)]);

    setSettingData(settingData);
    setData(JSON.parse(data.content));
    setGetting(false);
  }

  function dataChange(data) {
    setData(JSON.parse(JSON.stringify(data)));
  }


  function submit() {
    setSubmiting(true);

    service.saveContent(match.params.processId, JSON.stringify(data)).then(() => {
      message.success("保存成功！");
      setSubmiting(false);
      saveHastenList();
    }).catch(err => {
      message.error(err.response.data.message);
      setSubmiting(false);
    });
  }

  function saveHastenList() {
    const hastenList = Object.values(hasten);
    service
      .hastenApproval(hastenList)
      .then(() => {})
      .catch(err => {
        message.error(err.response.data.message);
      })
  }

  function renderCurrentNode() {
    switch (type) {
      case "approval":
        return (
          <ApprovalNode
            params={currentNode.attribute || {}}
            nodeCode={currentNode.id}
            processTypeId={settingData.processTypeId}
            onCancel={() => { setAttrVisible(false) }}
            onSave={result => { save(result, "node") }}
            processSettingId={match.params.processId}
          />
        );
      case "robot":
        return (
          <RobotNode params={currentNode.attribute || {}} processTypeId={settingData.processTypeId} onCancel={() => { setAttrVisible(false) }} onSave={result => { save(result, "attr") }} />
        );
      case "custom-approval":
        return (
          <CustomApprovalNode
            params={currentNode.attribute || {}}
            nodeCode={currentNode.id}
            processTypeId={settingData.processTypeId}
            onCancel={() => { setAttrVisible(false) }}
            onSave={result => { save(result, "node") }}
            processSettingId={match.params.processId}
          />
        );
      case "approval-chain":
        return (
          <ApprovalChainNode
            params={currentNode.attribute || {}}
            nodeCode={currentNode.id}
            processTypeId={settingData.processTypeId}
            onCancel={() => { setAttrVisible(false) }}
            onSave={result => { save(result, "node") }}
            processSettingId={match.params.processId}
          />
        )
    }
  }

  function renderNoticeNode() {
    switch (type) {
      case "approval":
        return (
          <NoticeSetting
            dataType={currentNode.dataType}
            params={currentNode.attribute || {}}
            processTypeId={settingData.processTypeId}
            onCancel={() => { setNoticeVisible(false) }}
            onSave={result => { save(result, "notice") }}
            nodeCode={currentNode.id}
            processSettingId={match.params.processId}
          />
        );
      case "robot":
        return (
          <NoticeSetting
            params={currentNode.attribute || {}}
            processTypeId={settingData.processTypeId}
            onCancel={() => { setNoticeVisible(false) }}
            onSave={result => { save(result, "notice") }}
            nodeCode={currentNode.id}
            processSettingId={match.params.processId}
          />
        );
      case "custom-approval":
        return (
          <NoticeSetting
            params={currentNode.attribute || {}}
            processTypeId={settingData.processTypeId}
            onCancel={() => { setNoticeVisible(false) }}
            onSave={result => { save(result, "notice") }}
            nodeCode={currentNode.id}
            processSettingId={match.params.processId}
          />
        );
      case "start":
        return (
          <NoticeSetting
            nodeType="start"
            params={currentNode.attribute || {}}
            processTypeId={settingData.processTypeId}
            onCancel={() => { setNoticeVisible(false) }}
            onSave={result => { save(result, "notice") }}
            nodeCode={currentNode.id}
            processSettingId={match.params.processId}
          />
        );
      case "end":
        return (
          <NoticeSetting
            nodeType="end"
            params={currentNode.attribute || {}}
            processTypeId={settingData.processTypeId}
            onCancel={() => { setNoticeVisible(false) }}
            onSave={result => { save(result, "notice") }}
            nodeCode={currentNode.id}
            processSettingId={match.params.processId}
          />
        );
    }
  }

  function renderConditionNode() {
    return (
      <ConditionsNode
        params={currentNode.conditions || {}}
        processTypeId={settingData.processTypeId}
        onCancel={() => { setNoticeVisible(false) }}
        onSave={result => { save(result, "condition") }}
      />
    )
  }

  function back() {
    Modal.confirm({
      title: '提示',
      content: '确认返回？所做的更改将会丢失。',
      okText: '确认',
      cancelText: '取消',
      onOk: () => {
        pushPage({
          functionCode: "workflow_definition",
          pageCode: "workflow_definition",
        })
      }
    });
  }

  function renderItem(label, value, top = 10, showBadge = false) {
    return (
      <div style={{ display: "flex", marginTop: top }}>
        <div style={{ flex: "0 0 100px", textAlign: "right" }}>{label}:</div>
        <div style={{ flex: "1", marginLeft: 6 }}>
          {showBadge && <Badge status={value ? "success" : "error"} />}
          {showBadge ? (value ? "启用" : "禁用") : value}
        </div>
      </div>
    )
  }

  function handleEditLineValue(e) {
    if (e) e.preventDefault();
    setEditVisible(true);
  }

  async function handleCloseAfterEdit(flag) {
    setEditVisible(false);
    if (flag) {
      const { data } = await service.getProcessType(match.params.processId);
      setSettingData(data)
    };
  }

  return (
    <>
      <div className="flow-design-container">
        <div className="header">
          <div onClick={back} style={{ float: "left", cursor: "pointer" }}>
            <Icon type="left" style={{ color: "#fff", fontSize: 20, }} />
            <span style={{ color: "#fff", fontSize: 18, marginLeft: 4 }}>{settingData.processName}</span>
          </div>
          <div style={{ float: "right" }}>
            <Button style={{ marginRight: 10 }} type="danger" onClick={reset}>重置</Button>
            <Button loading={submiting} type="primary" onClick={submit}>保存</Button>
          </div>
        </div>
        {getting ? (<Spin size="large"><div style={{ height: "calc(100vh - 64px)", width: "100%", backgroundColor: "#e3e5e8" }} /></Spin>) : (
          <>
            <div className="content">
              <div className="flow-design-box">
                <FlowDesign
                  onAttrClick={attrClick}
                  onNoticeClick={noticeClick}
                  onConditionClick={conditionClick}
                  onDataChange={dataChange}
                  data={data}
                />
              </div>
              <div className="right-panel">
                <Card
                  hoverable
                  title="基本信息"
                  extra={<a onClick={handleEditLineValue}>编辑</a>}
                  style={{ width: 300 }}
                >
                  {renderItem("工作流类型", settingData.processTypeName, 0)}
                  {renderItem("工作流代码", settingData.processCode)}
                  {renderItem("工作流名称", settingData.processName)}
                  {renderItem("工作流版本", settingData.contentVersion)}
                  {renderItem("是否允许撤回", settingData.withdrawFlag ? "可撤回" : "不可撤回")}
                  {settingData.withdrawFlag && renderItem("撤回模式", settingData.withdrawRule === 1001 ? '无审批记录时可撤回' : '审批流未结束均可撤回')}
                  {renderItem("状态", settingData.enabled, 10, true)}
                </Card>
              </div>
            </div>
            <SlideFrame
              title="属性"
              show={attrVisible}
              onClose={() => { setAttrVisible(false) }}
            >
              <div style={{ padding: 20, paddingTop: 0 }}>
                {renderCurrentNode()}
              </div>
            </SlideFrame>
            <SlideFrame
              title="通知"
              show={noticeVisible}
              onClose={() => { setNoticeVisible(false) }}
            >
              <div style={{ padding: 20, paddingTop: 0 }}>
                {renderNoticeNode()}
              </div>
            </SlideFrame>
            <SlideFrame
              title="条件"
              show={conditionVisible}
              onClose={() => { setConditionVisible(false) }}
            >
              <div style={{ padding: 20, paddingTop: 0 }}>
                {renderConditionNode()}
              </div>
            </SlideFrame>
            <SlideFrame
              title="编辑工作流"
              show={editVisible}
              onClose={() => { setEditVisible(false) }}
            >
              <WorkflowDefineFrame params={settingData} onClose={handleCloseAfterEdit} />
            </SlideFrame>
          </>
        )}
      </div>
    </>
  )
}

export default WorkFlow
