import G6 from '@antv/g6';
import userIcon from '../icons/user.svg'

export default {
  start: () => {
    G6.registerNode("start-node", {
      draw(cfg, group) {
        const style = this.getShapeStyle(cfg);
        const shape = group.addShape('rect', {
          attrs: {
            ...style,
            cursor: "pointer",
            radius: [10],
            fill: '#DDE9FB',
            stroke: '#DDE9FB',
            width: 126,
            height: 40,
          },
        });

        if (cfg.title) {
          let { title } = cfg;
          if (title.length > 5) {
            title = title.substr(0, 5);
            title += '.'.repeat(3);
          }
          group.addShape('text', {
            attrs: {
              x: style.x + 63,
              y: style.y + 20,
              textAlign: 'center',
              textBaseline: 'middle',
              text: title,
              fill: '#4390FF',
              fontSize: 14,
            },
          });
        }
        return shape;
      },
      setState(name, value, item) {
        const group = item.getContainer();
        const { style } = item.getModel();
        if (name === 'selected') {
          const rect = group.getChildByIndex(0);

          if (!style) {
            rect.attr('fill', '#95D6FB');
            return;
          }

          if (value) {
            rect.attr('fill', value);
          } else if (style.selected && style.selected.fill) {
            rect.attr('fill', style.selected.fill);
          } else {
            rect.attr('fill', '#95D6FB');
          }
        }
      },
    }, "rect");
  },
  end: () => {
    G6.registerNode("end-node", {
      draw(cfg, group) {
        const style = this.getShapeStyle(cfg);
        const shape = group.addShape('rect', {
          attrs: {
            ...style,
            cursor: "pointer",
            radius: [10],
            fill: '#FDE9EC',
            stroke: '#FDE9EC',
            width: 126,
            height: 40,
          },
        });

        if (cfg.title) {
          let { title } = cfg;
          if (title.length > 5) {
            title = title.substr(0, 5);
            title += '.'.repeat(3);
          }
          group.addShape('text', {
            attrs: {
              x: style.x + 63,
              y: style.y + 20,
              textAlign: 'center',
              textBaseline: 'middle',
              text: title,
              fill: '#FF465D',
              fontSize: 14,
            },
          });
        }
        return shape;
      },
      setState(name, value, item) {
        const group = item.getContainer();
        const { style } = item.getModel();
        if (name === 'selected') {
          const rect = group.getChildByIndex(0);

          if (!style) {
            rect.attr('fill', '#95D6FB');
            return;
          }

          if (value) {
            rect.attr('fill', value);
          } else if (style.selected && style.selected.fill) {
            rect.attr('fill', style.selected.fill);
          } else {
            rect.attr('fill', '#95D6FB');
          }
        }
      },
    }, "rect");
  },
  approval: () => {
    G6.registerNode("approval-node", {
      draw(cfg, group) {
        const style = this.getShapeStyle(cfg);
        const shape = group.addShape('rect', {
          attrs: {
            ...style,
            cursor: "pointer",
            radius: [4],
            fill: '#FFFFFF',
            stroke: '#FFFFFF',
            width: 148,
            height: 70,
          },
        });

        group.addShape('image', {
          attrs: {
            x: style.x + 4,
            y: style.y + 7,
            width: 10,
            height: 12,
            img: userIcon,
          },
        });

        group.addShape('text', {
          attrs: {
            x: style.x + 18,
            y: style.y + 7,
            textAlign: 'left',
            textBaseline: 'top',
            text: '审批',
            fill: '#999999',
            fontSize: 14,
          },
        });

        if (cfg.title) {
          let { title } = cfg;
          if (title.length > 5) {
            title = title.substr(0, 5);
            title += '.'.repeat(3);
          }
          group.addShape('text', {
            attrs: {
              x: style.x + 74,
              y: style.y + 42,
              textAlign: 'center',
              textBaseline: 'middle',
              text: title,
              fill: '#333333',
              fontSize: 14,
            },
          });
        }
        return shape;
      },
      setState(name, value, item) {
        const group = item.getContainer();
        const { style } = item.getModel();
        if (name === 'selected') {
          const rect = group.getChildByIndex(0);

          if (!style) {
            rect.attr('fill', '#95D6FB');
            return;
          }

          if (value) {
            rect.attr('fill', value);
          } else if (style.selected && style.selected.fill) {
            rect.attr('fill', style.selected.fill);
          } else {
            rect.attr('fill', '#95D6FB');
          }
        }
      },
    }, "rect");
  },
  condition: () => {
    G6.registerNode("condition-node", {
      draw(cfg, group) {
        const style = this.getShapeStyle(cfg);
        const shape = group.addShape('rect', {
          attrs: {
            ...style,
            cursor: "pointer",
            radius: [18],
            fill: '#FEFAE9',
            stroke: '#ccc',
            width: 148,
            height: 46,
          },
        });

        group.addShape('image', {
          attrs: {
            x: style.x + 4,
            y: style.y + 7,
            width: 10,
            height: 12,
            img: userIcon,
          },
        });

        if (cfg.title) {
          let { title } = cfg;
          if (title.length > 5) {
            title = title.substr(0, 5);
            title += '.'.repeat(3);
          }
          group.addShape('text', {
            attrs: {
              x: style.x + 74,
              y: style.y + 23,
              textAlign: 'center',
              textBaseline: 'middle',
              text: title,
              fill: '#333333',
              fontSize: 14,
            },
          });
        }
        return shape;
      },
      setState(name, value, item) {
        const group = item.getContainer();
        const { style } = item.getModel();
        if (name === 'selected') {
          const rect = group.getChildByIndex(0);

          if (!style) {
            rect.attr('fill', '#95D6FB');
            return;
          }

          if (value) {
            rect.attr('fill', value);
          } else if (style.selected && style.selected.fill) {
            rect.attr('fill', style.selected.fill);
          } else {
            rect.attr('fill', '#95D6FB');
          }
        }
      },
    }, "rect");
  },


  
}
