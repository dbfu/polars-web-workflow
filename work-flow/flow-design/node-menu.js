import React from 'react'
import { Menu } from 'antd'

const { SubMenu } = Menu;

const NodeContextMenu = ({ x = -300, y = 0, onClick, onMouseLeave, node, parentNode }) => {

  let showCondition = false;
  if (parentNode && parentNode.children && parentNode.children.length > 1) {
    if (!node.children || node.children.length > 1 || (node.children.length === 1 && node.children[0].dataType !== "condition")) {
      showCondition = true;
    }
  }

  if (node.type === "node" && node.dataType !== "condition") {
    if (node.id === 'start' || node.id === 'end') {
      return (
        <div onMouseLeave={onMouseLeave} style={{ position: 'absolute', left: x, top: y - 64, boxShadow: '0 2px 8px rgba(0,0,0,0.15)', width: 100 }}>
          <Menu onClick={onClick} mode="vertical">
            <Menu.Item key="notice">通知</Menu.Item>
          </Menu>
        </div>
      )
    } else {
      return (
        <div onMouseLeave={onMouseLeave} style={{ position: 'absolute', left: x, top: y - 64, boxShadow: '0 2px 8px rgba(0,0,0,0.15)', width: 100 }}>
          <Menu onClick={onClick} mode="vertical">
            <Menu.Item key="attr">属性</Menu.Item>
            <Menu.Item key="notice">通知</Menu.Item>
            <Menu.Item key="delete">删除</Menu.Item>
          </Menu>
        </div>
      )
    }
  } else if (node.type === "node" && node.dataType === "condition") {
    return (
      <div onMouseLeave={onMouseLeave} style={{ position: 'absolute', left: x, top: y - 64, boxShadow: '0 2px 8px rgba(0,0,0,0.15)', width: 100 }}>
        <Menu onClick={onClick} mode="vertical">
          <Menu.Item key="setting">设置</Menu.Item>
          <Menu.Item key="delete">删除</Menu.Item>
        </Menu>
      </div>
    )
  } else if (node.children && node.children.length > 1) {
    return (
      <div onMouseLeave={onMouseLeave} style={{ position: 'absolute', left: x, top: y - 64, boxShadow: '0 2px 8px rgba(0,0,0,0.15)', width: 100 }}>
        <Menu onClick={onClick} mode="vertical">
          <SubMenu
            key="node"
            title={
              <span>
                <span>节点</span>
              </span>
            }
          >
            <Menu.Item key="approval">审批</Menu.Item>
            <Menu.Item key="custom-approval">自选审批</Menu.Item>
            <Menu.Item key="robot">机器人</Menu.Item>
            <Menu.Item key="approval-chain">审批链</Menu.Item>
          </SubMenu>
          <SubMenu
            key="branch"
            title={
              <span>
                <span>分支</span>
              </span>
            }
          >
            <Menu.Item key="main">主干分支</Menu.Item>
            <Menu.Item key="current">当前分支</Menu.Item>
            <Menu.Item key="child">子分支</Menu.Item>
          </SubMenu>
          {showCondition && <Menu.Item key="condition">条件</Menu.Item>}
          <Menu.Item key="delete">删除</Menu.Item>
        </Menu>
      </div>
    )
  } else if (node.children && node.children.length === 1) {
    return (
      <div onMouseLeave={onMouseLeave} style={{ position: 'absolute', left: x, top: y - 64, boxShadow: '0 2px 8px rgba(0,0,0,0.15)', width: 100 }}>
        <Menu onClick={onClick} mode="vertical">
          <SubMenu
            key="node"
            title={
              <span>
                <span>节点</span>
              </span>
            }
          >
            <Menu.Item key="approval">审批</Menu.Item>
            <Menu.Item key="custom-approval">自选审批</Menu.Item>
            <Menu.Item key="robot">机器人</Menu.Item>
            <Menu.Item key="approval-chain">审批链</Menu.Item>
          </SubMenu>
          <SubMenu
            key="branch"
            title={
              <span>
                <span>分支</span>
              </span>
            }
          >
            <Menu.Item key="main">主干分支</Menu.Item>
            <Menu.Item key="current">当前分支</Menu.Item>
          </SubMenu>
          {showCondition && <Menu.Item key="condition">条件</Menu.Item>}
          <Menu.Item key="delete">删除</Menu.Item>
        </Menu>
      </div>
    )
  } else {
    return (
      <div onMouseLeave={onMouseLeave} style={{ position: 'absolute', left: x, top: y - 64, boxShadow: '0 2px 8px rgba(0,0,0,0.15)', width: 100 }}>
        <Menu onClick={onClick} mode="vertical">
          <SubMenu
            key="node"
            title={
              <span>
                <span>节点</span>
              </span>
            }
          >
            <Menu.Item key="approval">审批</Menu.Item>
            <Menu.Item key="custom-approval">自选审批</Menu.Item>
            <Menu.Item key="robot">机器人</Menu.Item>
            <Menu.Item key="approval-chain">审批链</Menu.Item>
          </SubMenu>
          <Menu.Item key="branch">分支</Menu.Item>
          {showCondition && <Menu.Item key="condition">条件</Menu.Item>}
          {node.parent !== 'start' && <Menu.Item key="delete">删除</Menu.Item>}
        </Menu>
      </div>
    )
  }

}

export default NodeContextMenu