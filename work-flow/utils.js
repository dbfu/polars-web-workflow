import userIcon from './icons/user.svg'
import addIcon from './icons/add.svg'

export function uuid() {
  var d = new Date().getTime();
  if (window.performance && typeof window.performance.now === "function") {
    d += performance.now(); //use high-precision timer if available
  }
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
}

// 获取树的深度
export function handleGetTreeDeep(node) {
  let deep = 0;
  if (node.children) {
    node.children.forEach(item => {
      deep = Math.max(handleGetTreeDeep(item) + 1, deep);
    });
  } else {
    deep += 1;
  }
  if (node.end) {
    deep += handleGetTreeDeep(node.end);
  }
  return deep;
}

// 获取树的广度
export function handleGetTreeExtent(nodes) {
  let extend = 0;
  if (!nodes) return extend;
  nodes.forEach(item => {
    if (item.children) {
      const n1 = handleGetTreeExtent(item.children);
      if (item.end) {
        const n2 = handleGetTreeExtent([item.end]);
        extend += Math.max(n1, n2);
      } else {
        extend += n1;
      }
    } else {
      if (item.end && item.end.children) {
        const n2 = handleGetTreeExtent(item.end.children);
        extend += n2;
      } else {
        extend += 1;
      }
    }
  });
  return extend;
}

export function createNode(type) {
  switch (type) {
    case "add": {
      return {
        shape: "base-node",
        style: {
          cursor: "pointer",
          radius: [5],
          fill: 'rgba(0,0,0,0)',
          stroke: 'rgba(0,0,0,0)',
        },
        icon: {
          img: addIcon,
          x: -2,
          y: -2,
          width: 14,
          height: 14,
        },
        type: "node",
        size: [10, 10],
        anchorPoints: [
          [0.5, 0], // 上中
          [0.5, 1], // 下中
          [0, 0.5], // 左中
          [1, 0.5], // 右中
        ]
      }
    }
    case "start": {
      return {
        shape: "start-node",
        size: [126, 40],
      }
    }
    case "end": {
      console.log(111);
      return {
        shape: "end-node",
        size: [126, 40],
      }
    }
    case "approval": {
      return {
        shape: "approval-node",
        size: [148, 70],
      }
    }
    case "condition": {
      return {
        shape: "condition-node",
        size: [148, 46],
      }
    }
    default: {
      return {
        shape: "base-node",
        label: "节点",
        icon: {
          img: userIcon,
          x: 4,
          y: 4,
        },
        style: {
          fill: '#E7F7FE',
          stroke: '#1890FF',
          cursor: "pointer",
          radius: [4],
        },
        type: "node",
        size: [86, 46],
        linkPoints: {
          top: true,
          bottom: true,
          left: false,
          right: false,
          fill: '#fff',
          size: 5
        },
        anchorPoints: [
          [0.5, 0], // 上中
          [0.5, 1], // 下中
          [0, 0.5], // 左中
          [1, 0.5], // 右中
        ]
      }
    }
  }
}

export function getNodeById(data, id) {
  for (let i = 0; i < data.length; i++) {
    if (data[i].id === id) return data[i];
    if (data[i].children) {
      const node = this.getNodeById(data[i].children, id);
      if (node) return node;
    }
    if (data[i].end) {
      if (data[i].end.id === id) return data[i].end;
      const node = this.getNodeById([data[i].end], id);
      if (node) return node;
    }
  }
}