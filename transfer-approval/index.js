import React, { Component } from 'react';
import { Tabs } from 'antd';
import MyPassing from './my-passing';
import MyAgent from './my-agent';
import ContentHeader from "widget/content-header"
import Content from 'widget/content'
import ContentScroller from 'widget/content-scroller'
const TabPane = Tabs.TabPane;

class Transfer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabKey: '1',
    }
  }

  // tab选项卡切换
  tabChange = (tabKey) => {
    this.setState({ tabKey });
  }

  render() {
    const { tabKey } = this.state;
    return (
      <div>
        <ContentHeader title="审批流转交代理" />
        <ContentScroller>
          <Content>
            <Tabs
              defaultActiveKey={tabKey}
              onChange={this.tabChange}
            >
              <TabPane tab="我的转交" key="1" >
                <MyPassing />
              </TabPane>
              <TabPane tab="我的代理" key="2" >
                <MyAgent />
              </TabPane>
            </Tabs>
          </Content>
        </ContentScroller>
      </div>

    )
  }
}

export default Transfer;
