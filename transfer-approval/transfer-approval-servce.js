import config from 'config'
import httpFetch from 'share/httpFetch'

export default {
  /**
   * 新增转交
   * @param {*} params
   */
  addPassingSetting(params) {
    return httpFetch.post(`${config.wflUrl}/api/workflow/transfer/agent`, params);
  },
  /**
   * 更改转交
   * @param {*} params
   */
  editPassingSetting(params) {
    return httpFetch.put(`${config.wflUrl}/api/workflow/transfer/agent`, params);
  },

  /**
   * 获取全部工作流类型  
   */
  queryWorkflowTypeByCond() {
    return httpFetch.get(`${config.wflUrl}/api/wfl/type?page=0&size=9999`)
  },

  /**
   * 获取全部工作流
   */
  queryWorkflow(id){
    return httpFetch.get(`${config.wflUrl}/api/wfl/process/setting?page=0&size=9999&processType=${id}`)
  },

}
